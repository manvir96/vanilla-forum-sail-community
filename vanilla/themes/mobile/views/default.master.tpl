<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-ca">
<head>
    {asset name='Head'}
    
    <script src="{$js_dir}http://localhost/themes/mobile/main.js" type="text/javascript"></script>
    
</head>

<body id="{$BodyID}" class="{$BodyClass}">
    
<div id="Frame">
    <div class="Banner">
        <div class="top-bar">
    <div class="container">
        <a href="http://sailcannabis.co/physicians.html"><u>For Physicians</u></a>
        <a style="margin-left: 24px" href="http://sailcannabis.co/suppliers.html"><u>For Suppliers</u></a>
    </div>
</div>
        <div class="top-sail-banner container">
        <div class="logo"><a href="http://sailcannabis.co/">
                        <img src="http://sailcannabis.co/images/sail-logo.png" height="55.7px" width="89.1px">
                    </a>
        </div>
            <div class="menu-btn text-right">
            <a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="showSidebar()">
                <img src="http://sailcannabis.co/images/btn-menu@3x.png" class="btn-menu">
            </a>
            </div>
        </div>
        <ul>
            {home_link}
            {profile_link}
            {inbox_link}
            {custom_menu}
            {event name="BeforeSignInLink"}
            {if !$User.SignedIn}
                <li class="SignInItem">{link path="signin" class="SignIn"}</li>
            {/if}
        </ul>
    </div>
    
    
<!--    /* side bar menu */-->
     <div id="hidden-side-menu" class="sidebar-menu responsive hide-me">


            <div class="sidebar-h-menu container">

                <div class="sidebar-close-btn text-right" onclick="closeSidebar()">&#10006</div>

                <div class="sidebar-menu-navigation text-center">
                    <a href="https://sailportal.co" class="button" target="_blank">Canadian Login / Register</a><br><br>
                    <a href="https://sailportal.net" class="button" target="_blank" >American Login / Register</a><br><br>

                    <a href="Articles/Pain_Management/pain-management.html">Pain Management</a><br><br>
                    <!--<a href="#">Mood Disorders</a><br><br>-->
                    <a href="Articles/Mood_Disorder/Mood_Disorders.html">Mood Disorders</a><br><br>
                    <a href="Articles/Epilepsy/epilepsy.html">Epilepsy</a><br><br>
                    <a href="Articles/Cancer/cancer.html"><span>Cancer</span></a><br><br>
                    <a href="Articles/MultipleSclerosis/Multiple_Sclerosis.html"><span>Multiple Sclerosis</span></a><br><br>

                    <a href="https://sailcannabis.cmnty.com/authorize/login" target="_blank">Community</a><br><br>

                    <a href="webinar.html">Webinar</a><br><br>
                    <a href="find-clinic.html">Find A Clinic</a><br><br><br>

                    <div class="sidebar-social-icons">
                        <a href="https://www.facebook.com/sailcann/" class="social-icons" target="_blank">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                        <a href="https://twitter.com/sailcannabis" class="social-icons" target="_blank">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                        <a href="https://www.instagram.com/sailcannabis_/" class="social-icons" target="_blank">
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                        
                    </div>
                </div>

            </div>

        </div>
    
<!--    ///////////////////////////-->
    <div id="Body">
        <div class="BreadcrumbsWrapper">
            {breadcrumbs homelink="0"}
        </div>
        <div id="Content">
            {asset name="Content"}
        </div>
    </div>
    <div id="Foot">
        <div class="FootMenu">
            {nomobile_link wrap="span"}
            {dashboard_link wrap="span"}
            {signinout_link wrap="span"}
        </div>
<!--
        <a class="PoweredByVanilla" href="{vanillaurl}"><span>Powered by Vanilla</span></a>
        {asset name="Foot"}
-->
    </div>
</div>
{event name="AfterBody"}
</body>
</html>
