<?php if (!defined('APPLICATION')) exit();

//==================================================================================================
//                                          FORUM
//==================================================================================================

// Discussions
$Definition['All Discussions'] = 'Conversaciones Recientes';


//==================================================================================================
//                                          THEME
//==================================================================================================

// Preview Style Selector
$Definition['SELECT STYLE'] = 'SELECCIONE EL ESTILO';
$Definition['My Custom Styles'] = 'Mis Estilos Personalizados';
$Definition['System Styles'] = 'Estilos del Sistema';
$Definition['Version'] = 'Versión';

// Theme Options
$Definition['Choose your current active style and the desired layout. You can also configure any of the available styles and preview it on the front end.<br><small>While you preview a style, all other users will only see the current active style.</small>'] = 'Elige tu estilo activo corriente y el layout deseado. También puede configurar cualquiera de los estilos disponibles y obtener una vista previa en lo front end.<br><small>Mientras que obtener una vista previa de un estilo, todos los demás usuarios sólo ven el estilo activo actual.</small>';
$Definition['This is the default layout.<br>The main content will appear on the left and the sidebar on the right.'] = 'Este es el layout predeterminado.<br>El contenido principal aparecerá a la izquierda y la barra lateral a la derecha.';
$Definition['A right side layout.<br>The main content will appear on the right and the sidebar on the left.'] = 'Un layout de lateral derecho. El contenido principal aparecerá a la derecha y la barra lateral a la izquierda.';
$Definition['Extra Style Being Configured'] = 'Estilo Extra en Configuración';
$Definition['Current Style'] = 'Estilo Actual';
$Definition['SAVE'] = 'GUARDAR';
$Definition['Styles'] = 'Estilos';
$Definition['Layouts'] = 'Layouts';
$Definition['Show the Preview Style Selector'] = 'Mostrar el Selector de Vista Previa';
$Definition['Import Styles'] = 'Importar Estilos';
$Definition['Add ZIP file'] = 'Adiciona um ficheiro ZIP';
$Definition['Export Style'] = 'Exportar Estilo';
$Definition['EXPORT'] = 'EXPORTAR';
$Definition['Style Info'] = 'Información del Estilo';
$Definition['STYLE INFO'] = 'INFORMACIÓN DEL ESTILO';
$Definition['Expandable'] = 'Expandible';
$Definition['Collapsible'] = 'Colapsable';
$Definition['Author Url'] = 'Url del Autor';
$Definition['SAVE AS'] = 'GUARDAR COMO';
$Definition['DELETE'] = 'BORRAR';
$Definition['Save As'] = 'Guardar Como';
$Definition['Save Style'] = 'Guardar Estilo';
$Definition['Style saved successfully!'] = 'Estilo guardó con éxito!';
$Definition['Delete Style'] = 'Borrar Estilo';
$Definition['Are you sure you want to delete this style?'] = '¿Seguro que quieres borrar este estilo?';
$Definition['You can not delete this style because its current active style.'] = 'No lo puedes eliminar este estilo porque es el estilo activo actual.';
$Definition['Okay'] = 'Ok';
$Definition['Import'] = 'Importar';
$Definition['Please fill the Style Name, the Description, the Version and the Author!'] = 'Por favor rellene el Nombre del Estilo, la Descripción, la Versión y el Autor!';
$Definition['There is already a Style with that name. Please change to another one.'] = 'Ya existe un estilo con ese nombre. Por favor cambia a otro.';

// Envato Registration
$Definition['New users needs an Envato Item Purchase Code.'] = 'Los nuevos usuarios necesita lo Envato Item Purchase Code.';
$Definition['The Envato registration form requires you to set up your Envato Username and API key.'] = 'El formulario de inscripción Envato le requiere configurar su Envato Username y API key.';
$Definition['Envato'] = 'Envato';
$Definition['API Key'] = 'API Key';
$Definition['where do I find the API Key?'] = '¿Dónde puedo encontrar la API Key?';


//==================================================================================================
//                                          PLUGINS
//==================================================================================================

// In This Discussion
$Definition['In this Discussion'] = 'En esta Conversacione';