<?php if (!defined('APPLICATION')) exit();

//==================================================================================================
//                                          FORUM
//==================================================================================================

// Discussions
$Definition['All Discussions'] = 'Recent Discussions';
$Definition['Howdy, Stranger!'] = 'Welcome';

//==================================================================================================
//                                          THEME
//==================================================================================================

// Preview Style Selector
$Definition['SELECT STYLE'] = 'SELECT STYLE';
$Definition['My Custom Styles'] = 'My Custom Styles';
$Definition['System Styles'] = 'System Styles';
$Definition['Version'] = 'Version';

// Theme Options
$Definition['Choose your current active style and the desired layout. You can also configure any of the available styles and preview it on the front end.<br><small>While you preview a style, all other users will only see the current active style.</small>'] = 'Choose your current active style and the desired layout. You can also configure any of the available styles and preview it on the front end.<br><small>While you preview a style, all other users will only see the current active style.</small>';
$Definition['This is the default layout.<br>The main content will appear on the left and the sidebar on the right.'] = 'This is the default layout.<br>The main content will appear on the left and the sidebar on the right.';
$Definition['A right side layout.<br>The main content will appear on the right and the sidebar on the left.'] = 'A right side layout.<br>The main content will appear on the right and the sidebar on the left.';
$Definition['Extra Style Being Configured'] = 'Extra Style Being Configured';
$Definition['Current Style'] = 'Current Style';
$Definition['SAVE'] = 'SAVE';
$Definition['Styles'] = 'Styles';
$Definition['Layouts'] = 'Layouts';
$Definition['Show the Preview Style Selector'] = 'Show the Preview Style Selector';
$Definition['Import Styles'] = 'Import Styles';
$Definition['Add ZIP file'] = 'Add ZIP file';
$Definition['Export Style'] = 'Export Style';
$Definition['EXPORT'] = 'EXPORT';
$Definition['Style Info'] = 'Style Info';
$Definition['STYLE INFO'] = 'STYLE INFO';
$Definition['Expandable'] = 'Expandable';
$Definition['Collapsible'] = 'Collapsible';
$Definition['Author Url'] = 'Author Url';
$Definition['SAVE AS'] = 'SAVE AS';
$Definition['DELETE'] = 'DELETE';
$Definition['Save As'] = 'Save As';
$Definition['Save Style'] = 'Save Style';
$Definition['Style saved successfully!'] = 'Style saved successfully!';
$Definition['Delete Style'] = 'Delete Style';
$Definition['Are you sure you want to delete this style?'] = 'Are you sure you want to delete this style?';
$Definition['You can not delete this style because its current active style.'] = 'You can not delete this style because its current active style.';
$Definition['Okay'] = 'Okay';
$Definition['Import'] = 'Import';
$Definition['Please fill the Style Name, the Description, the Version and the Author!'] = 'Please fill the Style Name, the Description, the Version and the Author!';
$Definition['There is already a Style with that name. Please change to another one.'] = 'There is already a Style with that name. Please change to another one.';

// Envato Registration
$Definition['New users needs an Envato Item Purchase Code.'] = 'New users needs an Envato Item Purchase Code.';
$Definition['The Envato registration form requires you to set up your Envato Username and API key.'] = 'The Envato registration form requires you to set up your Envato Username and API key.';
$Definition['Envato'] = 'Envato';
$Definition['API Key'] = 'API Key';
$Definition['where do I find the API Key?'] = 'where do I find the API Key?';


//==================================================================================================
//                                          PLUGINS
//==================================================================================================

// In This Discussion
$Definition['In this Discussion'] = 'In this Discussion';