<?php if (!defined('APPLICATION')) exit();

//==================================================================================================
//                                          FORUM
//==================================================================================================

// Discussions
$Definition['All Discussions'] = 'Discussions Récentes';


//==================================================================================================
//                                          THEME
//==================================================================================================

// Preview Style Selector
$Definition['SELECT STYLE'] = 'CHOISISSEZ LE STYLE';
$Definition['My Custom Styles'] = 'Mes Styles Personnalisés';
$Definition['System Styles'] = 'Styles du Système';
$Definition['Version'] = 'Version';

// Theme Options
$Definition['Choose your current active style and the desired layout. You can also configure any of the available styles and preview it on the front end.<br><small>While you preview a style, all other users will only see the current active style.</small>'] = 'Choisissez votre style actif actuel et le layout souhaitée. Vous pouvez également configurer l\'un des styles disponibles et prévisualiser sur le front end.<br><small>Alors que vous visualisez un style, tous les autres utilisateurs ne verront le style actif.</small>';
$Definition['This is the default layout.<br>The main content will appear on the left and the sidebar on the right.'] = 'Ceci est le layout par défaut.<br>Le contenu principal apparaissent sur la gauche et la barre latérale sur la droite.';
$Definition['A right side layout.<br>The main content will appear on the right and the sidebar on the left.'] = 'Un layout de droite.<br>Le contenu principal apparaîtra sur la droite et la barre latérale sur la gauche.';
$Definition['Extra Style Being Configured'] = 'Extra Style Être Configurée';
$Definition['Current Style'] = 'Style en Cours';
$Definition['SAVE'] = 'ENREGISTRER';
$Definition['Styles'] = 'Styles';
$Definition['Layouts'] = 'Layouts';
$Definition['Show the Preview Style Selector'] = 'Montrer le Sélecteur de l\'Aperçu de Style';
$Definition['Import Styles'] = 'Importer des Styles';
$Definition['Add ZIP file'] = 'Ajouter fichier ZIP';
$Definition['Export Style'] = 'Exporter des Styles';
$Definition['EXPORT'] = 'EXPORT';
$Definition['Style Info'] = 'Info du Style';
$Definition['STYLE INFO'] = 'INFO DU STYLE';
$Definition['Expandable'] = 'Extensible';
$Definition['Collapsible'] = 'Pliable';
$Definition['Author Url'] = 'Url du Auteur';
$Definition['SAVE AS'] = 'ENREGISTRER SOUS';
$Definition['DELETE'] = 'SUPPRIMER';
$Definition['Save As'] = 'Enregistrer Sous';
$Definition['Save Style'] = 'Enregistrer Style';
$Definition['Style saved successfully!'] = 'Style enregistré avec succès!';
$Definition['Delete Style'] = 'Supprimer Style';
$Definition['Are you sure you want to delete this style?'] = 'Êtes-vous sûr de vouloir supprimer ce style?';
$Definition['You can not delete this style because its current active style.'] = 'Vous ne pouvez pas supprimer ce style parce que ce style est actif.';
$Definition['Okay'] = 'Ok';
$Definition['Import'] = 'Importer';
$Definition['Please fill the Style Name, the Description, the Version and the Author!'] = 'S\'il vous plaît remplir le Nom du Style, la Description, la Version et l\'Auteur!';
$Definition['There is already a Style with that name. Please change to another one.'] = 'Il existe déjà un Style avec ce nom. S\'il vous plaît changer pour une autre.';

// Envato Registration
$Definition['New users needs an Envato Item Purchase Code.'] = 'Les nouveaux utilisateurs a besoin d\'une Envato Item Purchase Code.';
$Definition['The Envato registration form requires you to set up your Envato Username and API key.'] = 'Le formulaire d\'inscription Envato vous oblige à mettre en place votre Envato Username et API key.';
$Definition['Envato'] = 'Envato';
$Definition['API Key'] = 'API Key';
$Definition['where do I find the API Key?'] = 'où puis-je trouver le API Key?';


//==================================================================================================
//                                          PLUGINS
//==================================================================================================

// In This Discussion
$Definition['In this Discussion'] = 'Dans cette Discussion';