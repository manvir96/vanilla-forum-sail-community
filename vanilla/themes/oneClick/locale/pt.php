<?php if (!defined('APPLICATION')) exit();

//==================================================================================================
//                                          FORUM
//==================================================================================================

// Discussions
$Definition['All Discussions'] = 'Tópicos Recentes';


//==================================================================================================
//                                          THEME
//==================================================================================================

// Preview Style Selector
$Definition['SELECT STYLE'] = 'SELECIONE O ESTILO';
$Definition['My Custom Styles'] = 'Meus Estilos Personalizados';
$Definition['System Styles'] = 'Estilos do Sistema';
$Definition['Version'] = 'Versão';

// Theme Options
$Definition['Choose your current active style and the desired layout. You can also configure any of the available styles and preview it on the front end.<br><small>While you preview a style, all other users will only see the current active style.</small>'] = 'Escolha o seu estilo ativo e o layout desejado. Você também pode configurar qualquer um dos estilos disponíveis e visualizá-lo no front-end.<br><small>Enquanto você visualiza um estilo, todos os outros usuários só irão ver o estilo ativo atual.</small>';
$Definition['This is the default layout.<br>The main content will appear on the left and the sidebar on the right.'] = 'Este é o layout padrão.<br>O conteúdo principal aparece na esquerda e a barra lateral à direita.';
$Definition['A right side layout.<br>The main content will appear on the right and the sidebar on the left.'] = 'Um layout à direita.<br>O conteúdo principal aparece na direita e a barra lateral à esquerda.';
$Definition['Extra Style Being Configured'] = 'Estilo Extra em Configuração';
$Definition['Current Style'] = 'Estilo Actual';
$Definition['SAVE'] = 'SAVAR';
$Definition['Styles'] = 'Estilos';
$Definition['Layouts'] = 'Layouts';
$Definition['Show the Preview Style Selector'] = 'Exibir o Seletor de Pré-Visualização de Estilos';
$Definition['Import Styles'] = 'Importar Estilos';
$Definition['Add ZIP file'] = 'Adiciona um ficheiro ZIP';
$Definition['Export Style'] = 'Exportar Estilo';
$Definition['EXPORT'] = 'EXPORTAR';
$Definition['Style Info'] = 'Informação do Estilo';
$Definition['STYLE INFO'] = 'INFORMAÇÃO DO ESTILO';
$Definition['Expandable'] = 'Expansível';
$Definition['Collapsible'] = 'Colapsável';
$Definition['Author Url'] = 'Url do Autor';
$Definition['SAVE AS'] = 'SALVAR COMO';
$Definition['DELETE'] = 'APAGAR';
$Definition['Save As'] = 'Salvar Como';
$Definition['Save Style'] = 'Salvar Estilo';
$Definition['Style saved successfully!'] = 'Estilo gravado com sucesso!';
$Definition['Delete Style'] = 'Apagar Estilo';
$Definition['Are you sure you want to delete this style?'] = 'Tem a certeza que deseja apagar este estilo?';
$Definition['You can not delete this style because its current active style.'] = 'Não pode apagar este estilo porque é o estilo actual.';
$Definition['Okay'] = 'Ok';
$Definition['Import'] = 'Importar';
$Definition['Please fill the Style Name, the Description, the Version and the Author!'] = 'Por favor preencha o Nome do Estilo, a Descrição, a Versão e o Autor!';
$Definition['There is already a Style with that name. Please change to another one.'] = 'Já existe um Estilo com esse nome. Por favor mude o para outro!';

// Envato Registration
$Definition['New users needs an Envato Item Purchase Code.'] = 'Novos usuários necessitam do Envato Item Purchase Code.';
$Definition['The Envato registration form requires you to set up your Envato Username and API key.'] = 'O formulário de inscrição Envato requer que você configure o seu Envato Username e API key.';
$Definition['Envato'] = 'Envato';
$Definition['API Key'] = 'API Key';
$Definition['where do I find the API Key?'] = 'onde posso encontrar a API Key?';


//==================================================================================================
//                                          PLUGINS
//==================================================================================================

// In This Discussion
$Definition['In this Discussion'] = 'Neste Tópico';