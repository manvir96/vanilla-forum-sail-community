<?php

$Session = Gdn::Session();
session_start();

if(C('ThemeOption.PreviewStyle') and $Session->IsValid() and $Session->User->Admin){

    if(isset($_POST['ajax_call']) and $_POST['ajax_call']=='set_style_preview' and $_POST['opened']!=''){
        $_SESSION['one_click_opened']=$_POST['opened'];
        exit;
    }

    if(isset($_GET['style']) and $_GET['style']!=''){
        $_SESSION['one_click_preview_style']=$_GET['style'];
        $selected_style = $_GET['style'];
    }elseif(isset($_SESSION['one_click_preview_style']) and $_SESSION['one_click_preview_style']!=''){
        $selected_style = $_SESSION['one_click_preview_style'];
    }else{
        $selected_style = Gdn::Config('ThemeOption.ExtraStyle');
    }

    if(isset($_GET['tag'])){
        $_SESSION['one_click_tag']=$_GET['tag'];
        $tag = $_GET['tag'];
    }elseif(isset($_SESSION['one_click_tag']) and $_SESSION['one_click_tag']!=''){
        $tag = $_SESSION['one_click_tag'];
    }

    if(isset($_SESSION['one_click_opened']) and $_SESSION['one_click_opened']=='true')
        $opened = true;
    else
        $opened = false;

}else{
    $selected_style = C('ThemeOption.CurrentStyle');
}

// Pass the selected style to the javascript
$this->Head->AddString('
<script type="text/javascript">
var one_click_selected_style = \''.$selected_style.'\';
</script>
');

function style_preview($selected_style,$tag,$opened,$url){

    //==================================================================================================
    //                                      CONFIGURATION
    //==================================================================================================
    $styles_url = 'http://www.one-click-forum.com/#styles';
    //==================================================================================================

    $styles_dir = getcwd().'/themes/oneClick/design/styles/';
    $system_style = false;
    $extra_style_options_custom = array();
    $extra_style_options_system = array();
    if ($handle = opendir($styles_dir)) {
        while (false !== ($file = readdir($handle))) {
            $title='';

            if ($file != "." && $file != ".." && is_dir($styles_dir.$file) && $file!='system') {

                unset($StyleInfo);
                if(is_file($styles_dir.$file.'/style.php')){

                    include($styles_dir.$file.'/style.php');
                    $style_info = current($StyleInfo);

                    $tag_aux = explode(',',$style_info['Tags']);
                    for($i=0; $i<count($tag_aux); $i++)
                        $tag_aux[$i]=trim($tag_aux[$i]);
                    $tag_aux = ','.implode(',',$tag_aux).',';

                    if(isset($tag) and isset($style_info['Tags']) and strpos($tag_aux,','.$tag.',')!==false or !isset($tag) or $tag==''){

                        if($selected_style==$file)
                            $selected_style_info = $style_info;

                        $sort=explode(' ',$style_info['DateCreated']);
                        $sort_date=explode('-',$sort[0]);
                        $sort_time=explode(':',$sort[1]);
                        $timestamp = mktime($sort_time[0], $sort_time[1], $sort_time[2], $sort_date[1], $sort_date[2], $sort_date[0]);

                        if($style_info['Order'] > 0)
                            $timestamp = $timestamp + (100000000 * (100000 - $style_info['Order']));

                        if($style_info['System']){
                            $extra_style_options_system_order[] = $timestamp;
                            $extra_style_options_system[] = '<option value="'.$file.'"'.($selected_style==$file ? ' selected="selected"' : '').'>'.$style_info['Name'].'</option>';
                            if($selected_style==$file)
                                $system_style = true;
                        }else{
                            $extra_style_options_custom_order[] = $timestamp;
                            $extra_style_options_custom[] = '<option value="'.$file.'"'.($selected_style==$file ? ' selected="selected"' : '').'>'.$style_info['Name'].'</option>';

                        }

                    }

                }

            }
        }
        closedir($handle);
    }

    if(count($extra_style_options_custom)>0){
        array_multisort($extra_style_options_custom_order,SORT_DESC,SORT_NUMERIC,$extra_style_options_custom,SORT_STRING,SORT_ASC);
        $extra_style_options_custom_group = '<optgroup label="'.T('My Custom Styles').'">'.implode('',$extra_style_options_custom).'</optgroup>';
    }

    if(count($extra_style_options_system)>0){
        array_multisort($extra_style_options_system_order,SORT_DESC,SORT_NUMERIC,$extra_style_options_system,SORT_STRING,SORT_ASC);
        $extra_style_options_system_group = '<optgroup label="'.T('System Styles').'">'.implode('',$extra_style_options_system).'</optgroup>';
    }

    if(count($extra_style_options_custom)<1 and count($extra_style_options_system)<1)
        unset($selected_style_info);
    ?>

    <div id="extra_style_container"<?php if($opened) echo 'class="opened"'; ?>>

        <a href="javascript: toggleExtraStyle();" id="extra_style_toggle" class="focus"></a>

        <h6><?php echo T('SELECT STYLE'); ?></h6>

        <?php if(count($extra_style_options_custom)>0 or count($extra_style_options_system)>0){ ?>
        <div id="extra_style_holder" class="selectHolder">
            <select id="extra_style" name="extra_style">
                <?php echo $extra_style_options_custom_group; ?>
                <?php echo $extra_style_options_system_group; ?>
            </select>
            <span></span>
        </div>

        <?php if(isset($tag) and $tag!=''){ ?>
        <br />
        <div id="remove_tag">
            <span><?php echo $tag; ?></span>
            <a href="<?php echo Url($url).(strpos(Url($url),'?')!==false ? '&' : '?'); ?>tag=">x</a>
        </div>
        <?php } ?>

        <hr style="position: relative; margin-top: 45px; clear: both;" />

<?php /*

        <div id="style_stabox"<?php if(!$system_style) echo ' style="display: none;'; ?>>

            <iframe src="<?php echo $styles_url.'?style='.$selected_style; ?>" style="margin: 10px 0; width: 230px; height: 42px;"></iframe>

            <hr />

        </div>

*/ ?>

        <div id="style_info_holder">
            <?php
                echo '<div id="style_info_description">'.$selected_style_info['Description'].'</div>';
                if($selected_style_info['Version']!='') echo '<div id="style_info_version">'.T('Version').' '.$selected_style_info['Version'].'</div>';
                $last_update=explode(' ',$selected_style_info['LastUpdate']);
                $last_update_date=explode('-',$last_update[0]);
                $last_update_time=explode(':',$last_update[1]);
                $timesptamp = mktime($last_update_time[0], $last_update_time[1], $last_update_time[2], $last_update_date[1], $last_update_date[2], $last_update_date[0]);
                if($selected_style_info['LastUpdate']!='') echo '<div id="style_info_last_update">'.date('Y-m-d',$timesptamp).'</div>';
                if($selected_style_info['Author']!='') echo '<div id="style_info_author">'.$selected_style_info['Author'].'</div>';
                if($selected_style_info['AuthorEmail']!='') echo '<div id="style_info_author_email"><a href="mailto:'.$selected_style_info['AuthorEmail'].'">'.$selected_style_info['AuthorEmail'].'</a></div>';
                if($selected_style_info['AuthorUrl']!='') echo '<div id="style_info_author_url"><a href="'.(strpos($selected_style_info['AuthorUrl'],'http://')!==false ? '' : 'http://').$selected_style_info['AuthorUrl'].'" target="_blank">'.$selected_style_info['AuthorUrl'].'</a></div>';
                if($selected_style_info['Tags']!=''){
                    $tag_aux = explode(',',$selected_style_info['Tags']);
                    $tags_links='';
                    for($i=0; $i<count($tag_aux); $i++)
                        $tags_links.=($tags_links!='' ? ', ' : '').'<a href="'.Url($url).(strpos(Url($url),'?')!==false ? '&' : '?').'tag='.trim($tag_aux[$i]).'">'.trim($tag_aux[$i]).'</a>';
                    echo '<div id="style_info_tags">'.$tags_links.'</div>';
                };
            ?>
        </div>

        <script>
        function toggleExtraStyle(){

            $('#extra_style_toggle, #extra_style_holder').addClass('focus');
            $('#extra_style').focus();

            if(parseInt($('#extra_style_container').css('left'))==0)
                left = '-'+($('#extra_style_container').outerWidth()+1);
            else
                var left = 0;

            $('#extra_style_container').stop().animate({
                'left' : left
            });

            $.ajax({
                type: "POST",
                url: document.URL,
                data: {
                    "ajax_call" : 'set_style_preview',
                    "opened" : left==0 ? 'true' : 'false'
                }
            });

        }

        $(function(){

            $('#extra_style_holder').addClass('focus');
            $('#extra_style').focus();

            var text = $('#extra_style').find('option:selected').text();
            $('#extra_style').next().html(text);

            $('#extra_style').change(function() {
                var text = $(this).find('option:selected').text();
                $(this).next().html(text);
                var curUrl = gdn.url(gdn.definition('Path'));
                window.location.href=curUrl+(curUrl.indexOf('?') == -1 ? '?' : '&')+'style='+$('#extra_style').val();
            });
            $('#extra_style').keyup(function() {
                var text = $(this).find('option:selected').text();
                $(this).next().html(text);
                var curUrl = gdn.url(gdn.definition('Path'));
                window.location.href=curUrl+(curUrl.indexOf('?') == -1 ? '?' : '&')+'style='+$('#extra_style').val();
            });

            $('#extra_style').focusin(function() {
                $(this).parent().addClass('focus');
            });

            $('#extra_style').focusout(function() {
                $(this).parent().removeClass('focus');

                if(parseInt($('#extra_style_container').css('left'))!=0)
                    $('#extra_style_toggle').removeClass('focus');
            });

        });
        </script>
        <?php } ?>

    </div>

<?php
}
?>