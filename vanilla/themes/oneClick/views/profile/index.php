<?php if (!defined('APPLICATION')) exit(); ?>
<div class="Profile">
   <?php
   include($this->FetchViewLocation('user'));
   if(C('ThemeOption.FilterTabs')){
     include($this->FetchViewLocation('tabs'));
     unset($this->Assets['Panel']['ProfileFilterModule']);
   }
   //echo Gdn_Theme::Module('ProfileFilterModule');
   include($this->FetchViewLocation($this->_TabView, $this->_TabController, $this->_TabApplication));
   ?>
</div>