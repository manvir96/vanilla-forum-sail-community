<?php if(!defined('APPLICATION')) exit();
/* Copyright 2013 Zachary Doll */

$Contents = $this->Data('Content');

echo '<ul class="MessageList Compact BlogList">';
foreach ($Contents as $Content) {
	static $UserPhotoFirst = NULL;
   if ($UserPhotoFirst === NULL) {
      $UserPhotoFirst = C('Vanilla.Comment.UserPhotoFirst', TRUE);
   }

   $ContentType = GetValue('ItemType', $Content);
   $ContentID = GetValue("{$ContentType}ID", $Content);
   $Author = GetValue('Author', $Content);

   switch (strtolower($ContentType)) {
      case 'comment':
         $ContentURL = CommentUrl($Content);
         break;
      case 'discussion':
         $ContentURL = DiscussionUrl($Content);
         break;
   }
?>
   <li id="<?php echo "{$ContentType}_{$ContentID}"; ?>" class="Item">
    <div class="Comment">
        <div class="Item-Header CommentHeader">
            <div class="AuthorWrap">
                <?php
                echo UserPhoto($Author);
                ?>
            </div>
        </div>
        <div class="Item-BodyWrap">
            <div class="Item-Body">

                <span class="AuthorName">
                <?php
                echo UserAnchor($Author, 'Username');
                ?>
                </span>

                <div class="Meta CommentMeta CommentInfo">

                    <span class="MItem DiscussionName">
                        <?php echo T('Comment in', 'in').' '; ?><?php echo Anchor(Gdn_Format::Text($Content['Name'], FALSE), $ContentURL); ?>
                    </span>

                    <span class="MItem DateCreated">
                        <?php echo Anchor(Gdn_Format::Date($Content['DateInserted'], 'html'), $ContentURL, 'Permalink', array('rel' => 'nofollow')); ?>
                    </span>
                    <?php
                    echo DateUpdated($Comment, array('<span class="MItem">', '</span>'));
                    ?>

                </div>

                <div class="Message">
                    <div class="Message Expander">
                      <?php echo Gdn_Format::To($Content['Body'], $Content['Format']); ?>
                     </div>
                     <?php
                     if(C('Yaga.Reactions.Enabled') && Gdn::Session()->CheckPermission('Yaga.Reactions.View')) {
                        echo RenderReactionRecord($ContentID, strtolower($ContentType));
                     }
                     ?>
                </div>

            </div>
        </div>
     </div>
   </li> <?php
}
echo '</ul>';

echo '<div class="PageControls Bottom">';
echo $this->Pager->ToString();
echo '</div>';