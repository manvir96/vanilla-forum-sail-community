<div id="Footer1">
<div class="sail_footer">
    <div class="container sail-footer-content">
        <div class="row text-left">
            <div class="col-lg-5 col-md-12">
                <div class="row">
                
                
                <div class="col-lg-12 col-md-2 white-logo footer-heading">
                    <img src="<?php echo Gdn::Request()->Domain(); ?>/<?php echo (Gdn::Request()->WebRoot() ? Gdn::Request()->WebRoot().'/' : ''); ?>themes/oneClick/views/images/sail-logo-white.png" height="55.7px" width="89.1px">
                </div>
                <div class="col-lg-12 col-md-10 about-sail-cannabis">
                    <p>Sail bridges the gap between practitioners, patients and regulated vendors as a trusted medical resource.
                        Sail simplifies the cannabis journey by providing secure, clinical & peer-peer data driven solutions.</p>
                    <div class="sail-copyright desktop-vr-copyright">Copyright © 2010 - 2017 MVC Technologies. All Rights Reserved.</div>
                </div>
                
                </div>
            </div>
            
            <div class="col-lg-7 col-md-12">
                <div class="row">
            <div class="col-4">

                <div class="footer-heading footer-heading-2">Company</div>
                <ul>
                    <li>
                        <a href="https://sailcannabis.co/careers">Careers</a>
                    </li>
                    <li>
                        <a href="https://sailcannabis.co/contact">Contact</a>
                    </li>
                    <li>
                        <a href="https://sailcannabis.co/terms">Terms of Service</a>
                    </li>
                    <li>
                        <a href="https://sailcannabis.co">Privacy Policy</a>
                    </li>

                </ul>
            </div>
            <div class="col-4">
                <div class="footer-heading footer-heading-2">Pro Solutions</div>

                <ul>
                    <li>
                        <a href="https://sailcannabis.co/physicians">Physicians</a>
                    </li>
                    <li>
                        <a href="https://sailcannabis.co/for-suppliers">Licensed Producers</a>
                    </li>

                </ul>

            </div>
            <div class="col-4">
                <div class="footer-heading footer-heading-2">Contact</div>
                <ul>
                    <li>
                        <a href="mailto:info@sailcannabis.co">info@sailcannabis.co</a>
                    </li>
                    <div class="social-icons">
                        <a href="https://www.facebook.com/sailcann/" class="social-icons" target="_blank">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                        <a href="https://twitter.com/sailcannabis" class="social-icons" target="_blank">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                        <a href="https://www.instagram.com/sailcannabis_/" class="social-icons" target="_blank">
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                    </div>

                </ul>
            </div>
</div>
                </div>
        </div>
        
        <div class="sail-copyright ipad-vr-copyright">Copyright © 2010 - 2017 MVC Technologies. All Rights Reserved.</div>
    </div>
    
    <div class="sail-footer-mobile">
    
    <div class="text-center">
         <img src="<?php echo Gdn::Request()->Domain(); ?>/<?php echo (Gdn::Request()->WebRoot() ? Gdn::Request()->WebRoot().'/' : ''); ?>themes/oneClick/views/images/sail-logo-white.png" height="55.7px" width="89.1px"><br>
         <a href="mailto:info@sailcannabis.co">info@sailcannabis.co</a><br>
         <div class="social-icons">
                        <a href="https://www.facebook.com/sailcann/" class="social-icons" target="_blank" style="margin-right:24px;">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                        <a href="https://twitter.com/sailcannabis" class="social-icons" target="_blank">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                        <a href="https://www.instagram.com/sailcannabis_/" class="social-icons" target="_blank" style="margin-left:24px;">
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                    </div>
       
               <a href="http://sailcannabis.co/terms-conditions.html">Terms of Service</a> <a>|</a> <a href="#">Privacy Policy</a>      <br>
               <div class="sail-copyright">Copyright © 2010 - 2017 MVC Technologies. All Rights Reserved.</div>
        </div>
    </div>
    
    
</div>
    
    
</div>