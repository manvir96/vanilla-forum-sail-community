<?php if (!defined('APPLICATION')) exit();
include($this->FetchViewLocation('helper_functions', 'discussions', 'vanilla'));

if(!isset($this->Assets['Panel']['DiscussionFilterModule']))
    $this->AddModule('DiscussionFilterModule', 'Panel');

if(!isset($this->Assets['Panel']['CategoriesModule']))
    $this->AddModule('CategoriesModule');

//WriteFilterTabs($this);

?>
<h1 class="H TaggedHeading"><?php printf(T('Tagged with "%s"'), htmlspecialchars($this->Tag)); ?></h1>
<?php if ($this->DiscussionData->NumRows() > 0) { ?>
<ul class="DataList Discussions">
   <?php include($this->FetchViewLocation('discussions')); ?>
</ul>
<?php
   $PagerOptions = array('RecordCount' => $this->Data('CountDiscussions'), 'CurrentRecords' => $this->Data('Discussions')->NumRows());
   if ($this->Data('_PagerUrl')) {
      $PagerOptions['Url'] = $this->Data('_PagerUrl');
   }
   echo PagerModule::Write($PagerOptions);
} else {
   ?>
   <div class="Empty"><?php printf(T('No items tagged with %s.'), htmlspecialchars($this->Tag)); ?></div>
   <?php
}
?>

<script>
$(function(){
    $('.Tags ul li').each(function(index) {
        if($(this).find('a').html()=='<?php echo htmlspecialchars($this->Tag); ?>')
            $(this).addClass('Active');
    });
});
</script>