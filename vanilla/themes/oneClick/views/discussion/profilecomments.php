<?php if (!defined('APPLICATION')) exit();
foreach ($this->Data('Comments') as $Comment) {
	$Permalink = '/discussion/comment/'.$Comment->CommentID.'/#Comment_'.$Comment->CommentID;
	$User = UserBuilder($Comment, 'Insert');
	$this->EventArguments['User'] = $User;

    $Author = Gdn::UserModel()->GetID($Comment->InsertUserID); //UserBuilder($Comment, 'Insert');

?>
<li id="<?php echo 'Comment_'.$Comment->CommentID; ?>" class="Item">
    <div class="Comment">

        <div class="Item-Header CommentHeader">
            <div class="AuthorWrap">
                <?php
                echo UserPhoto($Author);
                ?>
            </div>
        </div>
    	<?php $this->FireEvent('BeforeItemContent'); ?>
        <div class="Item-BodyWrap">
            <div class="Item-Body">

                <span class="AuthorName">
                <?php
                echo UserAnchor($Author, 'Username');
                ?>
                </span>

                <div class="Meta CommentMeta CommentInfo">

                    <span class="MItem DiscussionName">
                        <?php echo T('Comment in', 'in').' '; ?><?php echo Anchor(Gdn_Format::Text($Comment->DiscussionName), $Permalink); ?>
                    </span>

                    <span class="MItem DateCreated">
                        <?php echo Gdn_Format::Date($Comment->DateInserted, 'html'); ?>
                    </span>
                    <?php
                    echo DateUpdated($Comment, array('<span class="MItem">', '</span>'));
                    ?>

                </div>

                <div class="Message">
                <?php
                    echo Anchor(SliceString(Gdn_Format::Text(Gdn_Format::To($Comment->Body, $Comment->Format), FALSE), 250), $Permalink);;
                ?>
                </div>

            </div>
        </div>

	</div>
</li>
<?php
}