<?php if (!defined('APPLICATION')) exit();
$UserPhotoFirst = C('Vanilla.Comment.UserPhotoFirst', TRUE);

$Discussion = $this->Data('Discussion');
$Author = Gdn::UserModel()->GetID($Discussion->InsertUserID); // UserBuilder($Discussion, 'Insert');

// Prep event args.
$CssClass = CssClass($Discussion, FALSE);
$this->EventArguments['Discussion'] = &$Discussion;
$this->EventArguments['Author'] = &$Author;
$this->EventArguments['CssClass'] = &$CssClass;

// DEPRECATED ARGUMENTS (as of 2.1)
$this->EventArguments['Object'] = &$Discussion; 
$this->EventArguments['Type'] = 'Discussion';

// Discussion template event
$this->FireEvent('BeforeDiscussionDisplay');
?>
<div id="<?php echo 'Discussion_'.$Discussion->DiscussionID; ?>" class="<?php echo $CssClass; ?>">
   <div class="Discussion">
      <div class="Item-Header DiscussionHeader">
          <?php
          if (!C('ThemeOption.AdminCheckboxesInsideOptions'))
            WriteAdminCheck();
          ?>
         <div class="AuthorWrap">
            <?php
            echo UserPhoto($Author);
            echo FormatMeAction($Discussion);
            ?>
         </div>
         <?php $this->FireEvent('AfterDiscussionMeta'); // DEPRECATED ?>
      </div>
      <?php $this->FireEvent('BeforeDiscussionBody'); ?>

         <div class="Item-Body">
            <div class="Options">
            <?php
            if (C('ThemeOption.AdminCheckboxesInsideOptions'))
                WriteAdminCheck();
            $this->FireEvent('BeforeDiscussionOptions');
            //WriteBookmarkLink();
            WriteDiscussionOptions();
            ?>
            </div>
             <span class="AuthorName">
               <?php echo UserAnchor($Author, 'Username'); ?>
             </span>
             <div class="Meta DiscussionMeta">
                <?php if(GetValue('Title', $Author)!='' or GetValue('Location', $Author)!=''){ ?>
                <span class="AuthorInfo">
                   <?php
                   echo WrapIf(htmlspecialchars(GetValue('Title', $Author)), 'span', array('class' => 'MItem AuthorTitle'));
                   echo WrapIf(htmlspecialchars(GetValue('Location', $Author)), 'span', array('class' => 'MItem AuthorLocation'));
                   $this->FireEvent('AuthorInfo');
                   ?>
                </span>
                <?php } ?>
                <span class="MItem DateCreated">
                   <?php
                   echo Anchor(Gdn_Format::Date($Discussion->DateInserted, 'html'), $Discussion->Url, 'Permalink', array('rel' => 'nofollow'));
                   ?>
                </span>
                <?php
                   echo DateUpdated($Discussion, array('<span class="MItem">', '</span>'));
                ?>
                <?php
                // Include source if one was set
                if ($Source = GetValue('Source', $Discussion))
                   echo ' '.Wrap(sprintf(T('via %s'), T($Source.' Source', $Source)), 'span', array('class' => 'MItem MItem-Source')).' ';

                // Category
                if (C('Vanilla.Categories.Use')) {
                   echo ' <span class="MItem Category">';
                   echo ' '.T('in').' ';
                   echo Anchor(htmlspecialchars($this->Data('Discussion.Category')), CategoryUrl($this->Data('Discussion.CategoryUrlCode')));
                   echo '</span> ';
                }
                $this->FireEvent('DiscussionInfo');

                // USED ON HEADER INSTEAD
                //$this->FireEvent('AfterDiscussionMeta'); // DEPRECATED
                ?>
             </div>
            <div class="Message">
               <?php
                  echo FormatBody($Discussion);
               ?>
            </div>
            <?php 
            $this->FireEvent('AfterDiscussionBody');
            WriteReactions($Discussion);
            ?>
         </div>

   </div>
</div>
