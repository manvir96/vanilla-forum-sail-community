<?php include_once('style_preview.php'); ?>
<!DOCTYPE html>
<html>
<head>
   <meta name="viewport" content="width=device-width, initial-scale=1" />
   <?php $this->RenderAsset('Head'); ?>
   <meta name="msapplication-tap-highlight" content="no"/>
   <link rel="stylesheet" type="text/css" href="<?php echo Gdn::Request()->Domain(); ?>/<?php echo (Gdn::Request()->WebRoot() ? Gdn::Request()->WebRoot().'/' : ''); ?>themes/oneClick/design/base.css">
   <link rel="stylesheet" type="text/css" href="<?php echo Gdn::Request()->Domain(); ?>/<?php echo (Gdn::Request()->WebRoot() ? Gdn::Request()->WebRoot().'/' : ''); ?>themes/oneClick/design/waves.css">
   <link rel="stylesheet" type="text/css" href="<?php echo Gdn::Request()->Domain(); ?>/<?php echo (Gdn::Request()->WebRoot() ? Gdn::Request()->WebRoot().'/' : ''); ?>themes/oneClick/design/tipsy.css">
   <link rel="stylesheet" type="text/css" href="<?php echo Gdn::Request()->Domain(); ?>/<?php echo (Gdn::Request()->WebRoot() ? Gdn::Request()->WebRoot().'/' : ''); ?>themes/oneClick/design/styles/<?php echo $selected_style; ?>/css/<?php if (C('ThemeOption.PreviewStyle') and $Session->IsValid() and $Session->User->Admin){ echo 'style.css?ver='.time(); }else{ echo 'style.min.css'; } ?>">
   <script type="text/javascript" src="<?php echo Gdn::Request()->Domain(); ?>/<?php echo (Gdn::Request()->WebRoot() ? Gdn::Request()->WebRoot().'/' : ''); ?>themes/oneClick/js/jquery.easing.1.3.js"></script>
   <script type="text/javascript" src="<?php echo Gdn::Request()->Domain(); ?>/<?php echo (Gdn::Request()->WebRoot() ? Gdn::Request()->WebRoot().'/' : ''); ?>themes/oneClick/js/jquery.responsivemenu.js"></script>
   <script type="text/javascript" src="<?php echo Gdn::Request()->Domain(); ?>/<?php echo (Gdn::Request()->WebRoot() ? Gdn::Request()->WebRoot().'/' : ''); ?>themes/oneClick/js/waves.js"></script>
   <script type="text/javascript" src="<?php echo Gdn::Request()->Domain(); ?>/<?php echo (Gdn::Request()->WebRoot() ? Gdn::Request()->WebRoot().'/' : ''); ?>themes/oneClick/js/jquery.tipsy.js"></script>
   <script type="text/javascript" src="<?php echo Gdn::Request()->Domain(); ?>/<?php echo (Gdn::Request()->WebRoot() ? Gdn::Request()->WebRoot().'/' : ''); ?>themes/oneClick/js/base.js"></script>
   <script type="text/javascript" src="<?php echo Gdn::Request()->Domain(); ?>/<?php echo (Gdn::Request()->WebRoot() ? Gdn::Request()->WebRoot().'/' : ''); ?>themes/oneClick/design/styles/<?php echo $selected_style; ?>/js/script.js<?php if (C('ThemeOption.PreviewStyle') and $Session->IsValid() and $Session->User->Admin) echo '?ver='.time(); ?>"></script>
</head>
    <style>
        
    .MeBox {
    background-color: #359ADE;
}
        .NewDiscussion .Button,.NewDiscussion a.BigButton:link,.NewDiscussion a.Button:link,.NewDiscussion .Button:link,.NewDiscussion a.BigButton:visited, .NewDiscussion a.Button:visited,.NewDiscussion .Button:visited {
    background-color: #7b7ff0;
}
        .CategoryFilterOptions a{
            color: #7b7ff0;
        }
        
        dl.About dt.ProfileYearsusingcannabis {
    padding: 0;
    background-image: none;
            width: 125px;
}
        .MeButton {
    
    padding-bottom: 0px;
    padding-top: 1px;
}
    </style>
<body id="<?php echo $BodyIdentifier; ?>" class="<?php echo $this->CssClass; ?>">
   <?php if (C('ThemeOption.PreviewStyle') and $Session->IsValid() and $Session->User->Admin) style_preview($selected_style,$tag,$opened,$this->SelfUrl);?>
   <div id="Frame">
      <?php include_once('header.php'); ?>
      <div id="HeadWrapper" style="background-color: #38B2EA;">
         
         <?php $this->AddModule('MeModule','Header'); ?>
         
         <?php $this->RenderAsset('Header'); ?>
             
         <div id="Head" class="container">
<!--            <h1 id="Logo"><a href="<?php echo Url('/'); ?>"><?php echo Gdn_Theme::Logo(); ?></a></h1>-->

            <?php
			      //$Session = Gdn::Session();
					if ($this->Menu) {
						$this->Menu->AddLink('Dashboard', T('Dashboard'), '/dashboard/settings', array('Garden.Settings.Manage'), array('class' => 'Dashboard', 'title' => T('Dashboard')));
						//$this->Menu->AddLink('Dashboard', T('Users'), '/user/browse', array('Garden.Users.Add', 'Garden.Users.Edit', 'Garden.Users.Delete'));
						$this->Menu->AddLink('Activity', T('Activity'), '/activity', '', array('title' => T('Activity')));
						if ($Session->IsValid()) {
							$Name = $Session->User->Name;
							$CountNotifications = $Session->User->CountNotifications;
							if (is_numeric($CountNotifications) && $CountNotifications > 0)
								$Name .= ' <span class="Alert">'.$CountNotifications.'</span>';
                            if (urlencode($Session->User->Name) == $Session->User->Name)
                                $ProfileSlug = $Session->User->Name;
                            else
                                $ProfileSlug = $Session->UserID.'/'.urlencode($Session->User->Name);
							$this->Menu->AddLink('User', T('Profile'), '/profile/'.$ProfileSlug, array('Garden.SignIn.Allow'), array('class' => 'UserNotifications'.(strpos(Gdn::Request()->Url(), 'profile') !== FALSE ? ' Highlight' : ''), 'title' => T('Profile')));
							$this->Menu->AddLink('SignOut', T('Sign Out'), SignOutUrl(), FALSE, array('class' => 'NonTab SignOut', 'title' => T('Sign Out')));
						}else {
							$Attribs = array();
							//if (SignInPopup() && strpos(Gdn::Request()->Url(), 'entry') === FALSE)
								$Attribs['class'] = 'SignInPopup';
                            $SignInUrl = strpos(Gdn::Request()->Url(), 'entry')!==FALSE ? 'discussions' : $this->SelfUrl;
							$this->Menu->AddLink('Entry', T('Sign In'), SignInUrl($SignInUrl), FALSE, array('class' => 'NonTab SignIn', 'title' => T('Sign In')), $Attribs);
						}

                        if(Gdn::Request()->Path()=='')
                            $this->Menu->HighlightRoute('');

                        if(strpos(Gdn::Request()->Url(), 'drafts') !== FALSE or $this->ControllerName=='searchcontroller' or $this->ControllerName=='postcontroller')
                            $this->Menu->HighlightRoute('/discussions');

                        if(strpos(Gdn::Request()->Url(), 'messages') !== FALSE)
                            $this->Menu->HighlightRoute('/messages/all');

                        $this->Menu->Items['Discussions'][0]['Attributes']['class'] = 'Discussions';
                        $this->Menu->Items['Discussions'][0]['Attributes']['title'] = T('Discussions');
                        $this->Menu->Items['Activity'][0]['Attributes']['class'] = 'Activity';
                        $this->Menu->Items['Conversations'][0]['Attributes']['class'] = 'Conversations';
                        $this->Menu->Items['Conversations'][0]['Attributes']['title'] = T('Conversations');

                        if(C('EnabledPlugins.Voting')){
                            $this->Menu->Items['Discussions'][0]['Url'] = 'discussions/popular';
                            if(Gdn::Request()->Path()!='' and ($this->ControllerName=='discussionscontroller' or $this->ControllerName=='discussioncontroller' or strpos(Gdn::Request()->Url(), 'drafts') !== FALSE or $this->ControllerName=='searchcontroller' or $this->ControllerName=='postcontroller'))
                                $this->Menu->HighlightRoute('/discussions/popular');
                        }

						echo $this->Menu->ToString();
					}
				?>

            <div id="SearchBox">
                <?php
                	$Form = Gdn::Factory('Form');
                	$Form->InputPrefix = '';
                	echo
                		$Form->Open(array('action' => Url('/search'), 'method' => 'get')),
                		$Form->TextBox('Search', array('placeholder' => T('Search...'))),
                		$Form->Button('Go', array('Name' => '', 'class' => 'Form_Go')),
                        '<a id="Form_Close" href="#" class="Form_Close">Close</a>',
                		$Form->Close();
    			?>
            </div>

         </div>
      </div>
      <div id="BodyWrapper" >
          <div id="Body">
             <div id="Breadcrumbs">
                    <?php

                    // Bradcrumbs
                    $breadcrumbs = array(
                        array(
                            'Url' => Url('/', TRUE),
                            'Name' => T('Home')
                        )
                    );

                    if(Gdn::Request()->Path()!=''){

                        if($this->ControllerName=='discussionscontroller' or $this->ControllerName=='DiscussionsController' or $this->ControllerName=='discussioncontroller' or $this->ControllerName=='draftscontroller' or $this->ControllerName=='postcontroller' or $this->ControllerName=='categoriescontroller' or $this->ControllerName=='searchcontroller'){
                            $breadcrumbs[] = array(
                                    'Url' => '/discussions',
                                    'Name' => T('Discussions')
                            );

                            if($this->ControllerName=='categoriescontroller'){
                                $breadcrumbs[] = array(
                                    'Url' => '/categories',
                                    'Name' => T('All Categories')
                                );
                            }

                        }

                        if($this->ControllerName=='searchcontroller'){
                            $breadcrumbs[] = array(
                                'Url' => $this->SelfUrl.'?Search='.$this->SearchTerm,
                                'Name' => sprintf(T('Search results for \'%s\''), htmlspecialchars($this->SearchTerm))
                             );
                        }

                        if($this->Data['Tag']!=''){
                            $breadcrumbs[] = array(
                                'Url' => $this->SelfUrl,
                                'Name' => str_replace('%s',htmlspecialchars($this->Tag),T('Tagged with "%s"'))
                             );
                        }

                        $Breadcrumbs = Gdn::Controller()->Data('Breadcrumbs');

                        if(count($Breadcrumbs) > 0){
                            foreach($Breadcrumbs as $data){

                                if($data['Url']!='/discussions' and $this->ControllerName!='searchcontroller'){

                                    if($this->Data['Conversation']->ConversationID > 0 and !isset($data['Url']))
                                        $breadcrumbs_name = $this->Data['Title'];
                                    else
                                        $breadcrumbs_name = $data['Name'];

                                    $breadcrumbs[] = array(
                                        'Url' => $data['Url'],
                                        'Name' => $breadcrumbs_name
                                    );

                                }
                            }
                        }

                        if($this->ControllerName=='discussioncontroller' or $this->ControllerName=='activitycontroller' or $this->ControllerName=='draftscontroller'){

                            if($this->SelfUrl!='activity'){

                                $breadcrumbs[] = array(
                                    'Url' => $this->SelfUrl,
                                    'Name' => $this->Data['Title']
                                );

                            }
                        }

                    }

                    echo Gdn_Theme::Breadcrumbs($breadcrumbs,false);
                    ?>
             </div>
             <div id="Content">
                <?php $this->RenderAsset('Messages'); ?>
                 <div id="ContentContainer">
                    <?php $this->RenderAsset('Content'); ?>
                 </div>
             </div>
             <div id="Panel">
             <?php $this->RenderAsset('Panel'); ?>
             <?php include_once('sidebar.php'); ?>
             </div>
          </div>
      </div>
      <div id="FootWrapper">
        <?php $this->RenderAsset('Foot'); ?>
        <?php include_once('footer.php'); ?>
	  </div>
   </div>
   <?php $this->FireEvent('AfterBody'); ?>
</body>
</html>