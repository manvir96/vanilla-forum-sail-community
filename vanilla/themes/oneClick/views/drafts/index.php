<?php if (!defined('APPLICATION')) exit();
include($this->FetchViewLocation('helper_functions', 'discussions', 'vanilla'));

if(C('ThemeOption.FilterTabs'))
    unset($this->Assets['Panel']['DiscussionFilterModule']);

if(!isset($this->Assets['Panel']['TagModule']))
    $this->AddModule('TagModule');


$Session = Gdn::Session();
$ShowOptions = TRUE;
$Alt = '';
$ViewLocation = $this->FetchViewLocation('drafts', 'drafts');

if(C('ThemeOption.FilterTabs')){
    WriteFilterTabs($this);
    unset($this->Assets['Panel']['DiscussionFilterModule']);
}

if ($this->DraftData->NumRows() > 0) {
   echo $this->Pager->ToString('less');
?>
<h1 class="H"><?php echo $this->Data('Title'); ?></h1>
<ul class="MessageList DataList Drafts">
   <?php
   include($ViewLocation);
   ?>
</ul>
   <?php
   echo $this->Pager->ToString('more');
} else {
   ?>
   <div class="Empty"><?php echo T('You do not have any drafts.'); ?></div>
   <?php
}
