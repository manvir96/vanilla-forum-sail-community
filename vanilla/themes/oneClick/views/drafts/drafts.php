<?php if (!defined('APPLICATION')) exit();
$Session = Gdn::session();
$ShowOptions = TRUE;
$Alt = '';
foreach ($this->DraftData->result() as $Draft) {
    $Offset = val('CountComments', $Draft, 0);
    if ($Offset > c('Vanilla.Comments.PerPage', 30)) {
        $Offset -= c('Vanilla.Comments.PerPage', 30);
    } else {
        $Offset = 0;
    }

    $EditUrl = !is_numeric($Draft->DiscussionID) || $Draft->DiscussionID <= 0 ? '/post/editdiscussion/0/'.$Draft->DraftID : '/discussion/'.$Draft->DiscussionID.'/'.$Offset.'/#Form_Comment';
    $Alt = $Alt == ' Alt' ? '' : ' Alt';
    $Excerpt = SliceString(Gdn_Format::text($Draft->Body), 200);

    $Author = Gdn::UserModel()->GetID($Draft->InsertUserID);

    ?>
    <li class="Item Draft<?php echo $Alt; ?>">
        <div class="Item-Header CommentHeader">
            <div class="AuthorWrap">
                <?php
                echo UserPhoto($Author);
                ?>
            </div>
        </div>
        <?php $this->FireEvent('BeforeItemContent'); ?>
        <div class="Item-BodyWrap">
            <div class="Item-Body">

                <div class="Options"><?php echo anchor(t('Draft.Delete', 'Delete'), 'vanilla/drafts/delete/'.$Draft->DraftID.'/'.$Session->TransientKey().'?Target='.urlencode($this->SelfUrl), 'Delete'); ?></div>

                <span class="AuthorName">
                <?php
                echo UserAnchor($Author, 'Username');
                ?>
                </span>

                <div class="Meta CommentMeta CommentInfo">

                    <span class="MItem DiscussionName">
                        <?php echo T('Comment in', 'in').' '; ?><?php echo anchor(Gdn_Format::text($Draft->Name, false), $EditUrl); ?>
                    </span>

                    <span class="MItem DateCreated">
                        <?php echo Gdn_Format::Date($Draft->DateInserted, 'html'); ?>
                    </span>
                    <?php
                    //echo DateUpdated($Draft, array('<span class="MItem">', '</span>'));
                    ?>
                    <?php
                    if (isset($Draft->CategoryID)) {
                        $Category = CategoryModel::Categories($Draft->CategoryID);
                        if ($Category) {
                            $Url = CategoryUrl($Category);
                            echo '<span class="MItem Category">'.Anchor($Category['Name'], $Url).'</span>';
                        }
                    }
                    ?>

                </div>

                <div class="Message">
                    <?php if ($Excerpt): ?>
                    <div class="Excerpt">
                        <?php echo $Excerpt; ?>
                    </div>
                    <?php endif; ?>
                </div>

            </div>
        </div>

    </li>
<?php
}
