<?php if (!defined('APPLICATION')) exit();
$Session = Gdn::Session();

include_once $this->FetchViewLocation('helper_functions', 'discussions', 'vanilla');

if(!isset($this->Assets['Panel']['CategoriesModule']))
    $this->AddModule('CategoriesModule');

// add a filter tab area
if(C('ThemeOption.FilterTabs')){
    WriteFilterTabs($this);
    unset($this->Assets['Panel']['DiscussionFilterModule']);
}

echo '<h1 class="H HomepageTitle">';

if(C('ThemeOption.AdminCheckboxesInsideOptions'))
    echo '<div class="Options">'.AdminCheck(NULL, array('', ' ')).'</div>';
else
    echo AdminCheck(NULL, array('', ' '));

echo $this->Data('Title');

if ($Description = $this->Description()) {
   echo Wrap($Description, 'div', array('class' => 'P PageDescription'));
}

echo '</h1>';

//include $this->FetchViewLocation('Subtree', 'Categories', 'Vanilla');

$PagerOptions = array('Wrapper' => '<div class="PageControls Top"><span class="PagerNub">&#160;</span><div %1$s>%2$s</div></div>', 'RecordCount' => $this->Data('CountDiscussions'), 'CurrentRecords' => $this->Data('Discussions')->NumRows());
if ($this->Data('_PagerUrl'))
   $PagerOptions['Url'] = $this->Data('_PagerUrl');

PagerModule::Write($PagerOptions);
echo Gdn_Theme::Module('NewDiscussionModule', $this->Data('_NewDiscussionProperties', array('CssClass' => 'Button Action Primary')));

if ($this->DiscussionData->NumRows() > 0 || (isset($this->AnnounceData) && is_object($this->AnnounceData) && $this->AnnounceData->NumRows() > 0)) {
?>
<ul class="DataList Discussions">
   <?php include($this->FetchViewLocation('discussions')); ?>
</ul>
<?php

$PagerOptions = array('Wrapper' => '<div class="PageControls Bottom"><span class="PagerNub">&#160;</span><div %1$s>%2$s</div></div>', 'RecordCount' => $this->Data('CountDiscussions'), 'CurrentRecords' => $this->Data('Discussions')->NumRows());
PagerModule::Write($PagerOptions);
//echo Gdn_Theme::Module('NewDiscussionModule', $this->Data('_NewDiscussionProperties', array('CssClass' => 'Button Action Primary')));

} else {
   ?>
   <div class="Empty"><?php echo T('No discussions were found.'); ?></div>
   <?php
}
