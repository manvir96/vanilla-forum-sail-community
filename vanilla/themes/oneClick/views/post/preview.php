<?php if (!defined('APPLICATION')) exit();

$extra_id = '';
if (!function_exists('WriteCommentFormHeader')){
   include $this->FetchViewLocation('helper_functions', 'discussion');
   $extra_id = ' id="DiscussionFormPreview"';
}

$this->FireEvent('BeforeCommentPreviewFormat');
$this->Comment->Body = Gdn_Format::To($this->Comment->Body, GetValue('Format', $this->Comment, C('Garden.InputFormatter')));
$this->FireEvent('AfterCommentPreviewFormat');
?>
<div<?php echo $extra_id; ?> class="Preview ClearFix">

    <?php if($extra_id!=''){ ?>
    <div id="PopupContent">
    <?php } ?>

        <div class="MessageList">
            <?php if($extra_id!=''){ ?>
            <div class="PageTitle">
                <h1 class="H">
                <?php echo $this->Discussion->Name; ?>
                </h1>
            </div>
            <?php } ?>
            <div class="Comment">
                <?php if($extra_id!=''){ ?>
                <div class="Item-Header CommentHeader">
                    <?php WriteCommentFormHeader(); ?>
                </div>
                <?php } ?>
                <div class="Item-Body">
                    <span class="Author AuthorName">
                    <?php WriteCommentFormHeader(); ?>
                    </span>
                    <div class="Meta DiscussionMeta">
                        <span class="MItem DateCreated">
                        <?php echo Gdn_Format::Date(date('Y-m-d H:i:s'), 'html'); ?>
                        </span>
                    </div>
                    <div class="Message">
                    <?php echo $this->Comment->Body; ?>
                    </div>
                </div>
            </div>
        </div>

    <?php if($extra_id!=''){ ?>
    </div>
    <?php } ?>

</div>

<?php if($extra_id!=''){ ?>
<script>
$(function(){
    $('#Popup .Body').css('background-color',$('body').css('background-color'));
    $('#Popup .Body').css('background-image',$('body').css('background-image'));
    $('#Popup .Body').css('background-attachment',$('body').css('background-attachment'));
    $('#Popup .Body').css('background-position',$('body').css('background-position'));
    $('#Popup .Body').css('background-repeat',$('body').css('background-repeat'));
    $('#Popup .Body').css('background-size',$('body').css('background-size'));
    $('#DiscussionFormPreview').width($('#Content').width());
});
</script>
<?php } ?>