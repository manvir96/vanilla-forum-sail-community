<!DOCTYPE html>
<html>
<head>
  {asset name="Head"}

  <link rel="stylesheet" type="text/css" href="{link path="/" withdomain="true"}themes/oneClick/design/base.css">
  <link rel="stylesheet" type="text/css" href="{link path="/" withdomain="true"}themes/oneClick/design/styles/{text code="ExtraStyle" default="one-dream"}/css/style.css">
  <script type="text/javascript" src="{link path="/" withdomain="true"}themes/oneClick/js/base.js"></script>
  <script type="text/javascript" src="{link path="/" withdomain="true"}themes/oneClick/design/styles/{text code="ExtraStyle" default="one-dream"}/js/script.js"></script>

</head>
<body id="{$BodyID}" class="{$BodyClass}">

    <div id="Frame">

        <div id="HeadWrapper">
            <div id="Head">

                <h1 id="Logo"><a href="{link path="/"}">{logo}</a></h1>

                <ul id="Menu">
                    {dashboard_link format='<li class="Dashboard"><a href="%url">%text</a></li>'}
                    {discussions_link format='<li class="Discussions"><a href="%url">%text</a></li>'}
                    {activity_link format='<li class="Activity"><a href="%url">%text</a></li>'}
                    {inbox_link format='<li class="Conversations"><a href="%url">%text</a></li>'}
<!--                    {custom_menu format='<li class="Custom"><a href="%url">%text</a></li>'}-->
                    {profile_link format='<li class="UserNotifications"><a href="%url">%text</a></li>'}
                    {signinout_link format='<li class="SignOut"><a href="%url">%text</a></li>'}
                </ul>

            </div>
        </div>

        <div id="BodyWrapper">
            <div id="Body">

                <div id="Breadcrumbs">{breadcrumbs}</div>

                <div id="Content">{asset name="Content"}</div>

                <div id="Panel">
                    <div class="SearchBox">{searchbox}</div>

                    {module name="MeModule" CssClass="Box ClearFix"}

                    {asset name="Panel"}

                </div>

          </div>
      </div>

      <div id="FootWrapper">
          <div id="Foot">
                &copy Copyright 2014 - <a href="http://www.creativedreams.eu" target="blank">Creative Dreams</a> |

                <a href="{vanillaurl}" class="PoweredByVanilla" title="Community Software by Vanilla Forums">Powered by Vanilla</a>

                {asset name="Foot"}
    	  </div>
	  </div>
   </div>
   {event name="AfterBody"}
</body>
</html>