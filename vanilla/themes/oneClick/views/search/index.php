<?php if (!defined('APPLICATION')) exit();

if(!isset($this->Assets['Panel']['NewDiscussionModule']))
    $this->AddModule('NewDiscussionModule', 'Panel');

if(!isset($this->Assets['Panel']['DiscussionFilterModule']) and !C('ThemeOption.FilterTabs'))
    $this->AddModule('DiscussionFilterModule', 'Panel');

if(!isset($this->Assets['Panel']['CategoriesModule'])){
    $this->ApplicationFolder = 'vanilla';
    $this->AddModule('CategoriesModule', 'Panel');
    $this->ApplicationFolder = 'dashboard';
}

if(!isset($this->Assets['Panel']['TagModule']))
    $this->AddModule('TagModule', 'Panel');

/*
<div class="SearchForm">
<?php
$Form = $this->Form;
$Form->InputPrefix = '';
echo  $Form->Open(array('action' => Url('/search'), 'method' => 'get')),
   '<div class="SiteSearch">',
   $Form->TextBox('Search'),
   $Form->Button('Search', array('Name' => '')),
   '</div>',
   $Form->Errors(),
   $Form->Close();
?>
</div>
*/
?>

<?php
echo '<h1 class="H">'.sprintf(T('Search results for \'<b>%s</b>\''), htmlspecialchars($this->SearchTerm)).'</h1>';
?>

<?php
if (!is_array($this->SearchResults) || count($this->SearchResults) == 0) {
   echo '<p class="NoResults">', sprintf(T('No results for %s.', 'No results for <b>%s</b>.'), htmlspecialchars($this->SearchTerm)), '</p>';
} else {
   $ViewLocation = $this->FetchViewLocation('results');
   include($ViewLocation);
   echo '<div class="PageControls Bottom">';
   PagerModule::Write();
   echo '</div>';
}