<?php if (!defined('APPLICATION')) exit();
$SearchTerm = GetValue('SearchTerm', $this);
?>
<ul class="MessageList SearchResults">
<?php
if (is_array($this->SearchResults) && count($this->SearchResults) > 0) {
	foreach ($this->SearchResults as $Key => $Row) {
		$Row = (object)$Row;
		$this->EventArguments['Row'] = $Row;
?>
	<li class="Item">
        <div class="Comment">

            <div class="Item-Header CommentHeader">
                <div class="AuthorWrap">
                    <?php
                    echo UserPhoto($Row, array('Size' => 'Small'));
                    ?>
                </div>
            </div>
        	<?php $this->FireEvent('BeforeItemContent'); ?>
            <div class="Item-BodyWrap">
                <div class="Item-Body">

                    <span class="Author">
                    <?php
                    echo UserAnchor($Row);
                    ?>
                    </span>

                    <div class="Meta CommentMeta CommentInfo">

                        <span class="MItem DiscussionName">
                            <?php echo T('Comment in', 'in').' '; ?>
                            <?php echo Anchor(Gdn_Format::Text($Row->Title), $Row->Url, 'Title'); ?>
                        </span>

                        <span class="MItem DateCreated">
                            <?php echo Gdn_Format::Date($Row->DateInserted, 'html'); ?>
                        </span>

                        <?php
                        if (isset($Row->CategoryID)) {
                            $Category = CategoryModel::Categories($Row->CategoryID);
                            if ($Category) {
                                $Url = CategoryUrl($Category);
                                echo '<span class="MItem Category">'.Anchor($Category['Name'], $Url).'</span>';
                            }
                        }
                        ?>

                    </div>

                    <div class="Message">
                    <?php
                        if ($SearchTerm)
                            echo MarkString($SearchTerm, $Row->Summary);
                        else
                            echo $Row->Summary;
                    ?>
                    </div>

                </div>
            </div>

        </div>
	</li>
<?php
	}
}
?>
</ul>