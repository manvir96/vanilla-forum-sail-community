<?php if (!defined('APPLICATION')) return; ?>

<?php
// add a filter tab area
if(C('ThemeOption.FilterTabs')){
    $ViewLocation = Gdn::Controller()->FetchViewLocation('helper_functions', 'Discussions', 'vanilla');
    include_once $ViewLocation;
    WriteFilterTabs($this);
    unset($this->Assets['Panel']['DiscussionFilterModule']);
}
?>

<h1 class="H HomepageTitle"><?php echo $this->Data('Title'); ?></h1>
<?php if($this->Description()!='') echo '<div class="P PageDescription">'.$this->Description().'</div>'; ?>
<?php
$Categories = CategoryModel::MakeTree($this->Data('Categories'), $this->Data('Category', NULL));

if (C('Vanilla.Categories.DoHeadings')) {
   foreach ($Categories as $Category) {
      ?>
      <div id="CategoryGroup-<?php echo $Category['UrlCode']; ?>" class="CategoryGroup">
         <h2 class="H"><?php echo htmlspecialchars($Category['Name']); ?></h2>
         <?php
         WriteCategoryTable($Category['Children'], 2);
         ?>
      </div>
      <?php
   }
} else {
   WriteCategoryTable($Categories, 1);
}
?>