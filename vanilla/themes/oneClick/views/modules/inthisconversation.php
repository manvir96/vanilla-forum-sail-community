<?php if (!defined('APPLICATION')) exit(); ?>
<div class="Box InThisConversation">
    <?php echo panelHeading(t('In this Conversation')); ?>
    <ul class="PanelInfo">
        <?php foreach ($this->data('Participants') as $User): ?>
            <li>
                <?php
                $Username = htmlspecialchars(val('Name', $User));
                $Photo = val('Photo', $User);

                if (GetValue('Deleted', $User))
                    echo Wrap(UserPhoto($User, array('ImageClass' => 'ProfilePhotoSmall')).' '.UserAnchor($User, 'UserLink'), 'del',
                    array('title' => sprintf(T('%s has left this conversation.'), htmlspecialchars(GetValue('Name', $User))))
                );
                else
                    echo UserPhoto($User, array('ImageClass' => 'ProfilePhotoSmall')).' '.UserAnchor($User, 'UserLink');
                ?>
            </li>
        <?php endforeach; ?>
    </ul>
</div>
