<?php if (!defined('APPLICATION')) exit();
$this->CheckPermissions();

// Title on the PROFILE > EDIT
if(Gdn::Controller()->ControllerName=='profilecontroller' and  Gdn::Controller()->EditMode==1)
    $this->Items['Options']['Text'] = T('Edit Profile');

// Loop through all the groups.
foreach ($this->Items as $Item) {
   // Output the group.
   echo '<div class="Box Group '.GetValue('class', $Item['Attributes']).'">';
   if ($Item['Text'] != '')
      echo "\n", '<h4>',
         isset($Item['Url']) ? Anchor($Item['Text'], $Item['Url']) : $Item['Text'],
         '</h4>';

   if (count($Item['Links'])) {
      echo "\n", '<ul class="PanelInfo">';

      // Loop through all the links in the group.
      foreach ($Item['Links'] as $key => $Link) {

        // Put the Active class on PROFILE > EDIT > NOTIFICATIONS PREFERENCES
        if(strpos(Gdn::Request()->Path(),'profile/preferences')!==FALSE and strpos($key,'profile/preferences')!==FALSE)
            $Link['Attributes']['class'] .= ' Active';

         echo "\n  <li".Attribute($Link['Attributes']).">",
            Anchor($Link['Text'], $Link['Url']),
            '</li>';
      }

      echo "\n", '</ul>';
   }

   echo "\n", '</div>';
}
