
<div class="Top" style="font-family: 'Open Sans',serif;">
       
            <div class="top-bar" >
    <div class="container">
        <a href="https://sailcannabis.co/physicians">For Physicians</a>
        <a style="margin-left: 24px" href="https://sailcannabis.co/for-suppliers">For Suppliers</a>
		<a class="top-links" style="margin-left: 24px" href="https://sailcannabis.co/careers">Careers</a>
		<a class="top-links" style="margin-left: 24px" href="https://sailcannabis.co/contact">Contact</a>
    </div>
</div>
        
    </div>
<section class="main-header " style="background-color: white; padding-bottom: 23px; padding-top: 18px; font-family: 'Open Sans',serif;">
    <div class="header container">
        <header class="row">
            <div class="header col-1">
                <div class="logo">
                    <a href="http://sailcannabis.co/">
                        <img src="https://sailcannabis.co/assets/images/sail-logo@2x.png" height="56px" />
                    </a>
                </div>
            </div>
            <div class="col-11 text-right topnav-1">
                <div class="">
                    <span class="header cannabis-journey">Simplifying the cannabis journey.</span>
					
                <a class="header icons" href="https://www.facebook.com/sailcann/" class="icn-faebook" target="_blank"><i onMouseOver="this.style.color='#3B5998'" onMouseOut="this.style.color='#8492a6'"  style="margin-right: 14px; margin-left: 14px; font-size: 17px; color: #8492a6" class="fa fa-facebook" aria-hidden="true"></i></a>
                <a class="header icons" href="https://twitter.com/sailcannabis" class="icn-titter" target="_blank"><i onMouseOver="this.style.color='#4099FF' " onMouseOut="this.style.color='#8492a6'" style="margin-right: 14px; font-size: 17px; color: #8492a6" class="fa fa-twitter" aria-hidden="true"></i></a>
                <a class="header icons" href="https://www.instagram.com/sailcannabis_/" class="icn-insagram" target="_blank"
                   style="margin-right: 0px;"><i onMouseOver="this.style.color='#517fa4' " onMouseOut="this.style.color='#8492a6'" style="margin-right: 14px;  font-size: 17px; color: #8492a6" class="fa fa-instagram" aria-hidden="true"></i></a>
           
                    <a href="https://sailportal.co" style="font-family: 'Open Sans',serif;" class="login-button button" target="_blank">Canadian Login / Register</a>
                    <a href="https://sailportal.net" style="font-family: 'Open Sans',serif;" class="login-button button" target="_blank">American Login / Register</a>

                </div>
            </div>
        </header>


        <hr class="header">
        <div class="header topnav" id="myTopnav">

           
 <div class="dropdown">
                <a href="https://sailcannabis.co/research" class="active">Research<span class="arrow">▼</span></a>

                <div class="dropdown-content">
                    <div><a href="https://sailcannabis.co/research/pain"><span>Pain Management</span></a></div>
                    <!--<div><a href="#"><span>Mood Disorders</span></a></div>-->
                    <div><a href="https://sailcannabis.co/research/mood"><span>Mood Disorders</span></a></div>
                    <div><a href="https://sailcannabis.co/research/ep"><span>Epilepsy</span></a></div>
					<div><a href="https://sailcannabis.co/research/gi"><span>Gastrointestinal</span></a></div>
                    <div><a href="https://sailcannabis.co/research/can"><span>Cancer</span></a></div>
                    <div>
                        <a href="https://sailcannabis.co/research/ms"><span>Multiple Sclerosis</span></a>
                    </div>

                </div>


            </div>
            <!--<a href="#">Forum</a>-->
<!--            <a href="http://sailcannabis.co/">Articles</a>-->
            <a href="https://sailcannabis.cmnty.com/home" target="_blank">Community</a>
            <a href="https://sailcannabis.co/webinars">Webinars</a>
            <a href="https://sailcannabis.co/clinics" class="find-clinic-menu">Find A Clinic</a>  
			<a href="https://sailcannabis.co/suppliers" class="find-clinic-menu">Find a Regulated Supplier</a>

         <!--   <div class="social-icons">
                <a href="https://www.facebook.com/sailcann/" class="icn-faebook" target="_blank"><i onMouseOver="this.style.color='#3B5998'" onMouseOut="this.style.color='#8492a6'"  style="font-size: 24px;" class="fa fa-facebook" aria-hidden="true"></i></a>
                <a href="https://twitter.com/sailcannabis" class="icn-titter" target="_blank"><i onMouseOver="this.style.color='#4099FF' " onMouseOut="this.style.color='#8492a6'" style="font-size: 24px;" class="fa fa-twitter" aria-hidden="true"></i></a>
                <a href="https://www.instagram.com/sailcannabis_/" class="icn-insagram" target="_blank"
                   style="margin-right: 0px;"><i style="font-size: 24px;" class="fa fa-instagram" aria-hidden="true"></i></a>
            </div> -->
            

            
        </div>


        <!---------------------------sidebar menu----------------------->
 
      
        <!-------------sidebar menu---------------------->


    </div>
	
<div class="header-mobile container">
    <header class="row">
            <div class="col-2">
                <div class="logo">
                    <a href="http://sailcannabis.co/">
                        <img src="https://sailcannabis.co/assets/images/sail-logo@2x.png" height="56px" />
                    </a>
                </div>
            </div>
            <div class="col-10 text-right mobile-menu-btn">
			<div class="row text-right">
			 <div class="col-md-10 col-9 text-right topnav-1 ipad-login" style="padding-top: 5px;">
                <div class="">
                              
                    <a href="https://sailportal.co" style="font-family: 'Open Sans',serif;" class="login-button button" target="_blank">Canadian Login / Register</a>
                    <a href="https://sailportal.net" style="font-family: 'Open Sans',serif;" class="login-button button" target="_blank">American Login / Register</a>

                </div>
            </div>
			<div class="col-md-2 col-3">
                <a href="javascript:void(0);" style="font-size:15px;" class="icon" onclick="showSidebar()"><img width="32" height="32"
                    src="<?php echo Gdn::Request()->Domain(); ?>/<?php echo (Gdn::Request()->WebRoot() ? Gdn::Request()->WebRoot().'/' : ''); ?>themes/oneClick/views/images/btn-menu@3x.png" class="btn-menu"></a>
					</div>
			</div>
			
            </div>
        </header>
    <div id="hidden-side-menu" class="sidebar-menu responsive hide-me">


            <div class="sidebar-h-menu container">

                <div class="sidebar-close-btn text-right" onclick="closeSidebar()">&#10006</div>

                <div class="sidebar-menu-navigation text-center">
                    <a href="https://sailportal.co" class="button" target="_blank">Canadian Login / Register</a><br><br>
                    <a href="https://sailportal.net" class="button" target="_blank" >American Login / Register</a><br><br>

                    <a href="https://sailcannabis.co/research">Research</a><br><br>
                    <!--<a href="#">Mood Disorders</a><br><br>
                    <a href="http://sailcannabis.co/Articles/Mood_Disorder/Mood_Disorders.html">Mood Disorders</a><br><br>
                    <a href="http://sailcannabis.co/Articles/Epilepsy/epilepsy.html">Epilepsy</a><br><br>
                    <a href="http://sailcannabis.co/Articles/Cancer/cancer.html"><span>Cancer</span></a><br><br>
                    <a href="http://sailcannabis.co/Articles/MultipleSclerosis/Multiple_Sclerosis.html"><span>Multiple Sclerosis</span></a><br><br>-->

                    <a href="https://sailcannabis.cmnty.com/authorize/login" target="_blank">Community</a><br><br>

                    <a href="https://sailcannabis.co/webinars">Webinar</a><br><br>
                    <a href="https://sailcannabis.co/clinics">Find A Clinic</a><br><br>
					 <a href="https://sailcannabis.co/suppliers" class="find-clinic-menu">Find a Regulated Supplier</a><br><br>
					 <a href="https://sailcannabis.co/media">Media & News</a><br><br>
					  <a href="https://sailcannabis.co/careers">Careers</a><br><br>
					   <a href="https://sailcannabis.co/contact">Contact</a><br><br><br>

                    <div class="sidebar-social-icons">
                        <a href="https://www.facebook.com/sailcann/" target="_blank" style="padding-right: 24px;">
                            <i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href="https://twitter.com/sailcannabis" target="_blank"> 
                            <i class="fa fa-twitter" aria-hidden="true"></i></a>
                        <a href="https://www.instagram.com/sailcannabis_/" target="_blank" style="padding-left: 24px;"> 
                            <i class="fa fa-instagram" aria-hidden="true"></i></a>
                    </div>
                </div>

            </div>

        </div>
    </div>

</section>
<script>
$(document).ready(function(){
        setMenuHeight();
    });

    $( window ).resize(function() {
        setMenuHeight();
    });

    function setMenuHeight(){
        var screenHeight = $( window ).height();
        $(".sidebar-menu-navigation").height(screenHeight - 100);
    }
function showSidebar(){
    
   

    var x = document.getElementById("hidden-side-menu");
        $(".sidebar-menu-navigation").css('display', 'block');

        if (x.className = "sidebar-menu responsive hide-me out") {

            $(".responsive").removeClass("out").addClass("active");
        }
        else {
            $(".responsive").addClass("active");
        }

        $(".responsive").show();
}

 function closeSidebar() {


        var x = document.getElementById("hidden-side-menu");

        $(".sidebar-menu-navigation").css('display', 'none');
        $(".responsive").removeClass("active").addClass("out");


    }
</script>