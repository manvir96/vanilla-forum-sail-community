<?php

if(isset($_POST['ajax_call']) or isset($_GET['ajax_call']))
    include_once('../../../../themes/oneClick/includes/functions.php');

if(isset($_POST['ajax_call']) and $_POST['ajax_call']=='get_style_info' and isset($_POST['current_style']) and $_POST['current_style']!=''){

    $styles_dir = '../../../../themes/oneClick/design/styles';

    include($styles_dir.'/'.$_POST['current_style'].'/style.php');
    $style_info = current($StyleInfo);

    $out = $style_info['Name'].'#@#'.$style_info['Description'].'#@#'.$style_info['Version'].'#@#'.formatDate($style_info['LastUpdate'],'Y-m-d').'#@#'.$style_info['Author'].'#@#'.$style_info['AuthorUrl'].'#@#'.$style_info['Tags'];

    echo $out;

    exit;

}

if(isset($_POST['ajax_call']) and $_POST['ajax_call']=='change_preview_style' and isset($_POST['preview_style']) and $_POST['preview_style']!=''){

    SaveToConfigFromAjax('ThemeOption.PreviewStyle',$_POST['preview_style'] ? TRUE : FALSE);

    exit;

}

if(isset($_POST['ajax_call']) and $_POST['ajax_call']=='get_css' and isset($_POST['extra_style']) and $_POST['extra_style']!=''){

    $styles_dir = '../../../../themes/oneClick/design/styles';

    // STYLE INFO
    include($styles_dir.'/'.$_POST['extra_style'].'/style.php');
    $style_info = current($StyleInfo);

    $out = '';

    $out .= $style_info['Name'].'#@#'.$style_info['Description'].'#@#'.$style_info['Version'].'#@#'.formatDate($style_info['LastUpdate'],'Y-m-d').'#@#'.$style_info['Author'].'#@#'.$style_info['AuthorUrl'].'#@#'.$style_info['Tags'];

    $out .= '##@@##';

    // READ THE STYLE CSS
    $filename = $styles_dir.'/'.$_POST['extra_style'].'/css/style.css';
    $handle = @fopen($filename, "r");

    if ($handle) {

        SaveToConfigFromAjax('ThemeOption.ExtraStyle',$_POST['extra_style']);

        $i=-1;
        $j=0;
        $start=false;
        $title='';
        $inside = false;
        $elements = array();
        $extra_css='';

        while (!feof($handle)) {
            $buffer = fgets($handle, 1024);

            if(strpos($buffer,'/* ================== EXTRA CSS ================== */')!==false or $extra_css!='')
                $extra_css.=$buffer;

            if((strpos($buffer,'/*')!==false or $title!='') and !$inside ){
                $title.=$buffer;
                $j=0;
            }

            if(strpos($buffer,'*/')!==false and !$inside and $extra_css==''){

                if($start){
                    $i++;
                    $title = trim(str_replace('/*','',str_replace('*/','',$title)));
                    preg_match('/((- )*)([^-])(.*)/', $title, $matches);
                    $elements[$i]['level'] = substr_count($matches[1],'-');
                    $title=$matches[3].$matches[4];
                    $elements[$i]['title'] = trim(str_replace('/*','',str_replace('*/','',$title)));
                }

                $title='';

            }

            if($start and $inside and $extra_css==''){
                if(strpos($buffer,':')!==false){
                    $elements[$i]['attributes'][$j] = trim(substr($buffer,0,strpos($buffer,':')));
                    $elements[$i]['values'][$j] = trim(str_replace('!important','',substr($buffer,strpos($buffer,':')+1,strpos($buffer,';')-strpos($buffer,':')-1)));
                    $elements[$i]['description'][$j] = trim(str_replace('/*','',str_replace('*/','',substr($buffer,strpos($buffer,';')+1))));
                    $j++;
                }
            }

            if(strpos($buffer,'}')!==false)
                $inside = false;

            if(strpos($buffer,'{')!==false and strpos($buffer,'@media')===false)
                $inside = true;

            if(strpos($buffer,'/* ==================== STYLE ==================== */')!==false)
                $start=true;

        }
        fclose($handle);
    }

    $ce = new cssEditor();
    $ce->custom_style=$_POST['extra_style'];

    $i = 0;
    foreach ($elements as $element) {

        if($i>0 and $last_level>=$element['level'])
            $out .= str_repeat('</div></div>',$last_level-$element['level']+1);

        $out .= '<div class="level'.$element['level'].'">';

        $out .= '<label id="attr_'.$i.'" rel="'.str_replace(' ','_',strtolower($element['title'])).'">'.$element['title'].'</label>';

        $out .= '<div class="attributes_container">';

        // loop through each style and split apart the key from the value
        if(isset($element['values']) and count($element['values'])>0 and is_numeric($_POST['attribute']) and $_POST['attribute']>=0 and $i==$_POST['attribute']){
            $last_element_group = '';
            for ($j=0; $j<count($element['values']); $j++) {

                $element_description = trim($element['description'][$j]);

                $element_group = '';
                $element_subdescription = '';
                if(strpos($element_description,':')!==false){
                    $element_group = substr($element_description,0,strpos($element_description,':'));
                    $element_subdescription = substr($element_description,strpos($element_description,':')+1);
                    if(strpos($element_subdescription,'[')!==false)
                        $element_subdescription = substr($element_subdescription,0,strpos($element_subdescription,'['));
                    $element_description = $element_group;
                }
                if(strpos($element_description,'[')!==false)
                    $element_description = trim(substr($element_description,0,strpos($element_description,'[')));
                if(trim($element_description)=='')
                    $element_description = ucwords(str_replace('-',' ',trim($element['attributes'][$j])));

                if($last_element_group=='' or $last_element_group!=$element_group){

                    if(($last_element_group!=$element_group and $last_element_group!='') or ($last_element_group!='' and $element_group==''))
                        $out_attr .= '</div>';

                    if($j>0)
                        $out_attr .= '</div>';

                    $out_attr .= '<div class="separator">';

                    $out_attr .= '<div class="attribute'.($element_group!='' ? ' group_attribute' : '').'">'.$element_description.'</div>';
                }

                if($element_group!=''){

                    if($last_element_group!=$element_group)
                        $out_attr .= '<div class="group_value_container">';

                    $out_attr .= '<div class="group_value">';

                    $out_attr .= trim($element_subdescription);

                    $out_attr .= $ce->get($element['attributes'][$j],'attributes['.$i.']['.$j.']',$element['values'][$j],$element['description'][$j]);

                    $out_attr .= '</div>';

                }else{

                    $out_attr .= $ce->get($element['attributes'][$j],'attributes['.$i.']['.$j.']',$element['values'][$j],$element['description'][$j]);

                }

                $last_element_group = $element_group;

            }

            if($last_element_group!='')
                $out_attr .= '</div>';

            $out_attr .= '</div>';

        }

        $i++;

        $last_level = $element['level'];

    }

    $out .= str_repeat('</div></div>',$last_level+1);

    $out .= '<div class="level0">
                <label>EXTRA CSS</label>
                <div class="attributes_container" style="display: none;">
                    <div class="separator">
                        <div class="attribute">Extra CSS</div>
                        <textarea name="attributes['.$i.'][extra-css]">'.trim(str_replace('/* ================== EXTRA CSS ================== */','',$extra_css)).'</textarea>
                    </div>
                </div>
            </div>';


    $filename = $styles_dir.'/'.$_POST['extra_style'].'/js/script.js';
    $extra_javascript = file_get_contents($filename);

    $out .= '<div class="level0">
                <label>EXTRA JAVASCRIPT</label>
                <div class="attributes_container" style="display: none;">
                    <div class="separator">
                        <textarea name="extra_javascript">'.$extra_javascript.'</textarea>
                        <div class="attribute">Extra Javascript</div>
                    </div>
                </div>
            </div>';

    if(is_numeric($_POST['attribute']) and $_POST['attribute']>=0)
        echo $out_attr;
    else
        echo $out;

    exit;
}

if(isset($_GET['ajax_call']) and $_GET['ajax_call']=='set_css'){

    $styles_dir = '../../../../themes/oneClick/design/styles';

    define('APPLICATION', 'Vanilla');
    include_once('../../../../library/core/class.format.php');
    $gdn_format = new Gdn_Format;
    $safe_filename = Gdn_Format::Clean($_POST['style_name']);

    $last_update = date('Y-m-d H:i:s');

    if($_GET['option']=='new'){

        if($safe_filename!='' and $safe_filename[0]!='.'){

            $dir_found = false;
            if ($handle = opendir($styles_dir)) {
                while (false !== ($file = readdir($handle)) and !$dir_found) {
                    if ($file != "." and $file != ".." and is_dir($styles_dir.'/'.$file) and $file==$safe_filename)
                        $dir_found=true;
                }
                closedir($handle);
            }

            if($dir_found)
                exit('0');

            mkdir($styles_dir.'/'.$safe_filename,'0755');
    /*
            mkdir($styles_dir.'/'.$safe_filename.'/css','0755');
            mkdir($styles_dir.'/'.$safe_filename.'/img','0755');
            mkdir($styles_dir.'/'.$safe_filename.'/js','0755');
    */
            rcopy($styles_dir.'/'.$_POST['extra_style'], $styles_dir.'/'.$safe_filename);


            $filename = $styles_dir.'/'.$safe_filename.'/style.php';
            $filecontent = "<?php

\$StyleInfo['".$safe_filename."'] = array(
    'Name'        => '".strip_tags($_POST['style_name'])."',
    'Description' => '".strip_tags($_POST['style_description'],'<br>')."',
    'Version'     => '".strip_tags($_POST['version'])."',
    'DateCreated' => '".$last_update."',
    'LastUpdate'  => '".$last_update."',
    'Author'      => '".strip_tags($_POST['author'])."',
    'AuthorUrl'   => '".strip_tags($_POST['author_url'])."',
    'Tags'        => '".strip_tags($_POST['tags'])."',
    'StyleEditorVersion' => '".$style_editor_version."'
);

?>";
            $h = fopen($filename, 'w');
            fwrite($h, $filecontent);
            fclose($h);

        }

    }else{

        include($styles_dir.'/'.$_POST['extra_style'].'/style.php');
        $style_info = current($StyleInfo);
        if(isset($style_info['System']) and $style_info['System'])
            exit('1');

        if($_POST['extra_style']!=$safe_filename){

            if($safe_filename!='' and $safe_filename[0]!='.'){

                $dir_found = false;
                if ($handle = opendir($styles_dir)) {
                    while (false !== ($file = readdir($handle)) and !$dir_found) {
                        if ($file != "." and $file != ".." and is_dir($styles_dir.'/'.$file) and $file==$safe_filename)
                            $dir_found=true;
                    }
                    closedir($handle);
                }

                if($dir_found)
                    exit('0');

                rename($styles_dir.'/'.$_POST['extra_style'] , $styles_dir.'/'.$safe_filename);

            }

        }

        if($_GET['update']=='true'){
            $filename = $styles_dir.'/'.$safe_filename.'/style.php';
            $filecontent = file_get_contents($filename);

            if($_POST['style_name']!=''){
                $filecontent = preg_replace('/\$StyleInfo\[\'(.*)\'\]/','$StyleInfo[\''.$safe_filename.'\']',$filecontent);
                $filecontent = preg_replace("/'Name'\s*=>\s*'(.*)',/","'Name'  => '".$_POST['style_name']."',",$filecontent);
                $filecontent = preg_replace("/'Description'\s*=>\s*'(.*)',/","'Description'  => '".$_POST['style_description']."',",$filecontent);
                $filecontent = preg_replace("/'Version'\s*=>\s*'(.*)',/","'Version'  => '".$_POST['version']."',",$filecontent);
                $filecontent = preg_replace("/'LastUpdate'\s*=>\s*'(.*)',/","'LastUpdate'  => '".$last_update."',",$filecontent);
                $filecontent = preg_replace("/'Author'\s*=>\s*'(.*)',/","'Author'  => '".$_POST['author']."',",$filecontent);
                $filecontent = preg_replace("/'AuthorUrl'\s*=>\s*'(.*)',/","'AuthorUrl'  => '".$_POST['author_url']."',",$filecontent);
                $filecontent = preg_replace("/'Tags'\s*=>\s*'(.*)'/","'Tags'  => '".$_POST['tags']."'",$filecontent);
            }else{
                $filecontent = preg_replace("/'LastUpdate'\s*=>\s*'(.*)',/","'LastUpdate'  => '".$last_update."',",$filecontent);
            }

            $h = fopen($filename, 'w');
            fwrite($h, $filecontent);
            fclose($h);
        }

        include($styles_dir.'/'.$safe_filename.'/style.php');
        $style_info = current($StyleInfo);
    }

    //if($_GET['update']=='true'){


        $selected_fonts = array();

        $out='';

        $styles_dir = '../../../../themes/oneClick/design/styles';
        $filename = $styles_dir.'/'.$safe_filename.'/css/style.css';

        $handle = @fopen($filename, "r");

        if ($handle) {

            $i=-1;
            $j=0;
            $start = false;
            $start_header = false;
            $title='';
            $inside = false;
            $elements = array();
            $out_header='';
            $extra_css='';
            $matches=Array();

            while (!feof($handle)) {
                $buffer = fgets($handle, 1024);

                if(strpos($buffer,'================== ONE CLICK STYLE ==================')!==false)
                    $start_header = true;

                if(strpos($buffer,'/* ================== EXTRA CSS ================== */')!==false or $extra_css!='')
                    $extra_css.=$buffer;

                if($start_header){
                    if(strpos($buffer,'*/')===false)
                        $out_header.=$buffer;
                    else
                        $start_header = false;
                }

                if((strpos($buffer,'/*')!==false or $title!='') and !$inside){
                    if(strpos($buffer,'*/')===false)
                        $title.=$buffer;
                    $j=0;
                }

                if(strpos($buffer,'*/')!==false and !$inside and $extra_css==''){

                    if($start){
                        $i++;
                        $title = trim(str_replace('/*','',str_replace('*/','',$title)));
                        preg_match('/((- )*)([^-])(.*)/', $title, $matches);
                        if(count($matches)>0){
                            $elements[$i]['level'] = substr_count($matches[1],'-');
                            $title=$matches[3].$matches[4];
                        }
                        $elements[$i]['title'] = trim(str_replace('/*','',str_replace('*/','',$title)));
                    }

                    $title='';

                }

                if($start and $inside and $extra_css==''){

                    if(strpos($buffer,':')!==false){
                        $old_value = trim(str_replace('!important','',substr($buffer,strpos($buffer,':')+1,strpos($buffer,';')-strpos($buffer,':')-1)));

                        $attribute = trim(substr($buffer,0,strpos($buffer,':')));
                        if($attribute=='font-family')
                            $selected_fonts[$i]=(isset($selected_fonts[$i]) ? array_merge($selected_fonts[$i], array('name' => isset($_POST['attributes'][$i][$j]) ? $_POST['attributes'][$i][$j] : $old_value)) : array('name' => isset($_POST['attributes'][$i][$j]) ? $_POST['attributes'][$i][$j] : $old_value));
                        if($attribute=='font-weight')
                            $selected_fonts[$i]=(isset($selected_fonts[$i]) ? array_merge($selected_fonts[$i], array('weight' => isset($_POST['attributes'][$i][$j]) ? $_POST['attributes'][$i][$j] : $old_value)) : array('weight' => isset($_POST['attributes'][$i][$j]) ? $_POST['attributes'][$i][$j] : $old_value));
                        if($attribute=='font-style')
                            $selected_fonts[$i]=(isset($selected_fonts[$i]) ? array_merge($selected_fonts[$i], array('style' => isset($_POST['attributes'][$i][$j]) ? $_POST['attributes'][$i][$j] : $old_value)) : array('style' => isset($_POST['attributes'][$i][$j]) ? $_POST['attributes'][$i][$j] : $old_value));
                        if($attribute=='font-stretch')
                            $selected_fonts[$i]=(isset($selected_fonts[$i]) ? array_merge($selected_fonts[$i], array('stretch' => isset($_POST['attributes'][$i][$j]) ? $_POST['attributes'][$i][$j] : $old_value)) : array('stretch' => isset($_POST['attributes'][$i][$j]) ? $_POST['attributes'][$i][$j] : $old_value));
                        if($attribute=='font-variant')
                            $selected_fonts[$i]=(isset($selected_fonts[$i]) ? array_merge($selected_fonts[$i], array('variant' => isset($_POST['attributes'][$i][$j]) ? $_POST['attributes'][$i][$j] : $old_value)) : array('variant' => isset($_POST['attributes'][$i][$j]) ? $_POST['attributes'][$i][$j] : $old_value));

                            //$background_path = substr($old_value,0,strrpos($old_value,'/')+1);
                            //$out .= str_replace($old_value,$background_path.$_POST['attributes'][$i][$j].')',$buffer);

                        if(isset($_POST['attributes'][$i][$j])){

                            $new_value = $_POST['attributes'][$i][$j];

                            if(substr($attribute,-5)=='color' and strtolower($new_value)=='transpa')
                                $new_value = str_replace('transpa','transparent',strtolower($new_value));

                            if($old_value==''){
                                $space=substr($buffer,strpos($buffer,':')+2,1);
                                $pos=($space==';' or $space==' ') ? 2 : 1;
                                $out .= substr($buffer,0,strpos($buffer,':')+$pos).$new_value.substr($buffer,strpos($buffer,':')+$pos);
                            }else{
                                $out .= str_replace(': '.$old_value,': '.$new_value,$buffer);
                            }
                        }else{
                            $out .= $buffer;
                        }

                        $j++;
                    }else{
                        $out .= $buffer;
                    }

                }else{
                    if($start and $extra_css=='')
                        $out .= $buffer;
                }

                if(strpos($buffer,'}')!==false)
                    $inside = false;

                if(strpos($buffer,'{')!==false and strpos($buffer,'@media')===false)
                    $inside = true;

                if(strpos($buffer,'/* ==================== STYLE ==================== */')!==false)
                    $start=true;

            }
            fclose($handle);

            $out_header = preg_replace("/Name:(.*)/","Name: ".$safe_filename,$out_header);
            $out_header = preg_replace("/Description:(.*)/","Description: ".$_POST['style_description'],$out_header);
            $out_header = preg_replace("/Version:(.*)/","Version: ".$_POST['version'],$out_header);
            $out_header = preg_replace("/Last Update:(.*)/","Last Update: ".date('Y-m-d H:i:s'),$out_header);
            $out_header = preg_replace("/Author:(.*)/","Author: ".$_POST['author'],$out_header);
            $out_header = preg_replace("/Author Url:(.*)/","Author Url: ".$_POST['author_url'],$out_header);
            $out_header = preg_replace("/Tags:(.*)/","Tags: ".$_POST['tags'],$out_header);

            $ce = new cssEditor();
            $out_final = '/*'.PHP_EOL.$out_header.'*/'.PHP_EOL.PHP_EOL.PHP_EOL;
            $out_final .= '/* ==================== FONTS ==================== */'.PHP_EOL.PHP_EOL.$ce->getFontsCode($selected_fonts).PHP_EOL.PHP_EOL;
            $out_final .= '/* ==================== STYLE ==================== */'.PHP_EOL.$out;
            $out_final .= '/* ================== EXTRA CSS ================== */'.PHP_EOL.PHP_EOL.$_POST['attributes'][$i+1]['extra-css'];

            $handle = @fopen($filename, "w");
            fwrite($handle, $out_final);
            fclose($handle);

            $filename = $styles_dir.'/'.$safe_filename.'/css/style.min.css';
            $handle = @fopen($filename, "w");
            $out_final = cssMinify($out_final);
            fwrite($handle, $out_final);
            fclose($handle);

        }

    //}

    if($safe_filename!=''){
        if($_GET['update']=='true'){
            $filename = $styles_dir.'/'.$safe_filename.'/js/script.js';
            $h = fopen($filename, 'w');
            fwrite($h, $_POST['extra_javascript']);
            fclose($h);
        }
    }

    if($safe_filename!='')
        SaveToConfigFromAjax('ThemeOption.ExtraStyle',$safe_filename);

    echo '#@#'.$safe_filename.'#@#'.formatDate($last_update,'Y-m-d');
    exit;
}

if(isset($_GET['ajax_call']) and $_GET['ajax_call']=='delete_css'){
    $styles_dir = '../../../../themes/oneClick/design/styles';

    $count_dirs = 0;
    if ($handle = opendir($styles_dir)) {
        while (false !== ($file = readdir($handle))) {
            if ($file != "." and $file != ".." and is_dir($styles_dir.'/'.$file))
                $count_dirs++;
        }
        closedir($handle);
    }

    if($count_dirs>2){
        $filename = $styles_dir.'/'.$_POST['extra_style'];
        rrmdir($filename);
    }
    exit;
}

if(isset($_GET['ajax_call']) and $_GET['ajax_call']=='delete_custom_image'){
    $styles_dir = '../../../../themes/oneClick/design/styles';
    if($_POST['image']!=''){
        $image = basename(substr($_POST['image'],4,-1));
        $file_ext = substr($image,strrpos($image,'.')+1);
        if($file_ext=='png' or $file_ext=='jpg' or $file_ext=='jpeg' or $file_ext=='gif'){
            $filename = $styles_dir.'/'.$_POST['extra_style'].'/'.$_POST['upload-dir'].$image;
            if(file_exists($filename))
                unlink($filename);
        }else{
            echo 'Image extension is not a png, jpg, jpeg or gif.';
        }
    }else{
        echo 'Image not selected';
    }

    exit;
}

if(isset($_GET['ajax_call']) and $_GET['ajax_call']=='export_style'){
    $styles_dir = '../../../../themes/oneClick/design/styles';

    if($_GET['extra_style']!='' and $_GET['extra_style'][0]!='.' and is_dir($styles_dir.'/'.$_GET['extra_style'])){

        Zip($styles_dir.'/'.$_GET['extra_style'],$styles_dir.'/'.$_GET['extra_style'].'.zip');

        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-type: application/octet-stream");
        header("Content-Disposition: attachment; filename=\"".$_GET['extra_style'].'.zip'."\"");
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: ".filesize($styles_dir.'/'.$_GET['extra_style'].'.zip'));
        ob_end_flush();

        readfile($styles_dir.'/'.$_GET['extra_style'].'.zip');

        unlink ($styles_dir.'/'.$_GET['extra_style'].'.zip');

        exit;

    }else{

        exit(0);

    }

}

if(isset($_GET['ajax_call']) and $_GET['ajax_call']=='import_style'){
    $styles_dir = '../../../../themes/oneClick/design/styles';

    if(strpos($_POST['style_file'],'#@#')!==false)
        $style_files = explode('#@#',$_POST['style_file']);
    else
        $style_files[0] = $_POST['style_file'];

    foreach($style_files as $style_file){

        if($style_file!='' and $style_file[0]!='.' and is_file($styles_dir.'/oc_tmp_import/'.$style_file)){

            $zip = new ZipArchive;
            if ($zip->open($styles_dir.'/oc_tmp_import/'.$style_file) === TRUE) {
                $zip->extractTo($styles_dir.'/oc_tmp_import/');
                $zip->close();
            } else {
                exit('Couldn\'t open '.$styles_dir.'/oc_tmp_import/'.$style_file);
            }

        }

    }

    if ($handle = opendir($styles_dir)) {
        while (false !== ($file = readdir($handle))) {

            if ($file != "." && $file != ".." && is_dir($styles_dir.'/'.$file) && $file!='system') {

                if(is_file($styles_dir.'/'.$file.'/style.php')){

                    include($styles_dir.'/'.$file.'/style.php');

                }

            }
        }
        closedir($handle);
    }

    if(!isset($StyleInfo))
        $StyleInfo=array();

    $SystemStyleInfo = $StyleInfo;

    $imported=0;
    $updated=0;
    $uptodate=0;
    $error_importing=0;
    $out_log='';
    if ($handle = opendir($styles_dir.'/oc_tmp_import/')) {

        while (false !== ($file = readdir($handle))) {

            if ($file != "." && $file != ".." && is_dir($styles_dir.'/oc_tmp_import/'.$file)) {

                if(is_file($styles_dir.'/oc_tmp_import/'.$file.'/style.php')){

                    unset($StyleInfo);
                    include($styles_dir.'/oc_tmp_import/'.$file.'/style.php');

                    $import=true;
                    $update=false;
                    foreach($SystemStyleInfo as $key => $value){
                        if($key==key($StyleInfo)){

                            $StyleKey=key($StyleInfo);
                            $StyleInfo=current($StyleInfo);

                            if($value['Name']==$StyleInfo['Name'] and $value['DateCreated']==$StyleInfo['DateCreated'] and $value['Author']==$StyleInfo['Author']){
                                if($value['Version']<$StyleInfo['Version']){
                                    $update=true;
                                }else{
                                    $uptodate++;
                                    $out_log.='<p>'.$StyleInfo['Name'].' - <span class="uptodate" rel="'.$StyleInfo['Name'].'#@#'.$StyleKey.'#@#'.(isset($StyleInfo['System']) ? $StyleInfo['System'] : '').'">nothing made. Was already up to date</span></p>';
                                }
                            }

                            if($value['Name']!=$StyleInfo['Name'] or $value['DateCreated']!=$StyleInfo['DateCreated'] or $value['Author']!=$StyleInfo['Author']){
                                $error_importing++;
                                $out_log.='<p>'.$StyleInfo['Name'].' - <span class="error" rel="'.$StyleInfo['Name'].'#@#'.$StyleKey.'#@#'.(isset($StyleInfo['System']) ? $StyleInfo['System'] : '').'">there is already a custom style with that name</span></p>';
                            }

                            $import=false;
                            break;
                        }
                    }

                    if($import){
                        rename($styles_dir.'/oc_tmp_import/'.$file,$styles_dir.'/'.$file);
                        $StyleKey=key($StyleInfo);
                        $StyleInfo=current($StyleInfo);
                        $imported++;
                        $out_log.='<p>'.$StyleInfo['Name'].' - <span class="imported" rel="'.$StyleInfo['Name'].'#@#'.$StyleKey.'#@#'.(isset($StyleInfo['System']) ? $StyleInfo['System'] : '').'">imported</span></p>';
                    }

                    if($update){
                        rename($styles_dir.'/oc_tmp_import/'.$file,$styles_dir.'/'.$file);
                        $updated++;
                        $out_log.='<p>'.$StyleInfo['Name'].' - <span class="updated" rel="'.$StyleInfo['Name'].'#@#'.$StyleKey.'#@#'.(isset($StyleInfo['System']) ? $StyleInfo['System'] : '').'">updated</span></p>';
                    }

                }

            }
        }
        closedir($handle);
    }

    rrmdir($styles_dir.'/oc_tmp_import');

    $out="
<h2>Import</h2>
<div id=\"import_msg\">".
($imported>0 ? '<p>'.$imported.' style imported!</p>' : '').
($updated>0 ? '<p>'.$updated.' style updated!</p>' : '').
($uptodate>0 ? '<p>'.$uptodate.' style already up to date!</p>' : '').
($error_importing>0 ? '<p>'.$error_importing.' style with the same name as a custom style!</p>' : '').
"<a id=\"toggle_import\" href=\"javascript: toggleImportLog();\">show log</a>
<div id=\"import_log\" style=\"display: none;\">
<h3>LOG</h3>
".$out_log."
</div>
</div>";

    exit($out);

}

?>
<?php if (!defined('APPLICATION')) exit();
// REMOVE CONFLICTS
$c = Gdn::controller();
$c->removeCssFile('jquery.fileupload.css');
$c->removeJsFile('jquery.ui.widget.js');
$c->removeJsFile('jquery.iframe-transport.js');
$c->removeJsFile('jquery.fileupload.js');
?>

<link rel="stylesheet" href="/<?php echo Gdn::Request()->WebRoot(); ?>/themes/oneClick/design/themeoptions/colorpicker.css" type="text/css" />
<script type="text/javascript" src="/<?php echo Gdn::Request()->WebRoot(); ?>/themes/oneClick/js/themeoptions/colorpicker.js"></script>

<link rel="stylesheet" href="/<?php echo Gdn::Request()->WebRoot(); ?>/themes/oneClick/design/themeoptions/jquery.fileupload.css" type="text/css" />
<script src="/<?php echo Gdn::Request()->WebRoot(); ?>/themes/oneClick/js/themeoptions/jquery.ui.widget.js"></script>
<script src="/<?php echo Gdn::Request()->WebRoot(); ?>/themes/oneClick/js/themeoptions/jquery.iframe-transport.js"></script>
<script src="/<?php echo Gdn::Request()->WebRoot(); ?>/themes/oneClick/js/themeoptions/jquery.fileupload.js"></script>

<h1><?php echo $this->Data('Title'); ?></h1>

<?php if ($this->Data('ThemeInfo.Options.Description')) {
   echo '<div class="Info">',
      $this->Data('ThemeInfo.Options.Description'),
      '</div>';
}
?>
<?php
echo $this->Form->Open(array('id'=>'Form_Form'));
echo $this->Form->Errors();
?>

<?php
$current_style = C('ThemeOption.CurrentStyle');
$selected_extra_style = C('ThemeOption.ExtraStyle');

$styles_dir = getcwd().'/themes/oneClick/design/styles/';

$extra_style_options_custom = array();
$extra_style_options_system = array();
if ($handle = opendir($styles_dir)) {
    while (false !== ($file = readdir($handle))) {
        $title='';

        if ($file != "." && $file != ".." && is_dir($styles_dir.$file) && $file!='system') {

            unset($StyleInfo);
            if(is_file($styles_dir.$file.'/style.php')){

                include($styles_dir.$file.'/style.php');
                $style_info = current($StyleInfo);

                $tag_aux = explode(',',$style_info['Tags']);
                for($i=0; $i<count($tag_aux); $i++)
                    $tag_aux[$i]=trim($tag_aux[$i]);
                $tag_aux = ','.implode(',',$tag_aux).',';

                if(isset($_GET['tag']) and  isset($style_info['Tags']) and strpos($tag_aux,','.$_GET['tag'].',')!==false or !isset($_GET['tag']) or $_GET['tag']==''){

                    if($file==$selected_extra_style)
                        $selected_style_info = $style_info;

                    $sort=explode(' ',$style_info['DateCreated']);
                    $sort_date=explode('-',$sort[0]);
                    $sort_time=explode(':',$sort[1]);
                    $timestamp = mktime($sort_time[0], $sort_time[1], $sort_time[2], $sort_date[1], $sort_date[2], $sort_date[0]);

                    if($style_info['Order'] > 0)
                        $timestamp = $timestamp + (100000000 * (100000 - $style_info['Order']));

                    if($style_info['System']){
                        $current_style_options_system_order[] = $timestamp;
                        $current_style_options_system[] = '<option value="'.$file.'"'.($current_style==$file ? ' selected="selected"' : '').'>'.$style_info['Name'].'</option>';
                        $extra_style_options_system_order[] = $timestamp;
                        $extra_style_options_system[] = '<option value="'.$file.'"'.($selected_extra_style==$file ? ' selected="selected"' : '').'>'.$style_info['Name'].'</option>';
                    }else{
                        $current_style_options_custom_order[] = $timestamp;
                        $current_style_options_custom[] = '<option value="'.$file.'"'.($current_style==$file ? ' selected="selected"' : '').'>'.$style_info['Name'].'</option>';
                        $extra_style_options_custom_order[] = $timestamp;
                        $extra_style_options_custom[] = '<option value="'.$file.'"'.($selected_extra_style==$file ? ' selected="selected"' : '').'>'.$style_info['Name'].'</option>';

                    }

                }

            }

        }
    }
    closedir($handle);
}

if(count($current_style_options_custom)>0){
    array_multisort($current_style_options_custom_order,SORT_DESC,SORT_NUMERIC,$current_style_options_custom,SORT_STRING,SORT_ASC);
    $current_style_options_custom_group = '<optgroup label="'.T('My Custom Styles').'">'.implode('',$current_style_options_custom).'</optgroup>';
}

if(count($current_style_options_system)>0){
    array_multisort($current_style_options_system_order,SORT_DESC,SORT_NUMERIC,$current_style_options_system,SORT_STRING,SORT_ASC);
    $current_style_options_system_group = '<optgroup label="'.T('System Styles').'">'.implode('',$current_style_options_system).'</optgroup>';
}

if(count($extra_style_options_custom)>0){
    array_multisort($extra_style_options_custom_order,SORT_DESC,SORT_NUMERIC,$extra_style_options_custom,SORT_STRING,SORT_ASC);
    $extra_style_options_custom_group = '<optgroup label="'.T('My Custom Styles').'">'.implode('',$extra_style_options_custom).'</optgroup>';
}

if(count($extra_style_options_system)>0){
    array_multisort($extra_style_options_system_order,SORT_DESC,SORT_NUMERIC,$extra_style_options_system,SORT_STRING,SORT_ASC);
    $extra_style_options_system_group = '<optgroup label="'.T('System Styles').'">'.implode('',$extra_style_options_system).'</optgroup>';
}

if(count($extra_style_options_custom)<1 and count($extra_style_options_system)<1)
    unset($selected_style_info);

?>

<div id="current_style_container">

    <div style="float: left;">
        <label><?php echo T('Current Style'); ?></label>
        <div id="current_style_holder" class="selectHolder">
            <select id="current_style" name="current_style" onchange="getCurrentStyleInfo();">
                <?php echo $current_style_options_custom_group; ?>
                <?php echo $current_style_options_system_group; ?>
            </select>
            <span></span>
        </div>
        <br>
        <a href="javascript: saveCurrentStyle();" class="Button"><?php echo T('SAVE'); ?></a>

    </div>

    <div id="current_style_info_holder" class="style_info_holder"></div>

</div>

<div class="Tabs FilterTabs">
    <ul>
        <li id="styles_tab" class="Active"><a href="javascript: showTab('styles');"><?php echo T('Styles'); ?> <span>0</span></a></li>
        <li id="layouts_tab"><a href="javascript: showTab('layouts');"><?php echo T('Layouts'); ?> <span>0</span></a></li>
    </ul>
</div>

<?php if (is_array($this->Data('ThemeInfo.Options.Styles'))): ?>

<div id="layouts_container" style="display: none;">

<h3><?php echo T('Layouts'); ?></h3>
<table class="SelectionGrid ThemeStyles">
   <tbody>
<?php
$Alt = FALSE;
$Cols = 3;
$Col = 0;
$Classes = array('FirstCol', 'MiddleCol', 'LastCol');

foreach ($this->Data('ThemeInfo.Options.Styles') as $Key => $Options) {
   $Basename = GetValue('Basename', $Options, '%s');

   if ($Col == 0)
      echo '<tr>';

   $Active = '';
   if ($this->Data('ThemeOptions.Styles.Key') == $Key || (!$this->Data('ThemeOptions.Styles.Key') && $Basename == '%s'))
      $Active = ' Active';

   $KeyID = str_replace(' ', '_', $Key);
   echo "<td id=\"{$KeyID}_td\" class=\"{$Classes[$Col]}$Active\">";
   echo '<h4>',T($Key),'</h4>';

   // Look for a screenshot for the style.
   $Screenshot = SafeGlob(PATH_THEMES.DS.$this->Data('ThemeFolder').DS.'design'.DS.ChangeBasename('screenshot.*', $Basename), array('gif','jpg','png'));
   if (is_array($Screenshot) && count($Screenshot) > 0) {
      $Screenshot = basename($Screenshot[0]);
      echo Img('/themes/'.$this->Data('ThemeFolder').'/design/'.$Screenshot, array('alt' => T($Key), 'width' => '160', 'class' => 'popup', 'id'=> T($Key)));
   }

   $Disabled = $Active ? ' Disabled' : '';

   if (isset($Options['Description'])) {
      echo '<div class="Info2">',
         $Options['Description'],
            '<div class="Buttons">',
            Anchor(T('Select'), '?style='.urlencode($Key), 'SmallButton SelectThemeStyle'.$Disabled, array('Key' => $Key)),
            '</div>',
         '</div>';
   }else{
       echo '<div class="Buttons">',
          Anchor(T('Select'), '?style='.urlencode($Key), 'SmallButton SelectThemeStyle'.$Disabled, array('Key' => $Key)),
        '</div>';
   }

   echo '</td>';

   $Col = ($Col + 1) % 3;
   if ($Col == 0)
      echo '</tr>';
}
   if ($Col > 0)
      echo '<td colspan="'.(3 - $Col).'">&#160;</td></tr>';

?>
   </tbody>
</table>

<h3 style="overflow: hidden; position: relative;">&nbsp;</h3>

</div>

<script>
jQuery(document).ready(function($) {
    $('.popup').css('cursor','pointer').click(function() {
        $.popup({},'<img id="popup_img" src="'+$(this).attr('src')+'" />');
    });
});
</script>

<?php endif; ?>

<?php if (is_array($this->Data('ThemeInfo.Options.Text'))): ?>

<?php

$out='';
foreach ($this->Data('ThemeInfo.Options.Text') as $Code => $Options) {

    if($Code=='CurrentStyle'){
        //$current_style = $this->Form->GetFormValue($this->Form->EscapeString('Text_'.$Code));
        echo '<input type="hidden" value="'.$current_style.'" name="Text_CurrentStyle" id="Form_Text_CurrentStyle">';
    }elseif($Code=='PreviewStyle'){
        $selected_preview_style = $this->Form->GetFormValue($this->Form->EscapeString('Text_'.$Code));
        $out_preview_style = '<input type="checkbox" value="TRUE" name="Text_PreviewStyle" id="Form_Text_PreviewStyle"'.($selected_preview_style ? ' checked="checked"' : '').' onclick="changePreviewStyle();">';
    }elseif($Code=='ExtraStyle'){
        //$selected_extra_style = $this->Form->GetFormValue($this->Form->EscapeString('Text_'.$Code));
        echo '<input type="hidden" value="'.$selected_extra_style.'" name="Text_ExtraStyle" id="Form_Text_ExtraStyle">';
    }else{

       $out .= ($out=='' ? '<ul>' : '').'<li>';

       $this->Form->Label('@'.$Code, 'Text_'.$Code);

       if (isset($Options['Description']))
          $out .=  '<div class="Info2">'.$Options['Description'].'</div>';

       switch (strtolower(GetValue('Type', $Options, 'textarea'))) {
          case 'textbox':
             $out .=  $this->Form->TextBox($this->Form->EscapeString('Text_'.$Code));
             break;
          case 'textarea':
          default:
             $out .=  $this->Form->TextBox($this->Form->EscapeString('Text_'.$Code), array('MultiLine' => TRUE));
             break;
       }

       $out .=  '</li>';

   }

}
$out .= ($out!='' ? '</ul>' : '');
echo $out;
?>

<div id="styles_container">

    <h3><?php echo $out_preview_style; ?> <?php echo T('Show the Preview Style Selector'); ?></h3>

    <div id="style_info">

        <div id="extra_style_container">
            <?php //echo $this->Form->Button('Save'); ?>
            <?php if(count($extra_style_options_custom)>0 or count($extra_style_options_system)>0){ ?>
            <div id="extra_style_holder" class="selectHolder">
                <select id="extra_style" name="extra_style" onchange="changeExtraStyle();">
                    <?php echo $extra_style_options_custom_group; ?>
                    <?php echo $extra_style_options_system_group; ?>
                </select>
                <span></span>

                <img id="ajax_loader_gray" src="<?php echo '/'.Gdn::Request()->WebRoot().'/themes/'.$this->Data('ThemeFolder').'/design/images/ajax-loader-gray.gif' ?>" alt="" class="ajax_loader" />
                <img id="ajax_loader_blue" src="<?php echo '/'.Gdn::Request()->WebRoot().'/themes/'.$this->Data('ThemeFolder').'/design/images/ajax-loader-blue.gif' ?>" alt="" class="ajax_loader" />

            </div>
            <?php } ?>

            <div id="import_extra_style" class="fileupload-container">
                <div class="fileinput-button" title="<?php echo T('Import Styles'); ?>">
                    <?php echo T('Add ZIP file'); ?>
                    <input id="import_styles" type="file" name="files[]" multiple upload-dir="oc_tmp_import/">
                </div>
            </div>
            <?php //if((!isset($selected_style_info['System']) or $selected_style_info['System']!=1) and (count($extra_style_options_custom)>0 or count($extra_style_options_system)>0)){?>
            <a id="export_extra_style" href="javascript: exportStyle();" title="<?php echo T('Export Style'); ?>"><?php echo T('EXPORT'); ?></a>
            <a id="edit_extra_style" href="javascript: editStyleInfo();" title="<?php echo T('Style Info'); ?>"><?php echo T('STYLE INFO'); ?></a>
            <?php //} ?>

            <?php if(isset($_GET['tag']) and $_GET['tag']!=''){ ?>
            <br />
            <div id="remove_tag">
                <span><?php echo $_GET['tag']; ?></span>
                <a href="?p=/dashboard/settings/themeoptions">x</a>
            </div>
            <?php } ?>

        </div>


        <div id="extra_style_nav">
            <!--<a id="help" href="#help_container" title="<?php echo T('Help'); ?>"><?php echo T('Help'); ?></a>-->
            <a id="collapse_expand" href="javascript: collapseAttributes();" title="<?php echo T('Expandable'); ?>" class="expandable"><?php echo T('Expandable'); ?></a>
            <!--
            <a id="show_all" href="javascript: showAttributes();" title="Show All">show all</a>
            <a id="hide_all" href="javascript: hideAttributes();" title="Hide All">hide all</a>
            -->
        </div>
        <div id="extra_style_info_holder" class="style_info_holder">
            <?php
                /*
                echo '<div id="style_info_description">'.$selected_style_info['Description'].'</div>';
                if($selected_style_info['Version']!='') echo '<div id="style_info_version">Version '.$selected_style_info['Version'].'</div>';
                $last_update=explode(' ',$selected_style_info['LastUpdate']);
                $last_update_date=explode('-',$last_update[0]);
                $last_update_time=explode(':',$last_update[1]);
                $timesptamp = mktime($last_update_time[0], $last_update_time[1], $last_update_time[2], $last_update_date[1], $last_update_date[2], $last_update_date[0]);
                if($selected_style_info['LastUpdate']!='') echo '<div id="style_info_last_update">'.date('Y-m-d',$timesptamp).'</div>';
                if($selected_style_info['Author']!='') echo '<div id="style_info_author">'.$selected_style_info['Author'].'</div>';
                if($selected_style_info['AuthorUrl']!='') echo '<div id="style_info_author_url"><a href="'.(strpos($selected_style_info['AuthorUrl'],'http://')!==false ? '' : 'http://').$selected_style_info['AuthorUrl'].'" target="_blank">'.$selected_style_info['AuthorUrl'].'</a></div>';
                if($selected_style_info['Tags']!=''){
                    $tag_aux = explode(',',$selected_style_info['Tags']);
                    $tags_links='';
                    for($i=0; $i<count($tag_aux); $i++)
                        $tags_links.=($tags_links!='' ? ', ' : '').'<a href="?p=/dashboard/settings/themeoptions&tag='.trim($tag_aux[$i]).'">'.trim($tag_aux[$i]).'</a>';
                    echo '<div id="style_info_tags">'.$tags_links.'</div>';
                };
                */
            ?>
        </div>
    </div>

    <div id="form_save_as" style="display: none;">
        <input type="hidden" id="last_update" name="last_update" value="" />
        <ul>
            <li>
                <label><?php echo T('Name'); ?></label>
                <input type="text" id="style_name" name="style_name" value="" class="InputBox WideInput" />
            </li>
            <li>
                <label><?php echo T('Description'); ?></label>
                <textarea id="style_description" name="style_description" rel="<?php echo $selected_style_info['Description']; ?>" class="TextBox"></textarea>
            </li>
            <li>
                <label><?php echo T('Version'); ?></label>
                <input type="text" id="version" name="version" rel="<?php echo $selected_style_info['Version']; ?>" value="" class="InputBox SmallInput" />
            </li>
            <li>
                <label><?php echo T('Author'); ?></label>
                <input type="text" id="author" name="author" rel="<?php echo $selected_style_info['Author']; ?>" value="" class="InputBox WideInput" />
            </li>
            <li>
                <label><?php echo T('Author Url'); ?></label>
                <input type="text" id="author_url" name="author_url" rel="<?php echo $selected_style_info['AuthorUrl']; ?>" value="" class="InputBox WideInput" />
            </li>
            <li>
                <label><?php echo T('Tags'); ?></label>
                <input type="text" id="tags" name="tags" rel="<?php echo $selected_style_info['Tags']; ?>" value="" class="InputBox WideInput" />
            </li>
        </ul>
    </div>

    <input type="hidden" id="upload-dir" name="upload-dir" value="" />

    <div id="extra_style_options" class="<?php if($selected_style_info['System']!=1){ echo 'custom'; }else{ echo 'system'; } ?>"></div>

    <h3>
        <?php
        if((count($extra_style_options_custom) + count($extra_style_options_system))<1){
            echo '&nbsp;';
        }else if(isset($selected_style_info['System']) and $selected_style_info['System']){
            echo $this->Form->Button('SAVE',array('style'=>'display:none;'));
            echo '<a id="save_as_popup" href="#form_save_as" class="Button">'.T('SAVE AS').'</a>';
            if((count($extra_style_options_custom) + count($extra_style_options_system))>1)
                echo '<a id="delete_extra_style" href="#form_save_as" class="ExtraSmallButton">'.T('DELETE').'</a>';
        }else{
            echo $this->Form->Button('SAVE');
            echo '<a id="save_as_popup" href="#form_save_as" class="ExtraSmallButton">'.T('SAVE AS').'</a>';
            if((count($extra_style_options_custom) + count($extra_style_options_system))>1)
                echo '<a id="delete_extra_style" href="#form_save_as" class="ExtraSmallButton">'.T('DELETE').'</a>';
        }
        ?>
    </h3>

</div>

<div id="help_container" style="display: none;"></div>

<script>
var option='new';
var update=false;
var edit_style_info=false;
var collapse=false;
var system_style = false;
var fileuploads=0;
var fileupload_msg='';
var fileuploads_error=0;
var fileupload_error_msg='';
var files_import='';

function getCurrentStyleInfo(){

    var webRoot = gdn.definition('WebRoot', '');

    $.ajax({
        type: "POST",
        url: webRoot+'/themes/oneClick/views/settings/themeoptions.php',
        data: {
            "ajax_call" : 'get_style_info',
            "current_style" : $('#current_style').val(),
            'DeliveryType' : 'BOOL',
            'DeliveryMethod' : 'HTML'
        },
        dataType: 'html',
        success: function(out){

            var style_info_values = out.split('#@#');

            var style_info='';
            style_info += '<div class="style_info_description">'+style_info_values[1]+'</div>';
            style_info += style_info_values[2]!='' ? '<div class="style_info_version"><?php echo T('Version'); ?> '+style_info_values[2]+'</div>' : '';
            style_info += style_info_values[3]!='' ? '<div class="style_info_last_update">'+style_info_values[3]+'</div>' : '';
            style_info += style_info_values[4]!='' ? '<div class="style_info_author">'+style_info_values[4]+'</div>' : '';
            style_info += style_info_values[5]!='' ? '<div class="style_info_author_url"><a href="'+(style_info_values[5].indexOf('http://')!=-1 ? '' : 'http://')+style_info_values[5]+'" target="_blank">'+style_info_values[5]+'</a></div>' : '';

            if(style_info_values[6]!=''){
                var tag_aux = style_info_values[6].split(',');
                var tags_links='';
                for(i=0; i<tag_aux.length; i++)
                    tags_links+=(tags_links!='' ? ', ' : '')+'<a href="?p=/dashboard/settings/themeoptions&tag='+tag_aux[i].trim()+'">'+tag_aux[i].trim()+'</a>';
                style_info +=  '<div class="style_info_tags">'+tags_links+'</div>';
            }

            $('#current_style_info_holder').html(style_info);

        }
    });

}

function saveCurrentStyle(){
    $('#Form_Text_CurrentStyle').val($('#current_style').val());
    $('#Form_Form').submit();
}

function changePreviewStyle(){

    var webRoot = gdn.definition('WebRoot', '');

    $.ajax({
        type: "POST",
        url: webRoot+'/themes/oneClick/views/settings/themeoptions.php',
        data: {
            "ajax_call" : 'change_preview_style',
            "preview_style" : ($('#Form_Text_PreviewStyle').prop('checked') ? 1 : 0),
            'DeliveryType' : 'BOOL',
            'DeliveryMethod' : 'HTML'
        },
        dataType: 'html',
        success: function(out){

        }
    });

}

function changeExtraStyle(){
    option='update';
    getExtraStyle();
}

function showTab(tab){
    $('.Tabs li').removeClass('Active');
    $('#'+tab+'_tab').addClass('Active');
    $('#layouts_container, #styles_container').hide();
    $('#'+tab+'_container').show();
}

function getExtraStyle(){

    if($('#extra_style_holder').hasClass('focus'))
        $('#ajax_loader_blue').show();
    else
        $('#ajax_loader_gray').show();

    var webRoot = gdn.definition('WebRoot', '');

    $.ajax({
        type: "POST",
        url: webRoot+'/themes/oneClick/views/settings/themeoptions.php',
        data: {
            "ajax_call" : 'get_css',
            "extra_style" : $('#extra_style').val(),
            'DeliveryType' : 'BOOL',
            'DeliveryMethod' : 'HTML'
        },
        dataType: 'html',
        success: function(out){

            out = out.split('##@@##');

            var style_info_values = out[0];
                style_info_values = style_info_values.split('#@#');
                $('#style_name').attr('value',style_info_values[0]);
                $('#style_description').html(style_info_values[1]);
                $('#version').attr('value',style_info_values[2]);
                $('#last_update').attr('value',style_info_values[3]);
                $('#author').attr('value',style_info_values[4]);
                $('#author_url').attr('value',style_info_values[5]);
                $('#tags').attr('value',style_info_values[6]);

            $('#extra_style_info_holder').html(createStyleInfo());

            $('#extra_style_options').html(out[1]);

            if($('#extra_style option:selected').parent().attr('label')=='<?php echo T('System Styles'); ?>'){
                system_style=true;
                $('#extra_style_options').removeClass('custom').addClass('system');
                $('#Form_SAVE').hide();
                $('#save_as_popup').addClass('Button').removeClass('ExtraSmallButton');
                $('.fileupload-container').hide();
                $('.delete-custom-image').hide();
            }else{
                system_style=false;
                $('#extra_style_options').removeClass('system').addClass('custom');
                $('#Form_SAVE').show();
                $('#save_as_popup').addClass('ExtraSmallButton').removeClass('Button');
                $('.fileupload-container').show();
            }
            if($('#extra_style option').length==1)
                $('#delete_extra_style').hide();
            else
                $('#delete_extra_style').show();

            $('.selectHolder select').each(function(index) {
                var text = $(this).find('option:selected').text();
                $(this).next().html(text);
            });
            $('.selectHolder select').change(function() {
                var text = $(this).find('option:selected').text();
                $(this).next().html(text);
            });
            $('.selectHolder select').keyup(function() {
                var text = $(this).find('option:selected').text();
                $(this).next().html(text);
            });
            $('.selectHolder select').focusin(function() {
                $(this).parent().addClass('focus');
            });
            $('.selectHolder select').focusout(function() {
                $(this).parent().removeClass('focus');
            });

            $('#extra_style_options label').click(function() {

                if($(this).next().html()=='')
                    getAttribute($(this).attr('id').substr(5));

                if($(this).hasClass('active')){
                    $(this).removeClass('active');
                }else{
                    if(collapse){
                        var parent_class = $(this).parent().attr('class');
                        $('.'+parent_class+' label').removeClass('active');
                        $('#extra_style_options label').filter($(this).parents('.level0').siblings('.level0').find('label')).removeClass('active');
                    }else{
                        var parent_class = $(this).closest().attr('class');
                        $('.'+parent_class+' > label').removeClass('active');
                    }
                    $(this).addClass('active');
                }
                if(collapse)
                    $('.attributes_container').not($(this).next()).not($(this).parents('.attributes_container')).hide();
                $(this).next().toggle();
            });

            $('.ajax_loader').hide();

        }
    });

}

function getAttribute(attribute){

    var webRoot = gdn.definition('WebRoot', '');

    $.ajax({
        type: "POST",
        url: webRoot+'/themes/oneClick/views/settings/themeoptions.php',
        data: {
            "ajax_call" : 'get_css',
            "extra_style" : $('#extra_style').val(),
            "attribute" : attribute,
            'DeliveryType' : 'BOOL',
            'DeliveryMethod' : 'HTML'
        },
        dataType: 'html',
        success: function(out){

            if(out!='')
                $('#attr_'+attribute).next().html(out);

            $('#attr_'+attribute).next().find('.colorSelector').ColorPicker({
                onSubmit: function(hsb, hex, rgb, el) {
                    $(el).prev().val('#' + hex);
                    $(el).css('backgroundColor', '#' + hex);
                    $(el).ColorPickerHide();
                },
                onBeforeShow: function () {
		            $(this).ColorPickerSetColor($(this).prev().val());
	            }
            });
            $('#attr_'+attribute).next().find('.colorInput>input').each(function(index) {
                var hex = $(this).val();
                $(this).next().css('backgroundColor', hex);
            });
            $('#attr_'+attribute).next().find('.colorInput>input').bind('keyup', function(){
                var hex = $(this).val();
            	$(this).next().css('backgroundColor', hex);
            });
            $('#attr_'+attribute).next().find('.colorInput>input').focusin(function() {
                $(this).parent().addClass('focus');
            });
            $('#attr_'+attribute).next().find('.colorInput>input').focusout(function() {
                $(this).parent().removeClass('focus');
            });

            $('#attr_'+attribute).next().find('.selectHolder select').each(function(index) {
                var text = $(this).find('option:selected').text();
                $(this).next().html(text);
            });
            $('#attr_'+attribute).next().find('.selectHolder select').change(function() {
                var text = $(this).find('option:selected').text();
                $(this).next().html(text);
            });
            $('#attr_'+attribute).next().find('.selectHolder select').keyup(function() {
                var text = $(this).find('option:selected').text();
                $(this).next().html(text);
            });
            $('#attr_'+attribute).next().find('.selectHolder select').focusin(function() {
                $(this).parent().addClass('focus');
            });
            $('#attr_'+attribute).next().find('.selectHolder select').focusout(function() {
                $(this).parent().removeClass('focus');
            });

            $('#attr_'+attribute).next().find('.fileupload').fileupload({
                url: webRoot+'/themes/oneClick/includes/',
                dataType: 'json',
                sequentialUploads: true,
                add: function (e, data) {
                    var container = $(this).parents('.backgroundImageContainer');
                    $(container).find('.progress').css('display','block').animate({'opacity' : 1 },300,'linear').find('.bar').css('width', '0%');
                    $('#upload-dir').val($('#extra_style').val()+'/'+$(this).attr('upload-dir'));
                    data.submit();
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $(this).parents('.backgroundImageContainer').find('.progress .bar').css('width', progress + '%');
                },
                done: function (e, data) {

                    var container = $(this).parents('.backgroundImageContainer');

                    $.each(data.result.files, function (index, file) {

                        if(file.error==undefined){

                            var select = $(container).find('select');
                            if($('optgroup[label="<?php echo T('My Custom Images'); ?>"]', select).length>0){
                                $(select).find('optgroup[label="<?php echo T('My Custom Images'); ?>"]').prepend('<option value="url('+$(select).attr('rel')+file.name+')">'+file.name+'</option>');
                            }else{
                                $("option:eq(0)", select).after('<optgroup label="<?php echo T('My Custom Images'); ?>"><option value="url('+$(select).attr('rel')+file.name+')">'+file.name+'</option></optgroup>');
                            }
                            $(select).val($(select).find('optgroup[label="<?php echo T('My Custom Images'); ?>"] option:first').val());
                            $(select).next().html(file.name).parents().addClass('focus')

                            fileuploads++;

                        }else{
                            if(fileupload_error_msg.indexOf(file.error)==-1)
                                fileupload_error_msg += (fileupload_error_msg!='' ? '; ' : '')+file.error;
                            fileuploads_error++;
                        }

                    });

                },
                start: function(e){
                    fileuploads=0;
                    fileupload_msg='';
                    fileuploads_error=0;
                    fileupload_error_msg='';
                },
                stop: function(e){

                    var msg='';

                    if(fileuploads==1)
                        msg += '1 image uploaded';
                    else if(fileuploads>1)
                        msg += fileuploads+' images uploaded';

                    if(fileuploads_error==1)
                        msg += (msg!='' ? ' and ' : '')+'1 error ('+fileupload_error_msg+')';
                    else if(fileuploads_error>1)
                        msg += (msg!='' ? ' and ' : '')+fileuploads_error+' errors ('+fileupload_error_msg+')';

                    $(this).parents('.backgroundImageContainer').find('.progress').animate({
                        'opacity' : 0
                    },300,'linear',function(){
                        $(this).css('display','none');
                        $(this).parents('.backgroundImageContainer').find('.delete-custom-image').fadeIn();
                        $(this).parents('.backgroundImageContainer').find('.custom-image-msg').html(msg).fadeIn().delay(2000).fadeOut(1000);
                    });
                },
                fail: function (e, data) {
                    console.log(data.errorThrown+' - '+data.textStatus);
                    // data.errorThrown
                    // data.textStatus;
                    // data.jqXHR;
                }
            });

        }
    });

}

function setExtraStyle(){

    var webRoot = gdn.definition('WebRoot', '');
    $.ajax({
        type: "POST",
        url: webRoot+'/themes/oneClick/views/settings/themeoptions.php?ajax_call=set_css&option='+option+'&update='+update,
        data: $('#Form_Form').serializeArray(),
        dataType: 'html',
        async: false,
        success: function(out){

            if(out=='0')
                return '0';

            if(out=='1'){
                $.popup.close('#Popup');
                return '1';
            }

            $.popup.close('#Popup');

            var arr_out = out.split('#@#');
            $('#Form_Text_ExtraStyle').val(arr_out[1]);
            $('#last_update').val(arr_out[2]);

            if(option=='new'){
                system_style=false;

                if(!$('#current_style optgroup[label="<?php echo T('My Custom Styles'); ?>"]').length)
                    $('#current_style').prepend('<optgroup label="<?php echo T('My Custom Styles'); ?>"></optgroup>');
                $('#current_style optgroup[label="<?php echo T('My Custom Styles'); ?>"]').prepend('<option value="'+arr_out[1]+'">'+$('#style_name').val()+'</option>');

                if(!$('#extra_style optgroup[label="<?php echo T('My Custom Styles'); ?>"]').length)
                    $('#extra_style').prepend('<optgroup label="<?php echo T('My Custom Styles'); ?>"></optgroup>');
                $('#extra_style optgroup[label="<?php echo T('My Custom Styles'); ?>"]').prepend('<option value="'+arr_out[1]+'">'+$('#style_name').val()+'</option>');
                $('#extra_style').val($('#extra_style option:eq(0)').val());
                $('#extra_style_holder span').html($('#style_name').val());

                $('#extra_style_options').removeClass('system').addClass('custom');

                $('#styles_tab span').html(parseInt($('#styles_tab span').html())+1);

                $('.fileupload-container').show();
                $('.backgroundImageContainer select').each(function(index) {
                    verifyCustomImage($(this));
                });

                $('#Form_SAVE').show();
                $('#save_as_popup').addClass('ExtraSmallButton').removeClass('Button');
            }
            if(option=='update'){
                $('#extra_style option:selected').attr('value',arr_out[1]).html($('#style_name').val());
                $('#extra_style_holder span').html($('#style_name').val());
            }

            $('#extra_style_info_holder').html(createStyleInfo());

            $.popup({},'<h2><?php echo T('Save Style'); ?></h2><div id="import_msg"><?php echo T('Style saved successfully!'); ?></div>');

            return false;
        }
    });

    return false;

}

/*
function showAttributes(){
    $('.attributes_container').show();
}

function hideAttributes(){
    $('.attributes_container').hide();
}
*/

function collapseAttributes(){
    if(collapse){
        collapse=false;
        $('#collapse_expand').removeClass('collapsible').addClass('expandable').attr('title','<?php echo T('Expandable'); ?>').html('<?php echo addslashes(T('Expandable')); ?>');
    }else{
        collapse=true;
        $('#collapse_expand').removeClass('expandable').addClass('collapsible').attr('title','<?php echo T('Collapsible'); ?>').html('<?php echo addslashes(T('Collapsible')); ?>');
    }
}

$(function(){

    var webRoot = gdn.definition('WebRoot', '');

    getCurrentStyleInfo();

    $('.Tabs ul li:eq(0) span').html($('#extra_style option').length);
    $('.Tabs ul li:eq(1) span').html($('table.ThemeStyles tr td h4').length);

    getExtraStyle();

    $('#Form_SAVE').click(function() {
        option='update';
        update=true;
        setExtraStyle();
        return false;
    });

    $('#save_as_popup').popup({
        confirm: true,
        followConfirm: false,
        confirmHtml:       '\
<div class="Overlay"> \
<div id="{popup.id}" class="Popup"> \
  <div class="Border"> \
    <div class="Body"> \
      <div class="Content"><h2><?php echo T('Save As'); ?></h2><div id="new_style_msg" class="msg" style="display: none;"></div><form id="form_new_style" name="form_new_style" action="" method="post"></form></div> \
      <div class="Footer Confirm"> \
        <input type="button" class="Button Okay" value="<?php echo T('Okay'); ?>" /> \
        <input type="button" class="Button Cancel" value="<?php echo T('Cancel'); ?>" /> \
      </div> \
    </div> \
  </div> \
</div> \
</div>',
        containerCssClass: 'popup_save_as'
    });

    $('#save_as_popup').click(function() {
        option='new';
        checkDOMChange();
    });

    $('#help').popup({
        targetUrl : webRoot+'/themes/oneClick/help/one_click-style_editor-popup.html',
        hijackForms:  false
    });

    $('#delete_extra_style').popup({
        confirm: true,
        followConfirm: false,
        confirmHtml:       '\
<div class="Overlay"> \
<div id="{popup.id}" class="Popup"> \
  <div class="Border"> \
    <div class="Body"> \
      <div class="Content"><h2><?php echo T('Delete Style'); ?></h2><div class="msg"><?php echo T('Are you sure you want to delete this style?'); ?></div></div> \
      <div class="Footer Confirm"> \
        <input type="button" class="Button Okay" value="<?php echo T('Okay'); ?>" /> \
        <input type="button" class="Button Cancel" value="<?php echo T('Cancel'); ?>" /> \
      </div> \
    </div> \
  </div> \
</div> \
</div>',
        afterConfirm:     function(json, sender) {

            if($('#extra_style').val()==$('#Form_Text_CurrentStyle').val()){
                $.popup({},'<h2><?php echo T('Delete Style'); ?></h2><div class="msg"><?php echo T('You can not delete this style because its current active style.'); ?></div>');
                return false;
            }

            $.ajax({
                type: "POST",
                url: webRoot+'/themes/oneClick/views/settings/themeoptions.php?ajax_call=delete_css',
                data: $('#Form_Form').serializeArray(),
                dataType: 'html',
                success: function(out){

                    var extra_style_selected_index = $('#extra_style option:selected').index();
                    $('#current_style option:eq('+extra_style_selected_index+')').remove();
                    if($('#current_style optgroup[label="<?php echo T('My Custom Styles'); ?>"]').children().length==0)
                        $('#current_style optgroup[label="<?php echo T('My Custom Styles'); ?>"]').remove();

                    $('#extra_style option:selected').remove();
                    $('#extra_style').val($('#extra_style option:eq(0)').val());

                    if($('#extra_style optgroup[label="<?php echo T('My Custom Styles'); ?>"]').children().length==0)
                        $('#extra_style optgroup[label="<?php echo T('My Custom Styles'); ?>"]').remove();

                    getExtraStyle();

                    $('#styles_tab span').html(parseInt($('#styles_tab span').html())-1);

                    return false;
                }
            });
        }

    });

    $('#import_styles').fileupload({
        url: webRoot+'/themes/oneClick/includes/',
        dataType: 'json',
        //singleFileUploads: true,
        //sequentialUploads: true,
        //limitConcurrentUploads: 1,
        add: function (e, data) {

            /*
            var container = $(this).parents('.backgroundImageContainer');
            $(container).find('.progress').css('display','block').animate({
                'opacity' : 1
            },300,'linear').find('.bar').css('width', '0%');
            $(container).find('.custom-image-msg').css('display','block').html('');
            */

            $('#upload-dir').val($(this).attr('upload-dir'));

            data.submit();
        },
        /*
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $(this).parents('.backgroundImageContainer').find('.progress .bar').css('width', progress + '%');
        },
        */
        done: function (e, data) {

            //var container = $(this).parents('.backgroundImageContainer');

            $.each(data.result.files, function (index, file) {

                if(file.error==undefined){

                    files_import += (files_import!='' ? '#@#' : '')+file.name;

                }else{
                    if(fileupload_error_msg.indexOf(file.error)==-1)
                        fileupload_error_msg += (fileupload_error_msg!='' ? '; ' : '')+file.error;
                    fileuploads_error++;
                }

            });

        },

        start: function(e){
            files_import='';
        },
        stop: function(e){

/*
            var msg='';

            if(fileuploads==1)
                msg += '1 image uploaded';
            else if(fileuploads>1)
                msg += fileuploads+' images uploaded';

            if(fileuploads_error==1)
                msg += (msg!='' ? ' and ' : '')+'1 error ('+fileupload_error_msg+')';
            else if(fileuploads_error>1)
                msg += (msg!='' ? ' and ' : '')+fileuploads_error+' errors ('+fileupload_error_msg+')';

            $(this).parents('.backgroundImageContainer').find('.progress').animate({
                'opacity' : 0
            },300,'linear',function(){
                $(this).css('display','none');
                $(this).parents('.backgroundImageContainer').find('.delete-custom-image').fadeIn();
                $(this).parents('.backgroundImageContainer').find('.custom-image-msg').html(msg).delay(2000).fadeOut(1000);
            });
*/

            if(files_import!=''){

                $.ajax({
                    type: "POST",
                    url: webRoot+'/themes/oneClick/views/settings/themeoptions.php?ajax_call=import_style',
                    async: false,
                    data: {
                        'style_file' : files_import
                    },
                    dataType: 'html',
                    success: function(out){
                        $.popup({
                            'afterLoad' : function(){
                                var flag_new=false;
                                $('#import_log .imported').each(function(index) {
                                    if($('#extra_style').length<1){
                                        $('#extra_style_container').prepend('<div id="extra_style_holder" class="selectHolder"><select id="extra_style" name="extra_style" onchange="changeExtraStyle();"></select><span></span></div>');
                                        flag_new=true;
                                    }
                                    var arr_option_text_value_system = $(this).attr('rel').split('#@#');
                                    if(arr_option_text_value_system[2]==1){
                                        if($('#extra_style optgroup[label="<?php echo T('System Styles'); ?>"]').length<1)
                                            $('#extra_style').append('<optgroup label="<?php echo T('System Styles'); ?>"></optgroup>');
                                        $('#extra_style optgroup[label="<?php echo T('System Styles'); ?>"]').prepend('<option value="'+arr_option_text_value_system[1]+'">'+arr_option_text_value_system[0]+'</option>');
                                    }else{
                                        if($('#extra_style optgroup[label="<?php echo T('My Custom Styles'); ?>"]').length<1)
                                            $('#extra_style').append('<optgroup label="<?php echo T('My Custom Styles'); ?>"></optgroup>');
                                        $('#extra_style optgroup[label="<?php echo T('My Custom Styles'); ?>"]').prepend('<option value="'+arr_option_text_value_system[1]+'">'+arr_option_text_value_system[0]+'</option>');
                                    }
                                });

                                //if(flag_new)
                                $('#extra_style').val($('#extra_style option:eq(0)').val());
                                changeExtraStyle();

                                $('.Tabs ul li:eq(0) span').html($('#extra_style option').length);
                            }
                        },out);
                    }
                });

            }else{
                $.popup({},'<h2><?php echo T('Import'); ?></h2><div id="import_msg">'+fileupload_error_msg+'</div>');
            }

        }
    });

});

function createStyleInfo(){

    var out='';
    out += '<div class="style_info_description">'+$('#style_description').val()+'</div>';
    out += $('#version').val()!='' ? '<div class="style_info_version"><?php echo T('Version'); ?> '+$('#version').val()+'</div>' : '';
    out += $('#last_update').val()!='' ? '<div class="style_info_last_update">'+$('#last_update').val()+'</div>' : '';
    out += $('#author').val()!='' ? '<div class="style_info_author">'+$('#author').val()+'</div>' : '';
    out += $('#author_url').val()!='' ? '<div class="style_info_author_url"><a href="'+($('#author_url').val().indexOf('http://')!=-1 ? '' : 'http://')+$('#author_url').val()+'" target="_blank">'+$('#author_url').val()+'</a></div>' : '';

    if($('#tags').val()!=''){
        var tag_aux = $('#tags').val().split(',');
        var tags_links='';
        for(i=0; i<tag_aux.length; i++)
            tags_links+=(tags_links!='' ? ', ' : '')+'<a href="?p=/dashboard/settings/themeoptions&tag='+tag_aux[i].trim()+'">'+tag_aux[i].trim()+'</a>';
        out +=  '<div class="style_info_tags">'+tags_links+'</div>';
    }

    return out;

}

function editStyleInfo(){
    edit_style_info=true;
    $('#save_as_popup').trigger('click');
}

function checkDOMChange()
{
    if($('#Popup .Okay').length > 0){

        var form_html = $('#form_save_as').html();
        form_html = form_html.replace(/id="/g,'id="tmp_');
        form_html = form_html.replace(/name="/g,'name="tmp_');
        $('#form_new_style').html(form_html);

        if(edit_style_info){
            option='update';
            $('.popup_save_as .Content > h2').html('Style Info').css('text-indent','0');
            edit_style_info=false;
            if(system_style){
                $('#form_new_style input').prop('readonly', true);
                $('#form_new_style textarea').prop('readonly', true);
            }else{
                $('#form_new_style input').prop('readonly', false);
                $('#form_new_style textarea').prop('readonly', false);
            }
        }else{
            $('.popup_save_as .Content > h2').css('text-indent','0');
            option='new';
            $('#form_new_style input').val('');
            $('#tmp_style_description').html('');
        }

        $('#tmp_style_name').focus();

        $('#Popup .Okay').unbind('click').click(function() {
            if($('#tmp_style_name').val()=='' || $('#tmp_style_description').val()=='' || $('#tmp_version').val()=='' || $('#tmp_author').val()==''){
                $('#new_style_msg').css('display','block').html('<?php echo addslashes(T('Please fill the Style Name, the Description, the Version and the Author!')); ?>');
            }else{

                $('#style_name').attr('value',$('#tmp_style_name').val());
                $('#style_description').html($('#tmp_style_description').val());
                $('#version').attr('value',$('#tmp_version').val());
                $('#author').attr('value',$('#tmp_author').val());
                $('#author_url').attr('value',$('#tmp_author_url').val());
                $('#tags').attr('value',$('#tmp_tags').val());

                update=true;
                var res = setExtraStyle();

                if(res=='0')
                    $('#new_style_msg').css('display','block').html('<?php echo addslashes(T('There is already a Style with that name. Please change to another one.')); ?>');


            }
        });

  }else{
        setTimeout( checkDOMChange, 100 );
    }
}

function verifyCustomImage(obj){

    var selected = $("option:selected", obj);
    if(selected.parent().attr('label') == "My Custom Images" && !system_style){
        $(obj).parents('.backgroundImageContainer').find('.delete-custom-image').fadeIn();
    } else {
        $(obj).parents('.backgroundImageContainer').find('.delete-custom-image').fadeOut();
    }

}

function deleteCustomImage(select_name,confirm){

    if(confirm=='confirm'){

        $('select[name="'+select_name+'"]').parents('.backgroundImageContainer').find('.custom-image-msg').fadeOut(400,function(){

            var webRoot = gdn.definition('WebRoot', '');
            $.ajax({
                type: "POST",
                url: webRoot+'/themes/oneClick/views/settings/themeoptions.php?ajax_call=delete_custom_image',
                data: {
                    'extra_style' : $('#extra_style').val(),
                    'upload-dir'  : $('select[name="'+select_name+'"]').parents('.backgroundImageContainer').find('.fileupload').attr('upload-dir'),
                    'image'       : $('select[name="'+select_name+'"]').val()
                },
                dataType: 'html',
                success: function(out){

                    if(out==''){

                        var container = $('select[name="'+select_name+'"]').parents('.backgroundImageContainer');
                        container.find('.custom-image-msg').html('Image deleted!').fadeIn().delay(2000).fadeOut();

                        var select_option_length = $('select[name="'+select_name+'"] optgroup[label="<?php echo T('My Custom Images'); ?>"] option').length;
                        if(select_option_length==1){
                            $('select[name="'+select_name+'"] optgroup[label="<?php echo T('My Custom Images'); ?>"]').remove();
                            container.find('.delete-custom-image').fadeOut();
                        }else{
                            var selected_option_index = $('select[name="'+select_name+'"] option:selected').index();
                            $('select[name="'+select_name+'"] option:selected').remove();

                            if(selected_option_index >= select_option_length - 1)
                                selected_option_index--;

                            $('select[name="'+select_name+'"]').val($('select[name="'+select_name+'"]').find('optgroup[label="<?php echo T('My Custom Images'); ?>"] option:eq('+selected_option_index+')').val());

                        }

                        $('select[name="'+select_name+'"]').next().html($('select[name="'+select_name+'"] option:selected').text()).parent().addClass('focus');

                    }else{

                        $('select[name="'+select_name+'"]').parents('.backgroundImageContainer').find('.custom-image-msg').fadeOut().html(out).fadeIn();

                    }

                }
            });

        });

    }else if(confirm=='cancel'){
        $('select[name="'+select_name+'"]').parents('.backgroundImageContainer').find('.custom-image-msg').fadeOut();
    }else{
        $('select[name="'+select_name+'"]').parents('.backgroundImageContainer').find('.custom-image-msg').html('Are you sure you want to delete this image?<br><a href="javascript: deleteCustomImage(\''+select_name+'\',\'confirm\');">YES</a> <a href="javascript: deleteCustomImage(\''+select_name+'\',\'cancel\');">CANCEL</a>').fadeIn();
    }

}

function exportStyle(){

    var webRoot = gdn.definition('WebRoot', '');

    window.location.href=webRoot+'/themes/oneClick/views/settings/themeoptions.php?ajax_call=export_style&extra_style='+$('#extra_style').val();

}

function toggleImportLog(){
    $('#import_log').toggle();
    if($('#import_log').is(':visible'))
        $('#toggle_import').html('hide log');
    else
        $('#toggle_import').html('show log');
}

</script>

<?php endif; ?>

<?php
echo $this->Form->Close();
?>