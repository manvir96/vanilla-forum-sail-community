/**
 * jquery.responsivemenu.js
 * http://creativedreams.eu
 * Author: Creative Dreams
 * Copyright 2014, Creative Dreams (creativedreams.eu)
 * Licensed under the MIT license.
 */
;(function ( $, window, document, undefined )
{
    var pluginName = "responsivemenu",
        menuInitialOuterWidth = 0,
        collapserInitialOuterWidth = 0,
        defaults =
        {
            parentContainer: '',
            sameLineElements: '',
            resizeWidth: '',
            collapserTitle: '',
            animSpeed: 'medium',
            easingEffect: null,
            indentChildren: false,
            childrenIndenter: '&nbsp;&nbsp;'
        };

    function Plugin( element, options )
    {
        this.element = element;
        this.$elem = $(this.element);
        this.options = $.extend( {}, defaults, options );
        this.init();
    }

    Plugin.prototype = {

        init: function()
        {
            var $options = this.options,
                $menu = this.$elem,
                $collapser = '<div class="menu-collapser">'+$options.collapserTitle+'<div class="collapse-button"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></div></div>',
                $menu_collapser;
                menuInitialOuterWidth = 0;
                collapserInitialOuterWidth = 0;

            $menu.find('li').each(function(index) {
                menuInitialOuterWidth += $(this).outerWidth(true);
            });
            menuInitialOuterWidth += $menu.outerWidth(true) - $menu.width();
            $menu.wrap('<div id="menu-container"></div>');
            //$($options.sameLineElements).css('width',$($options.sameLineElements).width()+'px');

            $menu.before($collapser);
            $menu_collapser = $menu.prev('.menu-collapser');

            $menu.on('click', '.sub-collapser', function(e)
            {
                e.preventDefault();
                e.stopPropagation();

                var $parent_li = $(this).closest('li');

                if ($(this).hasClass('expanded'))
                {
                    $(this).removeClass('expanded');
                    $(this).find('i').html('&#9660;');
                    $parent_li.find('>ul').slideUp($options.animSpeed, $options.easingEffect);
                }
                else
                {
                    $(this).addClass('expanded');
                    $(this).find('i').html('&#9650;');
                    $parent_li.find('>ul').slideDown($options.animSpeed, $options.easingEffect);
                }
            });

            $menu_collapser.on('click', function(e)
            {
                e.preventDefault();
                $('#menu-container').toggleClass('active');
                //$menu.slideToggle($options.animSpeed, $options.easingEffect);
                $menu.stop().toggle($options.animSpeed, $options.easingEffect);
            });

            $(document).mouseup(function (e)
            {
                var container = $('#menu-container');

                if (!container.is(e.target) // if the target of the click isn't the container...
                    && container.has(e.target).length === 0 // ... nor a descendant of the container
                    && container.hasClass('collapsed')) // is the responsive menu
                {
                    $menu.stop().hide($options.animSpeed, $options.easingEffect);
                }
            });

            this.resizeMenu({ data: { el: this.element, options: this.options } });
            $(window).on('resize', { el: this.element, options: this.options }, this.resizeMenu);
        },

        resizeMenu: function(event)
        {
            var $window = $(window),
                $options = event.data.options,
                $menu = $(event.data.el),
                $menu_collapser = $('body').find('.menu-collapser');
                $menu_collapser_button = $('body').find('.menu-collapser .collapse-button');
                $menu_container = $('#menu-container');

            if($options.sameLineElements!=''){
                $options.resizeWidth = menuInitialOuterWidth + $($options.sameLineElements).outerWidth(true) + ( $($options.parentContainer).outerWidth(true) - $($options.parentContainer).width() ) + 48;
            }else{
                $options.resizeWidth = 768;
            }

            $menu.find('li').each(function()
            {
                if ($(this).has('ul').length)
                {
                    if ($(this).has('.sub-collapser').length)
                    {
                        $(this).children('.sub-collapser i').html('&#9660;');
                    }
                    else
                    {
                        $(this).append('<span class="sub-collapser"><i>&#9660;</i></span>');
                    }
                }

                $(this).children('ul').hide();
                $(this).find('.sub-collapser').removeClass('expanded').children('i').html('&#9660;');
            });

            if ($options.resizeWidth >= $window.width())
            {
                if ($options.indentChildren)
                {
                    $menu.find('ul').each(function()
                    {
                        var $depth = $(this).parents('ul').length;
                        if (!$(this).children('li').children('a').has('i').length)
                        {
                            $(this).children('li').children('a').prepend(Plugin.prototype.indent($depth, $options));
                        }
                    });
                }

                $menu.find('li').has('ul').off('mouseenter mouseleave');
                $menu.addClass('collapsed').hide();

                $menu_container.addClass('collapsed');
                if(collapserInitialOuterWidth==0)
                    collapserInitialOuterWidth = $menu_collapser_button.outerWidth(true);

                console.log($($options.sameLineElements).outerWidth(true)+' + ('+$($options.parentContainer).outerWidth(true)+' - '+$($options.parentContainer).width()+' ) + '+collapserInitialOuterWidth+') > '+$window.width());
                if((($($options.sameLineElements).outerWidth(true) + ( $($options.parentContainer).outerWidth(true) - $($options.parentContainer).width() ) + collapserInitialOuterWidth) > $window.width())){
                    $menu_container.addClass('new-line');
                    $($options.sameLineElements).addClass('new-line');
                }else{
                    $menu_container.removeClass('new-line');
                    $($options.sameLineElements).removeClass('new-line');
                }
                $menu_collapser.show();
                //$menu_container.css('margin-top',(($($options.parentContainer).height() - $menu_container.height() ) / 2)+'px')
            }
            else
            {
                $menu.find('li').has('ul').on('mouseenter', function()
                {
                    $(this).find('>ul').stop().slideDown($options.animSpeed, $options.easingEffect);
                })
                .on('mouseleave', function()
                {
                    $(this).find('>ul').stop().slideUp($options.animSpeed, $options.easingEffect);
                });

                $menu.find('li > a > i').remove();
                $menu.removeClass('collapsed').show();

                $menu_container.removeClass('collapsed');
                $menu_container.removeClass('new-line');
                $($options.sameLineElements).removeClass('new-line');
                //$menu_container.css('margin-top','0')

                $menu_collapser.hide();
            }

        },

        indent: function(num, options)
        {
            var $indent = '';
            for (var i=0; i < num; i++)
            {
                $indent += options.childrenIndenter;
            }
            return '<i>'+$indent+'</i>';
        }
    };

    $.fn[pluginName] = function ( options )
    {
        return this.each(function ()
        {
            if (!$.data(this, "plugin_" + pluginName))
            {
                $.data(this, "plugin_" + pluginName,
                new Plugin( this, options ));
            }
        });
    };

})( jQuery, window, document );