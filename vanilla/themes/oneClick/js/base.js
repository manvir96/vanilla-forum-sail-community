var auto_hide_bookmarks = true;
var auto_hide_admin_checkboxes = true;
var vertical_align_statboxes = true;

function autoHideAdminCheckboxesDiscussionsItem(){

    $('.AdminCheck input:not(:checked)').parent().hide();
    if($('.AdminCheck input').is(':checked'))
        $('.TopAdminCheck').show();
    else
        $('.TopAdminCheck').hide();
    var all_clicked = true;
    $('.AdminCheck input').each(function(index) {
        if(!$(this).is(':checked') && !$(this).parent().hasClass('TopAdminCheck'))
            all_clicked = false;
    });
    if(all_clicked)
        $('.TopAdminCheck input').prop('checked', true);
    else
        $('.TopAdminCheck input').prop('checked', false);

    $('.TopAdminCheck input').click(function() {
        if($(this).is(':checked')){
            $('.AdminCheck').show();
        }else{
            $('.TopAdminCheck').hide();
            $('.AdminCheck').hide();
        }
    });
    $('.AdminCheck input').click(function() {
        if($('.AdminCheck input').is(':checked'))
            $('.TopAdminCheck').show();
        else
            $('.TopAdminCheck').hide();

        var all_clicked = true;
        $('.AdminCheck input').each(function(index) {
            if(!$(this).is(':checked') && !$(this).parent().hasClass('TopAdminCheck'))
                all_clicked = false;
        });

        if(all_clicked)
            $('.TopAdminCheck input').prop('checked', true);
        else
            $('.TopAdminCheck input').prop('checked', false);
    });

    $('.MessageList .Item').hover(function () {
        $(this).find('.AdminCheck').show();
    },function () {
        if(!$(this).find('.AdminCheck input').is(':checked'))
            $(this).find('.AdminCheck').hide();
    });

}

// This file contains javascript that is global to the entire Garden application
jQuery(document).ready(function($) {

    // IF THE FORUM IS BEING EMBEDDED (INSIDE AN IFRAME) ADDS THE EMBEDDED CLASS TO ADJUST THE HEIGHT TO AUTO
    if ( self !== top )
        $('body').addClass('embedded');
        
    // ADD A LOGO
    if($('#Head h1 a').length){
        var custom_logo_img = $('#Head h1 a').css('background-image');
        var custom_logo_img_width = $('#Head h1 a').css('width');
        var custom_logo_img_height = $('#Head h1 a').css('height');

        var filename = custom_logo_img.substring(custom_logo_img.lastIndexOf('/')+1);
        if(custom_logo_img.indexOf('"')!=-1)
            custom_logo_img = custom_logo_img.substring(5,custom_logo_img.length-2);
        else
            custom_logo_img = custom_logo_img.substring(4,custom_logo_img.length-1);
        if(filename!='none'){
            $('#Head h1 a').css({'background-image':'none','line-height':'0'}).html('<img src="'+custom_logo_img+'" />');
            //$('#Head h1 a').css('background-image','none').html('<img src="'+custom_logo_img+'" style="width: '+custom_logo_img_width+'; height: '+custom_logo_img_height+';" />');
        }
    }

    // RESPONSIVE MENU
    $('#Menu').responsivemenu(
    {
        parentContainer: '#Head',
        sameLineElements: '#Logo',
        animSpeed: 500,
        easingEffect: 'easeInOutQuint'
    });

    // TOOLTIP ON MENU
    $('#Menu li').tipsy({
        gravity: 'n',
        delayIn: 300,
        delayOut: 300,
        fade: true,
        opacity: 1
    });

    // FOOTER ALWAYS ON BOTTOM
    $('#BodyWrapper').css('paddingBottom',$('#FootWrapper').outerHeight(true));
    $('#FootWrapper').css('position','absolute');
    $(window).resize(function(){
        $('#BodyWrapper').css('paddingBottom',$('#FootWrapper').outerHeight(true));
    });

    // REMOVE CURRENT FLYOUT MENU ANIMATION AND ADD CUSTOM ANIMATION (EQUAL TO GLOBAL.JS WITH A LITTLE DIFFERENCE show() and hide() to toggle(500,'easeInOutQuint'));
    $(document).undelegate('.ToggleFlyout:not(.Message .ToggleFlyout)', 'click');
    $(document).undelegate('.ToggleFlyout a', 'mouseup');
    $(document).undelegate(document, 'click');

    var lastOpen = null;
    $(document).delegate('.ToggleFlyout:not(.Message .ToggleFlyout)', 'click', function(e) {

      var $flyout = $('.Flyout', this);
        var isHandle = false;

        if ($(e.target).closest('.Flyout').length == 0) {
           e.stopPropagation();
           isHandle = true;
        } else if ($(e.target).hasClass('Hijack') || $(e.target).closest('a').hasClass('Hijack')) {
           return;
        }
        e.stopPropagation();

      // Dynamically fill the flyout.
      var rel = $(this).attr('rel');
      if (rel) {
         $(this).attr('rel', '');
         $flyout.html('<div class="InProgress" style="height: 30px"></div>');

         $.ajax({
            url: gdn.url(rel),
            data: {DeliveryType: 'VIEW'},
            success: function(data) {
               $flyout.html(data);
            },
            error: function(xhr) {
               $flyout.html('');
               gdn.informError(xhr, true);
            }
         });
      }

      if ($flyout.css('display') == 'none') {
         if (lastOpen != null) {
            //$('.Flyout', lastOpen).hide(); // ORIGINAL CODE
            $('.Flyout', lastOpen).stop().hide(500,'easeInOutQuint'); // CHANGED CODE
            $(lastOpen).removeClass('Open').closest('.Item').removeClass('Open');
         }

         $(this).addClass('Open').closest('.Item').addClass('Open');
         //$flyout.show(); // ORIGINAL CODE
         $flyout.stop().show(500,'easeInOutQuint'); // CHANGED CODE


         lastOpen = this;
      } else {
         //$flyout.hide(); // ORIGINAL CODE
         $flyout.stop().hide(500,'easeInOutQuint'); // CHANGED CODE
         $(this).removeClass('Open').closest('.Item').removeClass('Open');
      }

        if (isHandle)
           return false;
    });
    // Close ToggleFlyout menu even if their links are hijacked
    $(document).delegate('.ToggleFlyout a', 'mouseup', function() {
      if ($(this).hasClass('FlyoutButton'))
         return;
      $('.ToggleFlyout').removeClass('Open').closest('.Item').removeClass('Open');
      //$('.Flyout').hide(); // ORIGINAL CODE
      $flyout.stop().hide(500,'easeInOutQuint'); // CHANGED CODE
    });

    $(document).delegate(document, 'click', function() {
        if (lastOpen) {
            //$('.Flyout', lastOpen).hide(); // ORIGINAL CODE
            $('.Flyout', lastOpen).stop().hide(500,'easeInOutQuint'); // CHANGED CODE
            $(lastOpen).removeClass('Open').closest('.Item').removeClass('Open');
        }
        $('.ButtonGroup').removeClass('Open');
    });

    // SEARCH
    var search_box_max_width = parseInt($('#SearchBox').css('max-width'));
    var head_height = parseInt($('#Head').outerHeight(true));

    var app_bar_two_lines = false;
    var remaining_space = 0;
    var search_box_inital_right_pos = 0;
    var search_box_final_right_pos = 0;
    remaining_space = $(window).width() - $('#Logo').outerWidth(true) - $('#menu-container').outerWidth(true) - $('#SearchBox').outerWidth(true);

    if(remaining_space < search_box_max_width){
        app_bar_two_lines = true;
        search_box_inital_right_pos = $(window).width() - ($('#SearchBox').offset().left + $('#SearchBox').outerWidth(true));
        search_box_final_right_pos = $(window).width()/2 - search_box_max_width/2;
    }else{
        search_box_final_right_pos = $(window).width()/2 - search_box_max_width/2 - $('#menu-container').outerWidth(true);
    }

    $(window).resize(function() {

        app_bar_two_lines = false;
        remaining_space = 0;
        search_box_inital_right_pos = 0;
        search_box_final_right_pos = 0;
        remaining_space = $(window).width() - $('#Logo').outerWidth(true) - $('#menu-container').outerWidth(true) - $('#SearchBox').outerWidth(true);

        if(remaining_space < search_box_max_width){
            app_bar_two_lines = true;
            search_box_inital_right_pos = $(window).width() - ($('#SearchBox').offset().left + $('#SearchBox').outerWidth(true));
            search_box_final_right_pos = $(window).width()/2 - search_box_max_width/2;
        }else{
            search_box_final_right_pos = $(window).width()/2 - search_box_max_width/2 - $('#menu-container').outerWidth(true);
        }

    });

    // OPEN THE SEARCH
    $('#Form_Go').click(function( event ) {

        if(!$('#SearchBox').hasClass('expanded')){

            event.preventDefault();

            if(app_bar_two_lines){

                $('#SearchBox').css({
                    'position' : 'absolute',
                    'right'    : search_box_inital_right_pos
                })

                $('#Head').stop().animate({
                    'height' : (head_height * 2)+'px'
                },500,'easeInOutQuint');

                $('#SearchBox').animate({
                    'right'        : ($(window).width()/2 - 24)+'px',
                },500,'easeInOutQuint', function(){

                    $('#SearchBox').animate({
                        'width'         : search_box_max_width+'px',
                        'right'         : search_box_final_right_pos,
                        'padding-right' : '48px',
                        'padding-left'  : '48px'
                    },500,'easeInOutQuint', function(){
                        $('#Form_Search').focus();
                    });
                    $('#Form_Close').fadeIn(500,'easeInOutQuint');

                });

            }else{

                $('#SearchBox').stop().animate({
                    'right'         : search_box_final_right_pos,
                    'width'         : search_box_max_width+'px',
                    'padding-right' : '48px',
                    'padding-left'  : '48px'
                },500,'easeInOutQuint', function(){
                    $('#Form_Search').focus();
                });

                $('#Form_Close').fadeIn(500,'easeInOutQuint');

            }

            $('#SearchBox').addClass('expanded');

        }
    });

    // CLOSE THE SEARCH
    $('#Form_Close').click(function( event ) {
        event.preventDefault();

        $('#Form_Close').fadeOut(500,'easeInOutQuint');

        if(app_bar_two_lines){

            $('#SearchBox').stop().animate({
                'right'         : ($(window).width()/2 - 24)+'px',
                'width'         : '0',
                'padding-right' : '24px',
                'padding-left' : '24px'
            },500,'easeInOutQuint',function(){

                    $('#Head').stop().delay(100).animate({
                        'height'         : head_height+'px'
                    },500,'easeInOutQuint');

                    $('#SearchBox').animate({
                        'right'        : search_box_inital_right_pos
                    },500,'easeInOutQuint', function(){

                        $('#SearchBox').css({
                            'position' : 'relative',
                            'right'    : 0
                        });

                    });

            });

        }else{

            $('#SearchBox').stop().animate({
                'right'         : '0',
                'width'         : '0',
                'padding-right' : '24px',
                'padding-left' : '24px'
            },500,'easeInOutQuint');

        }

        $('#SearchBox').removeClass('expanded');
    });

    // WAVES ON BUTTONS
    $('.MeButton, .MeBox .UserPhoto, .MeBox .Username, .MenuItems a, #Form_Go, #Menu a, #Panel .FilterMenu li a, .PanelInfo li a, #DiscussionForm a.Cancel, a.ProfileButtons').not('#Panel .Box.BoxDiscussions li a').addClass('waves-effect waves-button'); // FLAT BUTTONS
    $('.Button').addClass('waves-effect waves-button waves-float'); // FLOATED BUTTONS
    $('#menu-container .collapse-button').addClass('waves-effect waves-circle'); // WAVES ON DIV

    Waves.displayEffect({
        duration: 1500
    });

    // PANEL MENU - ADJUST THE PADDING RIGHT BECAUSE OF THE COUNTER
    /*
    $('.PanelInfo li a').each(function(index) {
        if($(this).find('.Count')){
            var initial_padding_right = $(this).css('padding-right');
            $(this).css('padding-right',(parseInt(initial_padding_right) + $(this).find('.Count').outerWidth())+'px');
        }
    });
    */

    // PAGER - IF SPRITE DEFINED THEN REMOVE THE TEXT
    if($('.NumberedPager').length){

        var pager_image = $('.NumberedPager .Previous').css('background-image');

        var filename = pager_image.substring(pager_image.lastIndexOf('/')+1);
        if(filename!='none')
            $('.NumberedPager .Previous, .NumberedPager .Next').css('text-indent','-9999px');
    }

    // DISCUSSIONS LIST
    if($('body.Discussions').length || $('body.Categories').length){

        // AUTO HIDE THE BOOKMARK
        if(auto_hide_bookmarks){
            $('.Item .Bookmark').filter(':not(.Bookmarked)').css('visibility','hidden');
            $('.Item').hover(function () {
                $(this).find('.Bookmark').css('visibility','visible');
            },function () {
                if(!$(this).find('.Bookmark').hasClass('Bookmarked'))
                    $(this).find('.Bookmark').css('visibility','hidden');
            });
        }

        // AUTO HIDE THE ADMIN CHECKBOXES
        if(auto_hide_admin_checkboxes){

            if($('body.Profile').length){

                $('.AdminCheck').hide();

            }else{

                $('.TopAdminCheck input').prop('checked', false);

                $('.AdminCheck input:not(:checked)').parent().hide();
                if($('.AdminCheck input').is(':checked'))
                    $('.TopAdminCheck').show();
                else
                    $('.TopAdminCheck').hide();
                var all_clicked = true;
                $('.AdminCheck input').each(function(index) {
                    if(!$(this).is(':checked') && !$(this).parent().hasClass('TopAdminCheck'))
                        all_clicked = false;
                    if($(this).is(':checked'))
                        $(this).parent().show();
                });
                if(all_clicked)
                    $('.TopAdminCheck input').prop('checked', true);
                else
                    $('.TopAdminCheck input').prop('checked', false);

                $('.TopAdminCheck input').click(function() {
                    if($(this).is(':checked')){
                        $('.AdminCheck').show();
                    }else{
                        $('.TopAdminCheck').hide();
                        $('.AdminCheck').hide();
                    }
                });
                $('.AdminCheck input').click(function() {
                    if($('.AdminCheck input').is(':checked'))
                        $('.TopAdminCheck').show();
                    else
                        $('.TopAdminCheck').hide();

                    var all_clicked = true;
                    $('.AdminCheck input').each(function(index) {
                        if(!$(this).is(':checked') && !$(this).parent().hasClass('TopAdminCheck'))
                            all_clicked = false;
                    });

                    if(all_clicked)
                        $('.TopAdminCheck input').prop('checked', true);
                    else
                        $('.TopAdminCheck input').prop('checked', false);
                });

                $('.DataList .Item, .DataTable .Item').hover(function () {
                    $(this).find('.AdminCheck').show();
                },function () {
                    if(!$(this).find('.AdminCheck input').is(':checked'))
                        $(this).find('.AdminCheck').hide();
                });

            }

        }

    }

    // DISCUSSIONS ITEM
    if($('body.Discussion').length){

        // AUTO HIDE THE ADMIN CHECKBOXES
        if(auto_hide_admin_checkboxes){

            autoHideAdminCheckboxesDiscussionsItem();

            $(document).on('CommentAdded', function() {
                autoHideAdminCheckboxesDiscussionsItem();
            });

        }

        // PLUGIN - IN THIS DISCUSSION - ADJUST THE PANEL INFO COUNTS AND DATE
        $('.PanelInfo li').contents().filter(function() {
            return this.nodeType != 1 && $.trim(this.nodeValue).length;
        }).wrap('<span/>');
        $('.PanelInfo li').each(function(i,li){
            var elem = $('.PanelInfo li:eq('+i+') strong > a');
            $('.PanelInfo li:eq('+i+') > span').appendTo(elem);
            elem.css('white-space','normal');
        });
        $('.PanelInfo').resize();

        // PLUGIN - VOTING - REARRANGE THE VOTER
        if($('.Voter').length){
            $('h2.CommentHeading').hide();
            $('.Voter').each(function(index) {
                //$(this).appendTo($(this).parents('.Item').find('.Item-Header'));
                if($(this).parents('.Item').find('.Item-Header').outerHeight() > $(this).parents('.Item').find('.Item-Body').outerHeight())
                    $(this).parents('.Item').find('.Item-Body').height($(this).parents('.Item').find('.Item-Header').outerHeight());
            });
        }
        $(document).on('CommentAdded', function() {
            $('.Voter').each(function(index) {
                //$(this).appendTo($(this).parents('.Item').find('.Item-Header'));
                if($(this).parents('.Item').find('.Item-Header').outerHeight() > $(this).parents('.Item').find('.Item-Body').outerHeight())
                    $(this).parents('.Item').find('.Item-Body').height($(this).parents('.Item').find('.Item-Header').outerHeight());
            });
        });

        // PLUGIN - ADVANCED EDITOR - HIDE ELEMENTS ON PREVIEW POST
        if($('.editor').length){
            $(document).on('click', 'a.PreviewButton', function() {
                $('.editor, .editor-upload-progress, .editor-upload-previews .editor-file-remove, .editor-help-text').hide();
            });
            $(document).on('click', 'a.WriteButton', function() {
                $('.editor, .editor-upload-progress, .editor-upload-previews .editor-file-remove, .editor-help-text').show();
            });
        }

    }

    // ACTIVITIES
    if($('body.Activity').length){
/*
        $('body.Activity').append('<div class="CommentLinkHover" style="display: none;"></div>')
        var commentLinkHoverColor = $('.CommentLinkHover').css('color');
        $('.CommentLinkHover').remove();

        $('.CommentLink').hover(function () {
            $(this).attr('style','color: '+commentLinkHoverColor+' !important');
        },function () {
            $(this).attr('style','');
        });
*/
    }

    // PROFILE
    if($('body.Profile').length){

        // PUT THE PROFILE OPTIONS BUTTON INSIDE THE HEADER
        if($('body.EditMode').length)
            $('.ProfileOptions').prependTo('h2.H');
        else
            $('.ProfileOptions').prependTo('h1.H');

    }

    // VOTING PLUGIN INSTALLED
    if($('.StatBox').length){

        $('.Item > .StatBox.VotesBox').each(function(){
            $(this).appendTo($(this).parents('.Item').find('.ItemContent > .StatBoxContainer'));
        });

        var statbox_sprite = $('.StatBox').css('background-image');

        var filename = statbox_sprite.substring(statbox_sprite.lastIndexOf('/')+1);
        if(filename!='none')
            $('head').append('<style>.StatBox span{display:none}</style>');

        // ADD A TITLE HOVER THE STATBOX IMAGES
        //$('.ViewsBox').attr('title','Views');
        //$('.AnswersBox').attr('title','Comments');

        // VERTICAL ALIGN THE STATBOXES
        /*
        if(vertical_align_statboxes){
            $('.Discussions > li').each(function(){
                var li_height = $(this).height();
                var stat_height = $(this).find('.StatBox').outerHeight();
                var res = parseInt((li_height - stat_height) / 2);
                $(this).find('.StatBox, .Meta strong').css('marginTop',res+'px');
            });
        }
        */

    }

    // REGISTER INPUT AUTOFOCUS
    $('#Form_User_Register ul input:first').focus();

});