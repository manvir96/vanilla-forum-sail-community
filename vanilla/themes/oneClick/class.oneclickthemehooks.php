<?php if (!defined('APPLICATION')) exit();

class OneClickThemeHooks implements Gdn_IPlugin {

   public function Setup() {

      // One Click Theme Options
      SaveToConfig('ThemeOption.CurrentStyle','material-blue');
      SaveToConfig('ThemeOption.PreviewStyle',TRUE);
      SaveToConfig('ThemeOption.ExtraStyle','material-blue');

	  // Set the order for the modules (make sure CategoriesModule is set).
	  SaveToConfig('Modules.Vanilla.Panel', array('MeModule', 'UserBoxModule', 'GuestModule', 'NewDiscussionModule', 'DiscussionFilterModule', 'CategoriesModule', 'SignedInModule', 'Ads'));

      // Set the Thumbnail Size
      SaveToConfig('Garden.Profile.MaxHeight', 300);
      SaveToConfig('Garden.Profile.MaxWidth', 300);
      SaveToConfig('Garden.Thumbnail.Size',150);
      SaveToConfig('Garden.Thumbnail.Width',150);

      // Set the Mobile Theme
      SaveToConfig('Garden.MobileTheme','oneClick');

      // Tagging Plugin
      SaveToConfig('Plugins.Tagging.DisableInline',FALSE);
      SaveToConfig('Plugins.Tagging.EnableInMeta',TRUE);


   }

   public function OnDisable() {

      // Removes the One Click Theme Options
      RemoveFromConfig('ThemeOption.CurrentStyle');
      RemoveFromConfig('ThemeOption.PreviewStyle');
      RemoveFromConfig('ThemeOption.ExtraStyle');

      // Restore the default order of the modules
	  RemoveFromConfig('Modules.Vanilla.Panel');

      // Restore the default Thumbnail Size
      RemoveFromConfig('Garden.Profile.MaxHeight');
      RemoveFromConfig('Garden.Profile.MaxWidth');
      RemoveFromConfig('Garden.Thumbnail.Size');
      RemoveFromConfig('Garden.Thumbnail.Width');

      // Restore the default Mobile Theme
      RemoveFromConfig('Garden.MobileTheme');

      // Restore the Tagging Plugin configuration
      RemoveFromConfig('Plugins.Tagging.DisableInline');
      RemoveFromConfig('Plugins.Tagging.EnableInMeta');
   }

    public function SettingsController_BeforeRegistrationUpdate_Handler($Sender) {

        $Sender->EventArguments['ConfigurationModel']->SetField(array(
             'Garden.Registration.EnvatoUsername',
             'Garden.Registration.EnvatoAPIKey'
        ));

    }

    public function EntryController_RegisterValidation_Handler($Sender) {

        if(C('Garden.Registration.Method')=='Envato'){

            $Sender->InvitationCode = $Sender->Form->GetValue('InvitationCode');

            $EnvatoPurchasecode = $Sender->InvitationCode;
            $EnvatoUsername = Gdn::Config('Garden.Registration.EnvatoUsername', '');
            $EnvatoAPIKey = Gdn::Config('Garden.Registration.EnvatoAPIKey', '');
            $url = 'http://marketplace.envato.com/api/edge/'.$EnvatoUsername.'/'.$EnvatoAPIKey.'/verify-purchase:'.$EnvatoPurchasecode.'.json';

            if (function_exists('curl_init')) {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_HEADER, 0);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_USERAGENT, $EnvatoUsername);
                $json_res = curl_exec($ch);
                curl_close($ch);
            }else{
                $json_res = file_get_contents($url);
            }
            $data = json_decode($json_res,true);
            if(empty($data['verify-purchase'])){
                $Sender->UserModel->Validation->AddValidationResult('InvitationCode', T('The provided purchase code is invalid.'));
            }

            // Add validation rules that are not enforced by the model
            //$Sender->UserModel->DefineSchema();
            //$Sender->UserModel->Validation->ApplyRule('Name', 'Username', $Sender->UsernameError);
            //$Sender->UserModel->Validation->ApplyRule('TermsOfService', 'Required', T('You must agree to the terms of service.'));
            //$Sender->UserModel->Validation->ApplyRule('Password', 'Required');
            //$Sender->UserModel->Validation->ApplyRule('Password', 'Match');
            //$Sender->UserModel->Validation->ApplyRule('DateOfBirth', 'MinimumAge');

            try {
                $Values = $Sender->Form->FormValues();
                unset($Values['Roles']);
                $AuthUserID = $Sender->UserModel->Register($Values);

                if (!$AuthUserID) {
                    $Sender->Form->SetValidationResults($Sender->UserModel->ValidationResults());
                } else {
                    // The user has been created successfully, so sign in now.
                    Gdn::Session()->Start($AuthUserID);
                    if ($Sender->Form->GetFormValue('RememberMe'))
                        Gdn::Authenticator()->SetIdentity($AuthUserID, TRUE);

                    $Sender->FireEvent('RegistrationSuccessful');
                    $Sender->UserModel->SendWelcomeEmail($AuthUserID, '', 'Register');
                    // ... and redirect them appropriately
                    $Route = $Sender->RedirectTo();
                    if ($Sender->_DeliveryType != DELIVERY_TYPE_ALL) {
                        $Sender->RedirectUrl = Url($Route);
                    } else {
                        if ($Route !== FALSE)
                            Redirect($Route);
                    }
                }
            } catch (Exception $Ex) {
                $Sender->Form->AddError($Ex);
            }

            $Sender->Render();

            exit;

        }

    }


    // INDEX PHOTOS PLUGIN

   /**
    * Trigger on All Discussions.
    */
   public function DiscussionsController_BeforeDiscussionContent_Handler($Sender) {
      $this->DisplayPhoto($Sender);
   }

   /**
    * Trigger on Categories.
    */
   public function CategoriesController_BeforeDiscussionContent_Handler($Sender) {
      $this->DisplayPhoto($Sender);
   }

   /**
    * Trigger on Profile Discussions.
    */
   public function ProfileController_BeforeDiscussionContent_Handler($Sender) {
      $this->DisplayPhoto($Sender);
   }

   /**
    * Display user photo for first user in each discussion.
    */
   protected function DisplayPhoto($Sender) {
      // Build user object & output photo
      $FirstUser = UserBuilder($Sender->EventArguments['Discussion'], 'First');
      echo '<div class="Author Photo">'.UserPhoto($FirstUser).'</div>';
   }


   // TAGGING PLUGIN
   /**
    * Show tags after discussion meta, instead the default after body
    */
   public function DiscussionController_DiscussionInfo_Handler($Sender) {

      if (!C('EnabledPlugins.Tagging',FALSE) or !C('Plugins.Tagging.EnableInMeta', TRUE))
         return;

      if (!property_exists($Sender->EventArguments['Object'], 'CommentID')) {
         $DiscussionID = property_exists($Sender, 'DiscussionID') ? $Sender->DiscussionID : 0;

         if (!$DiscussionID)
            return;

         $TagModule = new TagModule($Sender);
         echo $TagModule->InlineDisplay();
      }
   }

    // BASIC PAGES APPLICATION
    /**
    * Hides the sidebar discussion module and recent activity
    */
    public function PageController_Render_Before($Sender) {

        //$Page = $Sender->Data('Page');
        // For example, you can use $Page->PageID, $Page->Name, $Page->UrlCode, etc.
        //if($Page->Name === 'Terms of Use') {
            unset($Sender->Assets['Panel']['NewDiscussionModule']);
            unset($Sender->Assets['Panel']['DiscussionFilterModule']);
            unset($Sender->Assets['Panel']['BookmarkedModule']);
            //unset($Sender->Assets['Panel']['DiscussionsModule']);
            unset($Sender->Assets['Panel']['RecentActivityModule']);
        //}
    }

    public function Base_Render_Before($Sender, $Args) {

        // ENVATO REGISTRATION VIEW
        if(C('Garden.Registration.Method')=='Envato' and $Sender->RequestMethod=='register'){
            $Sender->View='RegisterEnvato';
        }

        // MOVE THE MESSAGES FROM THE CONTENT ASSET TO THE MESSAGES ASSET
        unset(Gdn::Controller()->Assets['Content']['MessageModule']);
        unset(Gdn::Controller()->Assets['Panel']['MessageModule']);

        $Location = $Sender->Application.'/'.substr($Sender->ControllerName, 0, -10).'/'.$Sender->RequestMethod;
        $Exceptions = array('[Base]');
        if (in_array($Sender->MasterView, array('', 'default')))
            $Exceptions[] = '[NonAdmin]';

        $MessageModel = new MessageModel();
        $MessageData = $MessageModel->GetMessagesForLocation($Location, $Exceptions);
        foreach ($MessageData as $Message) {

            if($Message['AssetTarget'] == 'Content')
                $Message['AssetTarget'] = 'Messages';

            $MessageModule = new MessageModule($Sender, $Message);
            if ($SignInOnly) // Insert special messages even in SignIn popup
                echo $MessageModule;
            elseif ($Sender->DeliveryType() == DELIVERY_TYPE_ALL)
                $Sender->AddModule($MessageModule);
        }

    }

}