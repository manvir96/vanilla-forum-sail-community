<?php

$StyleInfo['material-amber'] = array(
    'Name'  => 'Material Amber',
    'Description'  => 'A style based on Material Design with a colored header and footer',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-19 17:58:01',
    'LastUpdate'  => '2017-01-06 21:32:03',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, amber',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 14
);

?>