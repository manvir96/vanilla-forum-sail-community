<?php

$StyleInfo['material-indigo'] = array(
    'Name'  => 'Material Indigo',
    'Description'  => 'A style based on Material Design with a colored header and footer',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-19 17:40:27',
    'LastUpdate'  => '2017-01-06 21:31:31',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, indigo',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 5
);

?>