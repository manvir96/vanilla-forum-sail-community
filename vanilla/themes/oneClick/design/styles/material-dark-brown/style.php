<?php

$StyleInfo['material-dark-brown'] = array(
    'Name'  => 'Material Dark Brown',
    'Description'  => 'A style based on Material Design with a dark look and accent colors on links and buttons',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-26 19:55:05',
    'LastUpdate'  => '2017-01-06 21:33:30',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, dark, brown',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 36
);

?>