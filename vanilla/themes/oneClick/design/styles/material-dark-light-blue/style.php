<?php

$StyleInfo['material-dark-light-blue'] = array(
    'Name'  => 'Material Dark Light Blue',
    'Description'  => 'A style based on Material Design with a dark look and accent colors on links and buttons',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-26 19:40:11',
    'LastUpdate'  => '2017-01-06 21:32:52',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, dark, light blue',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 26
);

?>