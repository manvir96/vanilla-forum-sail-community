<?php

$StyleInfo['material-dark-green'] = array(
    'Name'  => 'Material Dark Green',
    'Description'  => 'A style based on Material Design with a dark look and accent colors on links and buttons',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-26 13:17:38',
    'LastUpdate'  => '2017-01-06 21:33:03',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, dark, green',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 29
);

?>