<?php

$StyleInfo['material-dark-deep-purple'] = array(
    'Name'  => 'Material Dark Deep Purple',
    'Description'  => 'A style based on Material Design with a dark look and accent colors on links and buttons',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-26 19:38:51',
    'LastUpdate'  => '2017-01-06 21:32:39',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, dark, deep purple',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 23
);

?>