<?php

$StyleInfo['material-dark-indigo'] = array(
    'Name'  => 'Material Dark Indigo',
    'Description'  => 'A style based on Material Design with a dark look and accent colors on links and buttons',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-26 19:39:55',
    'LastUpdate'  => '2017-01-06 21:32:43',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, dark, indigo',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 24
);

?>