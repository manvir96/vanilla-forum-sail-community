<?php

$StyleInfo['material-dark-yellow'] = array(
    'Name'  => 'Material Dark Yellow',
    'Description'  => 'A style based on Material Design with a dark look and accent colors on links and buttons',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-26 19:47:32',
    'LastUpdate'  => '2017-01-06 21:33:15',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, dark, yellow',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 32
);

?>