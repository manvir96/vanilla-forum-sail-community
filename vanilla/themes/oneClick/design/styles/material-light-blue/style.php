<?php

$StyleInfo['material-light-blue'] = array(
    'Name'  => 'Material Light Blue',
    'Description'  => 'A style based on Material Design with a colored header and footer',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-19 17:43:10',
    'LastUpdate'  => '2017-01-06 21:31:41',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, light blue',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 7
);

?>