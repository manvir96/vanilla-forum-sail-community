<?php

$StyleInfo['material-blue-grey'] = array(
    'Name'  => 'Material Blue Grey',
    'Description'  => 'A style based on Material Design with a colored header and footer',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-19 18:10:35',
    'LastUpdate'  => '2017-01-06 21:32:18',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, blue grey',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 19
);

?>