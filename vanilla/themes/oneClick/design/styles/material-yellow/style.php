<?php

$StyleInfo['material-yellow'] = array(
    'Name'  => 'Material Yellow',
    'Description'  => 'A style based on Material Design with a colored header and footer',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-19 17:55:19',
    'LastUpdate'  => '2017-01-06 21:32:00',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, yellow',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 13
);

?>