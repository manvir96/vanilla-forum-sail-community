<?php

$StyleInfo['material-dark-blue-grey'] = array(
    'Name'  => 'Material Dark Blue Grey',
    'Description'  => 'A style based on Material Design with a dark look and accent colors on links and buttons',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-26 20:00:30',
    'LastUpdate'  => '2017-01-06 21:33:38',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, dark, blue grey',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 38
);

?>