<?php

$StyleInfo['material-dark-blue'] = array(
    'Name'  => 'Material Dark Blue',
    'Description'  => 'A style based on Material Design with a dark look and accent colors on links and buttons',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-25 20:23:19',
    'LastUpdate'  => '2017-01-06 21:32:47',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, dark, blue',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 25
);

?>