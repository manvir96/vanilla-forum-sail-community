<?php

$StyleInfo['material-dark-purple'] = array(
    'Name'  => 'Material Dark Purple',
    'Description'  => 'A style based on Material Design with a dark look and accent colors on links and buttons',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-26 19:37:21',
    'LastUpdate'  => '2017-01-06 21:32:34',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, dark, purple',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 22
);

?>