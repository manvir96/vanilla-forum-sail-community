<?php

$StyleInfo['material-dark-lime'] = array(
    'Name'  => 'Material Dark Lime',
    'Description'  => 'A style based on Material Design with a dark look and accent colors on links and buttons',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-26 19:45:56',
    'LastUpdate'  => '2017-01-06 21:33:12',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, dark, lime',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 31
);

?>