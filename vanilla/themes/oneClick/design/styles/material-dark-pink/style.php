<?php

$StyleInfo['material-dark-pink'] = array(
    'Name'  => 'Material Dark Pink',
    'Description'  => 'A style based on Material Design with a dark look and accent colors on links and buttons',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-26 19:35:31',
    'LastUpdate'  => '2017-01-06 21:32:29',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, dark, pink',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 21
);

?>