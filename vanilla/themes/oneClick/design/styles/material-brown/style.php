<?php

$StyleInfo['material-brown'] = array(
    'Name'  => 'Material Brown',
    'Description'  => 'A style based on Material Design with a colored header and footer',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-19 18:04:43',
    'LastUpdate'  => '2017-01-06 21:32:12',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, brown',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 17
);

?>