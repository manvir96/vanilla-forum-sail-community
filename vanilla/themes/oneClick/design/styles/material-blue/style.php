<?php

$StyleInfo['material-blue'] = array(
    'Name'  => 'Material Blue',
    'Description'  => 'A style based on Material Design with a colored header and footer',
    'Version'  => '1.0',
    'DateCreated' => '2015-03-17 16:59:40',
    'LastUpdate'  => '2017-01-06 21:31:38',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, blue',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 6
);

?>