<?php

$StyleInfo['material-orange'] = array(
    'Name'  => 'Material Orange',
    'Description'  => 'A style based on Material Design with a colored header and footer',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-19 18:00:21',
    'LastUpdate'  => '2017-01-06 21:32:07',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, orange',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 15
);

?>