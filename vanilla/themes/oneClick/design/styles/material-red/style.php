<?php

$StyleInfo['material-red'] = array(
    'Name'  => 'Material Red',
    'Description'  => 'A style based on Material Design with a colored header and footer',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-19 17:18:54',
    'LastUpdate'  => '2017-01-06 21:31:15',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, red',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 1
);

?>