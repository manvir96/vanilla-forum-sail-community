<?php

$StyleInfo['material-cyan'] = array(
    'Name'  => 'Material Cyan',
    'Description'  => 'A style based on Material Design with a colored header and footer',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-19 17:45:43',
    'LastUpdate'  => '2017-01-06 21:31:45',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, cyan',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 8
);

?>