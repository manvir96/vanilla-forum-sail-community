<?php

$StyleInfo['material-pink'] = array(
    'Name'  => 'Material Pink',
    'Description'  => 'A style based on Material Design with a colored header and footer',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-19 17:27:05',
    'LastUpdate'  => '2017-01-06 21:31:20',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, pink',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 2
);

?>