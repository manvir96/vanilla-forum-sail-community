<?php

$StyleInfo['material-grey'] = array(
    'Name'  => 'Material Grey',
    'Description'  => 'A style based on Material Design with a colored header and footer',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-19 18:06:47',
    'LastUpdate'  => '2017-01-06 21:32:15',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, grey',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 18
);

?>