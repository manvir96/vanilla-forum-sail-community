<?php

$StyleInfo['material-dark-deep-orange'] = array(
    'Name'  => 'Material Dark Deep Orange',
    'Description'  => 'A style based on Material Design with a dark look and accent colors on links and buttons',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-26 19:51:32',
    'LastUpdate'  => '2017-01-06 21:33:26',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, dark, deep orange',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 35
);

?>