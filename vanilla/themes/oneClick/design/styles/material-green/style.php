<?php

$StyleInfo['material-green'] = array(
    'Name'  => 'Material Green',
    'Description'  => 'A style based on Material Design with a colored header and footer',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-19 16:49:24',
    'LastUpdate'  => '2017-01-06 21:31:50',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, green',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 10
);

?>