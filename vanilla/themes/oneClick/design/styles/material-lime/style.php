<?php

$StyleInfo['material-lime'] = array(
    'Name'  => 'Material Lime',
    'Description'  => 'A style based on Material Design with a colored header and footer',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-19 17:53:09',
    'LastUpdate'  => '2017-01-06 21:31:56',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, lime',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 12
);

?>