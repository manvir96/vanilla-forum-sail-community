<?php

$StyleInfo['material-purple'] = array(
    'Name'  => 'Material Purple',
    'Description'  => 'A style based on Material Design with a colored header and footer',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-19 17:33:00',
    'LastUpdate'  => '2017-01-06 21:31:24',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, purple',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 3
);

?>