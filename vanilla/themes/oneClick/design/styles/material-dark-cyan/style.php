<?php

$StyleInfo['material-dark-cyan'] = array(
    'Name'  => 'Material Dark Cyan',
    'Description'  => 'A style based on Material Design with a dark look and accent colors on links and buttons',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-26 19:41:33',
    'LastUpdate'  => '2017-01-06 21:32:55',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, dark, cyan',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 27
);

?>