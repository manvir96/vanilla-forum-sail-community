<?php

$StyleInfo['system'] = array(
    'Name'               => 'System',
    'Description'        => 'The template structure to build new styles and the system images',
    'Version'            => '1',
    'DateCreated'        => '2013-01-28',
    'LastUpdate'         => '2013-01-28',
    'Author'             => 'Creative Dreams',
    'AuthorUrl'          => 'www.one-click-forum.com',
    'Tags'               => '',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 0
);

?>