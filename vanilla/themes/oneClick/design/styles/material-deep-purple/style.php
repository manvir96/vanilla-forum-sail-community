<?php

$StyleInfo['material-deep-purple'] = array(
    'Name'  => 'Material Deep Purple',
    'Description'  => 'A style based on Material Design with a colored header and footer',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-19 17:36:11',
    'LastUpdate'  => '2017-01-06 21:31:35',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, deep purple',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 4
);

?>