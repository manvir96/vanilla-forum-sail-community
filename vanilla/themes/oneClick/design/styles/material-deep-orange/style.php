<?php

$StyleInfo['material-deep-orange'] = array(
    'Name'  => 'Material Deep Orange',
    'Description'  => 'A style based on Material Design with a colored header and footer',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-19 18:02:23',
    'LastUpdate'  => '2017-01-06 21:32:09',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, deep orange',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 16
);

?>