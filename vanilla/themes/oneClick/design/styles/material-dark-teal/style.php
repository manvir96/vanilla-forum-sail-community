<?php

$StyleInfo['material-dark-teal'] = array(
    'Name'  => 'Material Dark Teal',
    'Description'  => 'A style based on Material Design with a dark look and accent colors on links and buttons',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-26 19:43:00',
    'LastUpdate'  => '2017-01-06 21:32:59',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, dark, teal',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 28
);

?>