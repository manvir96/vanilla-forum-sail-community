<?php

$StyleInfo['material-dark-red'] = array(
    'Name'  => 'Material Dark Red',
    'Description'  => 'A style based on Material Design with a dark look and accent colors on links and buttons',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-26 19:33:41',
    'LastUpdate'  => '2017-01-06 21:32:25',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, dark, red',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 20
);

?>