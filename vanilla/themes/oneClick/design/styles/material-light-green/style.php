<?php

$StyleInfo['material-light-green'] = array(
    'Name'  => 'Material Light Green',
    'Description'  => 'A style based on Material Design with a colored header and footer',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-19 17:50:37',
    'LastUpdate'  => '2017-01-06 21:31:53',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, light green',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 11
);

?>