<?php

$StyleInfo['material-dark-light-green'] = array(
    'Name'  => 'Material Dark LIght Green',
    'Description'  => 'A style based on Material Design with a dark look and accent colors on links and buttons',
    'Version'  => '1.0',
    'DateCreated' => '2015-06-26 19:44:38',
    'LastUpdate'  => '2017-01-06 21:33:08',
    'Author'  => 'Creative Dreams',
    'AuthorUrl'  => 'www.one-click-forum.com',
    'Tags'  => 'material, dark, light green',
    'StyleEditorVersion' => '1.0',
    'System'             => true,
    'Order'              => 30
);

?>