<?php

$fonts = array(

    // System Fonts
    array(
        'name'   => 'Arial',
        'family' => 'Arial, Helvetica, sans-serif',
        'type'   => 'System'
    ),
    array(
        'name'   => 'Courier New',
        'family' => "'Courier New', Courier, monospace",
        'type'   => 'System'
    ),
    array(
        'name'   => 'Tahoma',
        'family' => 'Tahoma, Geneva, sans-serif',
        'type'   => 'System'
    ),
    array(
        'name'   => 'Trebuchet MS',
        'family' => "'Trebuchet MS', Helvetica, sans-serif",
        'type'   => 'System'
    ),
    array(
        'name'   => 'Verdana',
        'family' => 'Verdana, Geneva, sans-serif',
        'type'   => 'System'
    ),

    // Google Fonts
    array(
        'name'   => 'Arvo',
        'link'   => 'Arvo:400,400italic,700,700italic',
        'family' => "'Arvo', serif",
        'type'   => 'Google'
    ),
    array(
        'name'   => 'Bevan',
        'link'   => 'Bevan',
        'family' => "'Bevan', cursive",
        'type'   => 'Google'
    ),
    array(
        'name'   => 'Droid Sans',
        'link'   => 'Droid+Sans:400,700',
        'family' => "'Droid Sans\', sans-serif",
        'type'   => 'Google'
    ),
    array(
        'name'   => 'Droid Serif',
        'link'   => 'Droid+Serif:400,400italic,700,700italic',
        'family' => "'Droid Serif', serif",
        'type'   => 'Google'
    ),
    array(
        'name'   => 'Exo',
        'link'   => 'Exo:400,100,100italic,200,200italic,300italic,300,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic',
        'family' => "'Exo', sans-serif",
        'type'   => 'Google'
    ),
    array(
        'name'   => 'Josefin Sans',
        'link'   => 'Josefin+Sans:400,100,100italic,300,300italic,400italic,600,600italic,700,700italic',
        'family' => "'Josefin Sans', sans-serif",
        'type'   => 'Google'
    ),
    array(
        'name'   => 'Josefin Slab',
        'link'   => 'Josefin+Slab:400,100,100italic,300,300italic,400italic,600,600italic,700,700italic',
        'family' => "'Josefin Slab', serif",
        'type'   => 'Google'
    ),
    array(
        'name'   => 'Lato',
        'link'   => 'Lato:400,100,100italic,300italic,300,400italic,700,700italic,900,900italic',
        'family' => "'Lato', sans-serif",
        'type'   => 'Google'
    ),
    array(
        'name'   => 'Lobster',
        'link'   => 'Lobster',
        'family' => "'Lobster', cursive",
        'type'   => 'Google'
    ),
    array(
        'name'   => 'Lora',
        'link'   => 'Lora',
        'family' => "'Lora', serif",
        'type'   => 'Google'
    ),
    array(
        'name'   => 'Nunito',
        'link'   => 'Nunito:400,300,700',
        'family' => "'Nunito', sans-serif",
        'type'   => 'Google'
    ),
    array(
        'name'   => 'Open Sans',
        'link'   => 'Open+Sans:400,300,300italic,600,600italic,700,700italic,800,800italic',
        'family' => "'Open Sans', sans-serif",
        'type'   => 'Google'
    ),
    array(
        'name'   => 'Open Sans Condensed',
        'link'   => 'Open+Sans+Condensed:300,300italic,700',
        'family' => "'Open Sans Condensed', sans-serif",
        'type'   => 'Google'
    ),
    array(
        'name'   => 'Oswald',
        'link'   => 'Oswald:400,300,700',
        'family' => "'Oswald', sans-serif",
        'type'   => 'Google'
    ),
    array(
        'name'   => 'PT Sans',
        'link'   => 'PT+Sans:400,400italic,700,700italic',
        'family' => "'PT Sans', sans-serif",
        'type'   => 'Google'
    ),
    array(
        'name'   => 'PT Sans Narrow',
        'link'   => 'PT+Sans+Narrow:400,700',
        'family' => "'PT Sans Narrow', sans-serif",
        'type'   => 'Google'
    ),
    array(
        'name'   => 'Roboto',
        'link'   => 'Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic',
        'family' => "'Roboto', sans-serif",
        'type'   => 'Google'
    ),
    array(
        'name'   => 'Roboto Condensed',
        'link'   => 'Roboto+Condensed:400,300,300italic,400italic,700,700italic',
        'family' => "'Roboto Condensed', sans-serif",
        'type'   => 'Google'
    ),
    array(
        'name'   => 'Ubuntu',
        'link'   => 'Ubuntu:400,300,300italic,400italic,500italic,500,700,700italic',
        'family' => "'Ubuntu', sans-serif",
        'type'   => 'Google'
    ),
    array(
        'name'   => 'Ubuntu Condensed',
        'link'   => 'Ubuntu+Condensed',
        'family' => "'Ubuntu Condensed', sans-serif",
        'type'   => 'Google'
    ),
    array(
        'name'   => 'Wire One',
        'link'   => 'Wire+One',
        'family' => "'Wire One', sans-serif",
        'type'   => 'Google'
    ),
    array(
        'name'   => 'Yanone Kaffeesatz',
        'link'   => 'Yanone+Kaffeesatz:400,200,300,700',
        'family' => "'Yanone Kaffeesatz', sans-serif",
        'type'   => 'Google'
    )
);

?>