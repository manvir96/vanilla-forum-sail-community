The fonts needs to have this structure:

1. go to http://www.fontsquirrel.com
2. donwload a font (@font-face)
3. build a folder with the name of the font
4. You must rename the name of the fonts to

NAME _ WEIGHT _ STYLE _ STRETCH _ VARIANT -webfont.ext

EXAMPLES:

NAME:    quicksand
WEIGHT:  normal
STYLE:   normal
STRETCH: normal
VARIANT: normal

quicksand_normal_normal_normal-webfont.ttf
quicksand-webfont.ttf

* you can hide defaul values

----------------

FONT-WEIGHT
Thin			- 100
ExtraLight, UltraLight  - 200
Light			- 300
Normal, Regular, Book	- 400 (default)
Medium			- 500
SemiBold, DemiBold	- 600
Bold			- 700
ExtraBold, UltraBold	- 800
Black, Heavy		- 900

FONT-STYLE
Normal 	The browser displays a normal font style. This is default
Italic 	The browser displays an italic font style
Oblique The browser displays an oblique font style

FONT-STRETCH
UltraCondensed Makes the text as narrow as it gets
ExtraCondensed Makes the text narrower than condensed, but not as narrow as ultra-condensed
Condensed 	Makes the text narrower than semi-condensed, but not as narrow as extra-condensed
SemiCondensed 	Makes the text narrower than normal, but not as narrow as condensed
Normal 		Default value. No font stretching
SemiExpanded 	Makes the text wider than normal, but not as wide as expanded
Expanded 	Makes the text wider than semi-expanded, but not as wide as extra-expanded
ExtraExpanded 	Makes the text wider than expanded, but not as wide as ultra-expanded
UltraExpanded 	Makes the text as wide as it gets

FONT-VARIANT
normal 		The browser displays a normal font. This is default
small-caps 	The browser displays a small-caps font
SC