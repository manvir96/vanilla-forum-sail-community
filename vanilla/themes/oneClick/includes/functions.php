<?php

//==================================================================================================
//                                    CONFIGURATIONS
//==================================================================================================
$style_editor_version = '1.0';
//==================================================================================================

class cssEditor {

    public $fonts = array();
    public $fonts_path = '../../../../themes/oneClick/design/fonts/';
    public $fonts_filename = 'fonts.php';
    public $custom_style = '';

    function __construct() {
        $this->getFonts();
    }

    private function getFonts(){

        include($this->fonts_path.$this->fonts_filename);

        $this->fonts = $fonts;

        if ($handle = opendir($this->fonts_path)) {
            while (false !== ($file = readdir($handle))) {

                if ($file != "." and $file != ".." and is_dir($this->fonts_path.$file)) {

                    $this->fonts[] = array(
                        'name'   => $file,
                        'link'   => $file,
                        'family' => "'".trim(ucwords(preg_replace("/[^A-Za-z0-9]/", " ", $file)))."'",
                        'type'   => 'Font-Face'
                    );

                }
            }
            closedir($handle);
        }

    }

    function get($type,$attr,$value,$description){

        if($type=='font-family'){
            $out = $this->getFontFamily($attr,$value);
        }else if($type=='font-weight'){
            $out = $this->getFontWeight($attr,$value);
        }else if($type=='font-style'){
            $out = $this->getFontStyle($attr,$value);
        }else if($type=='font-stretch'){
            $out = $this->getFontStretch($attr,$value);
        }else if($type=='font-variant'){
            $out = $this->getFontVariant($attr,$value);
        }else if($type=='font-size' or $type=='line-height'){
            $out = $this->getSize($attr,$value);
        }else if(substr($type,-5)=='color'){
            $out = $this->getColor($attr,$value);
        }else if($type=='background-image'){
            $out = $this->getBackgroundImage($attr,$value,$description);
        }else if($type=='background-repeat'){
            $out = $this->getBackgroundRepeat($attr,$value);
        }else if($type=='background-attachment'){
            $out = $this->getBackgroundAttachment($attr,$value);
        }else if($type=='background-position'){
            $out = $this->getBackgroundPosition($attr,$value);
        }else if($type=='background-size'){
            $out = $this->getBackgroundSize($attr,$value);
        }else{
            $out = $this->getInput($attr,$value);
        }

        return $out;

    }

    function getInput($attr,$value){
        return '<input type="text" name="'.$attr.'" value="'.$value.'" class="InputBox" />';
    }

    function getSelect($attr,$options,$selected){
        $out = '<div class="selectHolder">';
        $out .= '<select name="'.$attr.'">';
        foreach($options as $key => $value){
            $out .= '<option value="'.$value.'"'.($value==$selected ? ' selected="selected"' : '').'>'.$value.'</option>';
        }
        $out .= '</select>';
        $out .= '<span></span>';
        $out .= '</div>';
        return $out;
    }

    function getColor($attr,$value){
        $out = '<div class="colorInput">
                    <input type="text" name="'.$attr.'" value="'.$value.'" maxlength="7" class="InputColor" />
                    <div class="colorSelector"></div>
                </div>';
        return $out;
    }

    function getSize($attr,$selected){
        $options = array('9px','10px','11px','12px','13px','14px','15px','16px','17px','18px','19px','20px','21px','22px','23px','24px','25px','26px','27px','28px','29px','30px','31px','32px','33px','34px','35px','36px','37px','38px','39px','40px','41px','42px','43px','44px','45px','46px','47px','48px','49px','50px');
        return $this->getSelect($attr,$options,$selected);
    }

    function getFontFamily($attr,$selected){
        $out = '<div class="selectHolder">';
        $out .= '<select name="'.$attr.'">';

        $flag_optgroup=false;
        $out .='<optgroup label="System Fonts">';
        foreach($this->fonts as $key => $font){
            if($font['type']=='Google' and !$flag_optgroup){
                $out.='</optgroup><optgroup label="Google Fonts">';
                $flag_optgroup=true;
            }
            if($font['type']=='Font-Face' and $flag_optgroup){
                $out.='</optgroup><optgroup label="Font-Face">';
                $flag_optgroup=false;
            }
            $out.='<option value="'.$font['name'].'"'.($font['name']==$selected ? ' selected="selected"' : '').'>'.$font['name'].'</option>';
        }
        $out .='</optgroup>';
        $out .= '</select>';
        $out .= '<span></span>';
        $out .= '</div>';

        return $out;
    }

    function getFontWeight($attr,$selected){
        $options = array('normal','bold','bolder','lighter','100','200','300','400','500','600','700','800','900','inherit');
        return $this->getSelect($attr,$options,$selected);
    }

    function getFontStyle($attr,$selected){
        $options = array('normal','italic','oblique','inherit');
        return $this->getSelect($attr,$options,$selected);
    }

    function getFontStretch($attr,$selected){
        $options = array('normal','wider','narrower','ultra-condensed','extra-condensed','condensed','semi-condensed','semi-expanded','expanded','extra-expanded','ultra-expanded','inherit');
        return $this->getSelect($attr,$options,$selected);
    }

    function getFontVariant($attr,$selected){
        $options = array('normal','small-caps','inherit');
        return $this->getSelect($attr,$options,$selected);
    }

    function getBackgroundImage($attr,$selected,$description){

        $selected_dir = substr(substr($description,strpos($description,'[')+1,strpos($description,']')-strpos($description,'[')-1),4);

        $selected_image = basename(substr($selected,strpos($selected,'(')+1,strpos($selected,')')-strpos($selected,'(')-1));

        $out = '<div class="backgroundImageContainer">';
        $out .= '<div class="selectHolder">';
        $out .='<select name="'.$attr.'" rel="../img/'.$selected_dir.'" onchange="verifyCustomImage(this);" onkeyup="verifyCustomImage(this);">';
        $out.='<option value="none"'.($selected_image=='none' ? ' selected="selected"' : '').'>none</option>';

        if($this->custom_style!=''){

            $out_custom = '';
            $custom_selected=false;
            $custom_dir = '../../../../themes/oneClick/design/styles/'.$this->custom_style.'/img/'.$selected_dir;
            if (is_dir($custom_dir) and $handle = opendir($custom_dir)) {
                while (false !== ($file = readdir($handle))) {

                    $file_ext = substr($file,strrpos($file,'.')+1);
                    if ($file != "." and $file != ".." and ($file_ext=='png' or $file_ext=='jpg' or $file_ext=='jpeg' or $file_ext=='gif')) {

                        $background = trim(ucwords(preg_replace("/[^A-Za-z0-9]/", " ", substr($file,0,strrpos($file,'.')))));

                        $out_custom.='<option value="url(../img/'.$selected_dir.$file.')"'.(($file==$selected_image and strpos($selected,'system')===false) ? ' selected="selected"' : '').'>'.$background.'</option>';

                        if($file==$selected_image and strpos($selected,'system')===false)
                            $custom_selected=true;

                    }
                }
                closedir($handle);
            }


        }
        if($out_custom!='')
            $out .= '<optgroup label="My Custom Images" class="custom_grp">'.$out_custom.'</optgroup>';

        $out_system = '';
        $backgrounds_dir = '../../../../themes/oneClick/design/styles/system/img/'.$selected_dir;
        if (@$handle = opendir($backgrounds_dir)) {
            while (false !== ($file = readdir($handle))) {

                $file_ext = substr($file,strrpos($file,'.')+1);
                if ($file != "." and $file != ".." and ($file_ext=='png' or $file_ext=='jpg' or $file_ext=='jpeg' or $file_ext=='gif')) {

                    $background = trim(ucwords(preg_replace("/[^A-Za-z0-9]/", " ", substr($file,0,strrpos($file,'.')))));

                    $out_system.='<option value="url(../../system/img/'.$selected_dir.$file.')"'.(($file==$selected_image and strpos($selected,'system')!==false) ? ' selected="selected"' : '').'>'.$background.'</option>';

                }
            }
            closedir($handle);
        }

        if($out_system!='')
            $out .= '<optgroup label="System Images" class="system_grp">'.$out_system.'</optgroup>';
        $out .='</select>';
        $out .= '<span></span>';
        $out .= '<div class="progress"><div class="bar" style="width: 0%;"></div></div>';
        $out .= '<div class="custom-image-msg"></div>';
        $out .= '</div>';
        $out .= '
<div class="fileupload-container">
    <div class="fileinput-button" title="Upload images">
        Add images...
        <input title="Upload images" class="fileupload" type="file" name="files[]" multiple upload-dir="img/'.$selected_dir.'">
    </div>
</div>';
        $out .= '<a href="javascript: deleteCustomImage(\''.$attr.'\',\'ask\');" title="Delete this Custom Image" class="delete-custom-image"'.($custom_selected ? '' : ' style="display: none;"').'></a>';
        $out .= '</div>';

        return $out;
    }

    function getBackgroundRepeat($attr,$selected){
        $options = array('repeat','repeat-x','repeat-y','no-repeat');
        return $this->getSelect($attr,$options,$selected);
    }

    function getBackgroundAttachment($attr,$selected){
        $options = array('scroll','fixed');
        return $this->getSelect($attr,$options,$selected);
    }

    function getBackgroundPosition($attr,$selected){
        $options = array('left top','left center','left bottom','right top','right center','right bottom','center top','center center','center bottom');
        return $this->getSelect($attr,$options,$selected);
    }

    function getBackgroundSize($attr,$selected){
        $options = array('auto','cover','contain');
        return $this->getSelect($attr,$options,$selected);
    }



    function getFontFromName($name){
        foreach($this->fonts as $key => $font){
            if($font['name']==$name){
                return $font;
            }
        }
    }

    function getFontFilesFromName($name){
        $files='|';
        $arr_files=array();
        if ($handle = opendir($this->fonts_path.$name.'/')) {
            while (false !== ($file = readdir($handle))) {

                if ($file != "." && $file != ".." and !is_dir($this->fonts_path.$file)) {

                    $files .= $file.'|';
                    $arr_files[] = $file;

                }
            }
            closedir($handle);
        }
        //return $files;
        return $arr_files;
    }

    function getFontsCode($selected_fonts){

        $out_google_font_family = '|';
        $out_font_face_font_family='';

        $available_weights_normal = array();
        $available_weights_italic = array();

        foreach($selected_fonts as $key => $value){

            $font = $this->getFontFromName(isset($value['name']) ? $value['name'] : '');

            if(!isset($value['weight']) or $value['weight']=='normal')
                $value['weight'] = 400;
            if(!isset($value['weight']) or $value['weight']=='bold')
                $value['weight'] = 700;

            if(!isset($value['style']))
                $value['style'] = 'normal';

            if(!isset($value['stretch']))
                $value['stretch'] = 'normal';

            if(!isset($value['variant']))
                $value['variant'] = 'normal';

            if($font['type']=='Google'){

                if(strpos($font['link'],':')!==false){

                    $font_name = substr($font['link'],0,strpos($font['link'],':'));
                    $available_weights = explode(',',substr($font['link'],strpos($font['link'],':')+1));

                    foreach($available_weights as $key2 => $value2){
                        if(strpos($value2,'italic')!==false)
                            $available_weights_italic[]=str_replace('italic','',$value2);
                        else
                            $available_weights_normal[]=$value2;
                    }

                }else{
                    $font_name = $font['link'];
                    $available_weights_normal = array();
                    $available_weights_italic = array();
                }

                if(in_array($value['weight'],${'available_weights_'.$value['style']})){
                    $font_weight_style=$value['weight'].($value['style']=='italic' ? 'italic' : '');
                }elseif(count(${'available_weights_'.$value['style']})<1){
                    $font_weight_style='';
                }else{
                    sort(${'available_weights_'.$value['style']},SORT_NUMERIC);
                    $lighter_weight='';
                    $darker_weight='';
                    foreach(${'available_weights_'.$value['style']} as $value2){
                        if($value2<=$value['weight'])
                            $lighter_weight=$value2;
                        if($value2>=$value['weight'] and $darker_weight=='')
                            $darker_weight=$value2;
                    }
                    if($lighter_weight=='')
                        $lighter_weight=$darker_weight;
                    if($darker_weight=='')
                        $darker_weight=$lighter_weight;
                    if($value['weight']<600){
                        $font_weight_style=$lighter_weight.($value['style']=='italic' ? 'italic' : '');
                    }else{
                        $font_weight_style=$darker_weight.($value['style']=='italic' ? 'italic' : '');
                    }
                }

                preg_match('/\|'.str_replace('+','\+',$font_name).':(.*?)\|/', $out_google_font_family, $matches);

                if(isset($matches[1])){
                    $google_weigts_and_style = $matches[1].',';
                    if(strpos($google_weigts_and_style,$font_weight_style.',')===false)
                        $out_google_font_family = preg_replace('/\|'.str_replace('+','\+',$font_name).':(.*?)\|/', '|'.$font_name.':$1,'.$font_weight_style.'|', $out_google_font_family);
                }else{
                    $out_google_font_family.=$font_name.($font_weight_style=='' ? '' : ':'.$font_weight_style).'|';
                }


            }

            if($font['type']=='Font-Face'){

                $font_files = $this->getFontFilesFromName($value['name']);

                $weights = array('Thin','ExtraLight','UltraLight','Light','Normal','Regular','Book','Medium','SemiBold','DemiBold','Bold','ExtraBold','UltraBold','Black','Heavy');
                $weights_values = array(
                    'Thin' => 100,
                    'ExtraLight' => 200,
                    'UltraLight' => 200,
                    'Light' => 300,
                    'Normal' => 400,
                    'Regular' => 400,
                    'Book' => 400,
                    'Medium' => 500,
                    'SemiBold' => 600,
                    'DemiBold' => 600,
                    'Bold' => 700,
                    'ExtraBold' => 800,
                    'UltraBold' => 800,
                    'Black' => 900,
                    'Heavy' => 900
                );

                $styles = array('Normal','Italic','Olique');
                $styles_values = array(
                    'Normal' => 'normal',
                    'Italic' => 'italic',
                    'Olique' => 'Olique'
                );

                $stretchs = array('Normal','Wider','Narrower','UltraCondensed','ExtraCondensed','Condensed','SemiCondensed','SemiExpanded','Expanded','ExtraExpanded','UltraExpanded');
                $stretchs_values = array(
                    'Normal' => 'normal',
                    'Wider' => 'wider',
                    'Narrower' => 'narrower',
                    'UltraCondensed' => 'ultra-condensed',
                    'ExtraCondensed' => 'extra-condensed',
                    'Condensed' => 'condensed',
                    'SemiCondensed' => 'semi-condensed',
                    'SemiExpanded' => 'semi-expanded',
                    'Expanded' => 'expanded',
                    'ExtraExpanded' => 'extra-expanded',
                    'UltraExpanded' => 'ultra-expanded'
                );

                $variants = array('normal','small-caps','SC');
                $variants_values = array(
                    'Normal' => 'normal',
                    'SmallCaps' => 'small-caps',
                    'SC' => 'small-caps'
                );


                $num_fonts=0;
                $selected_index='';
                $weight_flag=false;
                $style_flag=false;
                $stretch_flag=false;
                $variant_flag=false;
                $available_fonts = array();
                $available_fonts_inserted = array();
                foreach($font_files as $file){

                    if(preg_match('/'.$value['name'].'(-|_)?('.implode('|',$weights).')?(-|_)?('.implode('|',$styles).')?(-|_)?('.implode('|',$stretchs).')?(-|_)?('.implode('|',$variants).')?(-webfont)?\.(.*)/i',$file,$matches)){

                        $font_tag = ($matches[2]=='' ? '400' : $weights_values[$matches[2]]).'-'.($matches[4]=='' ? 'normal' : $styles_values[$matches[4]]).'-'.($matches[6]=='' ? 'normal' : $stretchs_values[$matches[6]]).'-'.($matches[8]=='' ? 'normal' : $variants_values[$matches[8]]);

                        $font_num = array_search($font_tag, $available_fonts_inserted);

                        if($font_num!==false){
                            $available_fonts[$font_num]['files'][] = $value['name'].$matches[1].$matches[2].$matches[3].$matches[4].$matches[5].$matches[6].$matches[7].$matches[8].$matches[9].'.'.$matches[10];

                        }else{

                            $available_fonts_inserted[$num_fonts] = $font_tag;
                            $available_fonts[$num_fonts] = array(
                                'weight'  => ($matches[2]=='' ? '400' : $weights_values[$matches[2]]),
                                'style'   => ($matches[4]=='' ? 'normal' : $styles_values[$matches[4]]),
                                'stretch' => ($matches[6]=='' ? 'normal' : $stretchs_values[$matches[6]]),
                                'variant' => ($matches[8]=='' ? 'normal' : $variants_values[$matches[8]])
                            );
                            $available_fonts[$num_fonts]['files'][] = $value['name'].$matches[1].$matches[2].$matches[3].$matches[4].$matches[5].$matches[6].$matches[7].$matches[8].$matches[9].'.'.$matches[10];

                            if($available_fonts[$num_fonts]['weight'] == $value['weight'])
                                $weight_flag=true;
                            if($available_fonts[$num_fonts]['style'] == $value['style'])
                                $style_flag=true;
                            if($available_fonts[$num_fonts]['stretch'] == $value['stretch'])
                                $stretch_flag=true;
                            if($available_fonts[$num_fonts]['variant'] == $value['variant'])
                                $variant_flag=true;

                            if($selected_index=='' and $available_fonts[$num_fonts]['weight'] == $value['weight'] and $available_fonts[$num_fonts]['style'] == $value['style'] and $available_fonts[$num_fonts]['stretch'] == $value['stretch'] and $available_fonts[$num_fonts]['variant'] == $value['variant'])
                                $selected_index = $num_fonts;

                            $num_fonts++;

                        }

                    }

                }

                if($selected_index==''){

                    $safe_style = !$style_flag ? 'normal' : $value['style'];
                    $safe_stretch = !$stretch_flag ? 'normal' : $value['stretch'];
                    $safe_variant = !$variant_flag ? 'normal' : $value['variant'];

                    $available_weights = array();
                    foreach($available_fonts as $key2 => $available_font){
                        if($available_font['style']==$safe_style and $available_font['stretch']==$safe_stretch and $available_font['variant']==$safe_variant)
                            $available_weights[] = $available_font['weight'];
                    }
                    sort($available_weights,SORT_NUMERIC);

                    $lighter_weight='';
                    $darker_weight='';
                    foreach($available_weights as $value2){
                        if($value2<=$value['weight'])
                            $lighter_weight=$value2;
                        if($value2>=$value['weight'] and $darker_weight=='')
                            $darker_weight=$value2;
                    }
                    if($lighter_weight=='')
                        $lighter_weight=$darker_weight;
                    if($darker_weight=='')
                        $darker_weight=$lighter_weight;
                    if($value['weight']<600){
                        $safe_weight=$lighter_weight;
                    }else{
                        $safe_weight=$darker_weight;
                    }

                    foreach($available_fonts as $key2 => $available_font){
                        if($available_font['weight']==$safe_weight and $available_font['style']==$safe_style and $available_font['stretch']==$safe_stretch and $available_font['variant']==$safe_variant)
                            $selected_index = $key2;
                    }

                    $selected_font = $available_fonts[$selected_index];

                }else{

                    $selected_font = $available_fonts[$selected_index];

                    $safe_weight = $value['weight'];
                    $safe_style = $value['style'];
                    $safe_stretch = $value['stretch'];
                    $safe_variant = $value['variant'];

                }

                $out_font_face_font_family.="
@font-face {
    font-family: '".$value['name']."';";

                foreach($selected_font['files'] as $file){
                    $ext = substr($file,strrpos($file,'.')+1);

                    if($ext == 'eot')
                        $out_font_face_font_family_eot="
    src: url('../../../fonts/".$value['name']."/".$file."');
    src: url('../../../fonts/".$value['name']."/".$file."?#iefix') format('embedded-opentype'),";

                    if($ext == 'woff')
                        $out_font_face_font_family_woff="
         url('../../../fonts/".$value['name']."/".$file."') format('woff'),";

                    if($ext == 'ttf')
                        $out_font_face_font_family_ttf="
         url('../../../fonts/".$value['name']."/".$file."') format('truetype'),";

                        if($ext == 'svg')
                    $out_font_face_font_family_svg="
         url('../../../fonts/".$value['name']."/".$file."#".$value['name']."') format('svg');";

                }

                $out_font_face_font_family.=$out_font_face_font_family_eot.$out_font_face_font_family_woff.$out_font_face_font_family_ttf.$out_font_face_font_family_svg;

                $out_font_face_font_family.="
    font-weight: ".$safe_weight.";
    font-style: ".$safe_style.";
    font-stretch: ".$safe_stretch.";
    font-variant: ".$safe_variant.";
}
";

            }

        }

        if($out_google_font_family!='|')
            $out_google_font_family="@import url(//fonts.googleapis.com/css?family=".substr(preg_replace('/:400\|/','|',$out_google_font_family),1,-1).");".PHP_EOL;
        else
            $out_google_font_family='';

        return $out_google_font_family.$out_font_face_font_family;

    }

}

// Saves to Config from an AJAX call
function SaveToConfigFromAjax($Name, $Value = '', $Options = array()){

    if(!defined('APPLICATION')) define('APPLICATION', 'Vanilla');
    define('APPLICATION_VERSION', '2.1');

    // Report and track all errors.

    error_reporting(E_ERROR | E_PARSE | E_CORE_ERROR | E_COMPILE_ERROR | E_USER_ERROR | E_RECOVERABLE_ERROR);
    ini_set('display_errors', 0);
    ini_set('track_errors', 1);

    //ob_start();

    // Define the constants we need to get going.

    define('DS', '/');
    define('PATH_ROOT', dirname(dirname(dirname(dirname(getcwd())))));

    // Include the bootstrap to configure the framework.
    require_once(PATH_ROOT.'/bootstrap.php');

    // Create and configure the dispatcher.
/*
    $Dispatcher = Gdn::Dispatcher();

    $EnabledApplications = Gdn::ApplicationManager()->EnabledApplicationFolders();
    $Dispatcher->EnabledApplicationFolders($EnabledApplications);
    $Dispatcher->PassProperty('EnabledApplications', $EnabledApplications);

    // Process the request.

    $Dispatcher->Start();
    $Dispatcher->Dispatch();
*/
    SaveToConfig($Name, $Value, $Options);

}

// Copies files and non-empty directories
function rcopy($src, $dst) {
    if (file_exists($dst)) rrmdir($dst);
    if (is_dir($src)) {
        mkdir($dst);
        $files = scandir($src);
        foreach ($files as $file)
            if ($file != "." && $file != "..") rcopy("$src/$file", "$dst/$file");
    }
    else if (file_exists($src)) copy($src, $dst);
}

// Removes files and non-empty directories
function rrmdir($dir) {
    if (is_dir($dir)) {
        $files = scandir($dir);
        foreach ($files as $file)
            if ($file != "." && $file != "..") rrmdir("$dir/$file");
            rmdir($dir);
    }
    else if (file_exists($dir)) unlink($dir);
}

// Zip a folder
function Zip($source, $destination, $include_dir = true)
{

    if (!extension_loaded('zip') || !file_exists($source)) {
        return false;
    }

    if (file_exists($destination)) {
        unlink ($destination);
    }

    $zip = new ZipArchive();
    if (!$zip->open($destination, ZIPARCHIVE::CREATE)) {
        return false;
    }
    $source = str_replace('\\', '/', realpath($source));

    if (is_dir($source) === true)
    {

        $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);

        if ($include_dir) {

            $arr = explode("/",$source);
            $maindir = $arr[count($arr)- 1];

            $source = "";
            for ($i=0; $i < count($arr) - 1; $i++) {
                $source .= '/' . $arr[$i];
            }

            $source = substr($source, 1);

            $zip->addEmptyDir($maindir);

        }

        foreach ($files as $file)
        {
            $file = str_replace('\\', '/', $file);

            // Ignore "." and ".." folders
            if( in_array(substr($file, strrpos($file, '/')+1), array('.', '..')) )
                continue;

            //$file = realpath($file);

            if (is_dir($file) === true)
            {
                $zip->addEmptyDir(str_replace($source . '/', '', $file . '/'));
            }
            else if (is_file($file) === true)
            {
                $zip->addFromString(str_replace($source . '/', '', $file), file_get_contents($file));
            }
        }
    }
    else if (is_file($source) === true)
    {
        $zip->addFromString(basename($source), file_get_contents($source));
    }

    return $zip->close();
}

function cssMinify( $css ) {

	$css = preg_replace( '#\s+#', ' ', $css );
	$css = preg_replace( '#/\*.*?\*/#s', '', $css );
	$css = str_replace( '; ', ';', $css );
	$css = str_replace( ': ', ':', $css );
	$css = str_replace( ' {', '{', $css );
	$css = str_replace( '{ ', '{', $css );
	$css = str_replace( ', ', ',', $css );
	$css = str_replace( '} ', '}', $css );
	$css = str_replace( ';}', '}', $css );

	return trim( $css );
}


//==================================================================================================
//                                  Formats the date
//==================================================================================================
function formatDate($datetime,$format){

    $date_time = explode(' ',$datetime);
    $date = explode('-',$date_time[0]);
    $time=array(0,0,0);
    if(!empty($date_time[1]))
        $time = explode(':',$date_time[1]);

    $timestamp = mktime($time[0], $time[1], $time[2], $date[1], $date[2], $date[0]);

    if($format=='timestamp')
        return date($format,$timestamp);
    else
        return date($format,$timestamp);
}

?>