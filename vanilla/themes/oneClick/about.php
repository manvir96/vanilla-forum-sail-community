<?php if (!defined('APPLICATION')) exit();

$ThemeInfo['oneClick'] = array(
   'Name' => 'One Click - Premium Vanilla Theme',
   'Description' => 'It has never been so fast and easy to start giving premium support.<br>Increase the quality of your services using the most Intuitive, Well Organized and Good Looking forum on the Web.',
   'Version' => '1.3.1',
   'Author' => "Creative Dreams",
   'AuthorEmail' => 'sandro@creativedreams.eu',
   'AuthorUrl' => 'http://www.creativedreams.eu',
   'Options' => array(
      'Description' => T('Choose your current active style and the desired layout. You can also configure any of the available styles and preview it on the front end.<br><small>While you preview a style, all other users will only see the current active style.</small>'),
      'Styles' => array(
         'oneClick' => array(
            'Basename' => '%s',
            'Description' => T('This is the default layout.<br>The main content will appear on the left and the sidebar on the right.')
         ),
         'oneClick Right' => array(
            'Basename' => '%s_right',
            'Description' => T('A right side layout.<br>The main content will appear on the right and the sidebar on the left.')
         )
      ),
      'Text' => array(
         'CurrentStyle' => array(
            'Type'=>'textbox',
            'Description' => T('Current Style'),
            'Default'=>''
         ),
         'PreviewStyle' => array(
            'Type'=>'textbox',
            'Description' => T('Show the Preview Style Selector'),
            'Default'=>''
         ),
         'ExtraStyle' => array(
            'Type'=>'textbox',
            'Description' => T('Extra Style Being Configured'),
            'Default'=>''
         )
      )
   )
);