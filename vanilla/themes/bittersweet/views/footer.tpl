<div class="sail_footer">
    <div class="container sail-footer-content">
        <div class="row text-left">
            <div class="col-5">
                <div class="white-logo footer-heading">
                    <img src="http://localhost/themes/bittersweet/views/images/sail-logo-white.png" height="55.7px" width="89.1px">
                </div>
                <div class="about-sail-cannabis">
                    <p>Sail bridges the gap between practitioners, patients and regulated vendors as a trusted medical resource.
                        Sail simplifies the cannabis journey by providing secure, clinical & peer-peer data driven solutions.</p>
                </div>
                <div class="sail-copyright">Copyright © 2010 - 2017 MVC Technologies. All Rights Reserved.</div>
            </div>
            <div class="col-7">
                <div class="row">
            <div class="col-4">

                <div class="footer-heading footer-heading-2">Company</div>
                <ul>
                    <li>
                        <a href="#">Careers</a>
                    </li>
                    <li>
                        <a href="#">Contact</a>
                    </li>
                    <li>
                        <a href="#">Terms of Service</a>
                    </li>
                    <li>
                        <a href="#">Privacy Policy</a>
                    </li>

                </ul>
            </div>
            <div class="col-4">
                <div class="footer-heading footer-heading-2">Pro Solutions</div>

                <ul>
                    <li>
                        <a href="#">Physicians</a>
                    </li>
                    <li>
                        <a href="#">Licensed Producers</a>
                    </li>

                </ul>

            </div>
            <div class="col-4">
                <div class="footer-heading footer-heading-2">Contact</div>
                <ul>
                    <li>
                        <a href="mailto:info@sailcannabis.co">info@sailcannabis.co</a>
                    </li>
                    <div class="social-icons">
                        <a href="https://www.facebook.com/sailcann/" class="social-icons" target="_blank">
                            <i class="fa fa-facebook" aria-hidden="true"></i>
                        </a>
                        <a href="https://twitter.com/sailcannabis" class="social-icons" target="_blank">
                            <i class="fa fa-twitter" aria-hidden="true"></i>
                        </a>
                        <a href="https://www.instagram.com/sailcannabis_/" class="social-icons" target="_blank">
                            <i class="fa fa-instagram" aria-hidden="true"></i>
                        </a>
                    </div>

                </ul>
            </div>
</div>
                </div>
        </div>
    </div>
</div>