<!DOCTYPE html>
<html>
<head>
    {asset name="Head"}
</head>
<body id="{$BodyID}" class="{$BodyClass}">
<div id="Frame">
    <div class="Top">
       
            <div class="top-bar" >
    <div class="container">
        <a href="http://sailcannabis.co/physicians.html"><u>For Physicians</u></a>
        <a style="margin-left: 24px" href="http://sailcannabis.co/suppliers.html"><u>For Suppliers</u></a>
    </div>
</div>
        
    </div>
    <div class="Banner">
        

{include file='header.tpl'}

        </div>
    </div>
    <div id="Head">
        <div class="container">
            <div class="SiteSearch">{searchbox}</div>
            <ul class="SiteMenu">
                {dashboard_link}
                {discussions_link}
                {activity_link}
                {inbox_link}
<!--                {custom_menu}-->
<!--                <li><a href="http://localhost/articles/categories">Articles</a></li>-->
                {profile_link}
                {signinout_link}
            </ul>
        </div>
    </div>
    <div class="BreadcrumbsWrapper">
        <div class="container">
            {breadcrumbs}
        </div>
    </div>
    <div id="Body">
        <div class="container">
            <div class="Column PanelColumn" id="Panel">
                {module name="MeModule"}
                {asset name="Panel"}
            </div>
            <div class="Column ContentColumn" id="Content">{asset name="Content"}</div>
        </div>
    </div>
    <div id="Foot">
        {include file='footer.tpl'}
<!--
        <div class="Row">
            <a href="{vanillaurl}" class="PoweredByVanilla">Powered by Vanilla</a>
            {asset name="Foot"}
        </div>
-->
    </div>

{event name="AfterBody"}
</body>
</html>
