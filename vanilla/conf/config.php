<?php if (!defined('APPLICATION')) exit();

// Articles
$Configuration['Articles']['Version'] = '1.2.0';
$Configuration['Articles']['ShowArticlesMenuLink'] = '1';
$Configuration['Articles']['ShowCategoriesMenuLink'] = '1';
$Configuration['Articles']['Articles']['ShowAuthorInfo'] = '1';
$Configuration['Articles']['Articles']['ShowSimilarArticles'] = '1';
$Configuration['Articles']['Comments']['EnableThreadedComments'] = '1';
$Configuration['Articles']['Comments']['AllowGuests'] = '';
$Configuration['Articles']['TwitterUsername'] = '@sailcannabis';
$Configuration['Articles']['Modules']['ShowCategoriesAsDropDown'] = '1';

// Conversations
$Configuration['Conversations']['Version'] = '2.3.1';

// Database
$Configuration['Database']['Name'] = 'vanilla';
$Configuration['Database']['Host'] = 'localhost';
$Configuration['Database']['User'] = 'root';
$Configuration['Database']['Password'] = '';

// EnabledApplications
$Configuration['EnabledApplications']['Conversations'] = 'conversations';
$Configuration['EnabledApplications']['Vanilla'] = 'vanilla';
$Configuration['EnabledApplications']['Yaga'] = 'yaga';

// EnabledPlugins
$Configuration['EnabledPlugins']['GettingStarted'] = false;
$Configuration['EnabledPlugins']['HtmLawed'] = 'HtmLawed';
$Configuration['EnabledPlugins']['Voting'] = true;
$Configuration['EnabledPlugins']['NoCaptchaReCaptcha'] = false;
$Configuration['EnabledPlugins']['OneallSocialLogin'] = false;
$Configuration['EnabledPlugins']['CountdownTimer'] = true;
$Configuration['EnabledPlugins']['registrationmessage'] = true;
$Configuration['EnabledPlugins']['QnA'] = true;
$Configuration['EnabledPlugins']['PrivateCommunity'] = true;
$Configuration['EnabledPlugins']['ReplyTo'] = false;
$Configuration['EnabledPlugins']['TopPosters'] = true;
$Configuration['EnabledPlugins']['DiscussionPolls'] = true;
$Configuration['EnabledPlugins']['Emoticons'] = true;
$Configuration['EnabledPlugins']['noCaptcha'] = true;
$Configuration['EnabledPlugins']['EmojiExtender'] = false;
$Configuration['EnabledPlugins']['Facebook'] = false;
$Configuration['EnabledPlugins']['Gravatar'] = true;
$Configuration['EnabledPlugins']['vanillicon'] = false;
$Configuration['EnabledPlugins']['VanillaInThisDiscussion'] = true;
$Configuration['EnabledPlugins']['GooglePlus'] = false;
$Configuration['EnabledPlugins']['IndexPhotos'] = false;
$Configuration['EnabledPlugins']['ProfileExtender'] = true;
$Configuration['EnabledPlugins']['MyProfile'] = true;
$Configuration['EnabledPlugins']['reply'] = false;
$Configuration['EnabledPlugins']['Quotes'] = true;
$Configuration['EnabledPlugins']['Sprites'] = true;
$Configuration['EnabledPlugins']['Pockets'] = true;
$Configuration['EnabledPlugins']['Groups'] = true;
$Configuration['EnabledPlugins']['Memberships'] = true;
$Configuration['EnabledPlugins']['Uploader'] = true;

// Garden
$Configuration['Garden']['Title'] = 'Sail Cannabis Forums';
$Configuration['Garden']['Cookie']['Salt'] = 'zhcz8MMx0N3T6UIk';
$Configuration['Garden']['Cookie']['Domain'] = '';
$Configuration['Garden']['Registration']['ConfirmEmail'] = false;
$Configuration['Garden']['Registration']['Method'] = 'NoCaptcha';
$Configuration['Garden']['Registration']['InviteExpiration'] = '1 week';
$Configuration['Garden']['Registration']['CaptchaPrivateKey'] = '6LdpwzwUAAAAAGWKSGSDOeIgt_hYvZV8ps0HmIlN';
$Configuration['Garden']['Registration']['CaptchaPublicKey'] = '6LdpwzwUAAAAAH7VCKfxmcLouZloRQzUJzrUX6dv';
$Configuration['Garden']['Registration']['InviteRoles']['3'] = '0';
$Configuration['Garden']['Registration']['InviteRoles']['4'] = '0';
$Configuration['Garden']['Registration']['InviteRoles']['8'] = '0';
$Configuration['Garden']['Registration']['InviteRoles']['16'] = '0';
$Configuration['Garden']['Registration']['InviteRoles']['32'] = '0';
$Configuration['Garden']['Email']['SupportName'] = 'Sail Cannabis Forums';
$Configuration['Garden']['Email']['Format'] = 'html';
$Configuration['Garden']['Email']['SupportAddress'] = 'noreply@mvctech.ca';
$Configuration['Garden']['Email']['UseSmtp'] = '1';
$Configuration['Garden']['Email']['SmtpHost'] = 'outlook.office365.com';
$Configuration['Garden']['Email']['SmtpUser'] = 'noreply@mvctech.ca';
$Configuration['Garden']['Email']['SmtpPassword'] = 'MVC@2016!';
$Configuration['Garden']['Email']['SmtpPort'] = '587';
$Configuration['Garden']['Email']['SmtpSecurity'] = 'tls';
$Configuration['Garden']['Email']['OmitToName'] = false;
$Configuration['Garden']['SystemUserID'] = '1';
$Configuration['Garden']['InputFormatter'] = 'Markdown';
$Configuration['Garden']['Version'] = '2.3.1';
$Configuration['Garden']['Cdns']['Disable'] = false;
$Configuration['Garden']['CanProcessImages'] = true;
$Configuration['Garden']['Installed'] = true;
$Configuration['Garden']['Theme'] = 'oneClick';
$Configuration['Garden']['HomepageTitle'] = 'Sail Cannabis Forum';
$Configuration['Garden']['Description'] = '';
$Configuration['Garden']['Embed']['Allow'] = false;
$Configuration['Garden']['Embed']['RemoteUrl'] = 'http://localhost:63342/Sail-Portal/index.html?_ijt=pc9u00ic4lj39amkh1898bq90r';
$Configuration['Garden']['Profile']['MaxHeight'] = 300;
$Configuration['Garden']['Profile']['MaxWidth'] = 300;
$Configuration['Garden']['Thumbnail']['Size'] = 150;
$Configuration['Garden']['Thumbnail']['Width'] = 150;
$Configuration['Garden']['MobileTheme'] = 'oneClick';
$Configuration['Garden']['ThemeOptions']['Name'] = 'One Click - Premium Vanilla Theme';
$Configuration['Garden']['Upload']['AllowedFileExtensions'] = array('txt', 'jpg', 'jpeg', 'gif', 'png', 'bmp', 'tiff', 'ico', 'zip', 'gz', 'tar.gz', 'tgz', 'psd', 'ai', 'fla', 'pdf', 'doc', 'xls', 'ppt', 'docx', 'xlsx', 'log', 'rar', '7z');

// Modules
$Configuration['Modules']['Vanilla']['Panel'] = array('MeModule', 'UserBoxModule', 'GuestModule', 'NewDiscussionModule', 'DiscussionFilterModule', 'CategoriesModule', 'SignedInModule', 'Ads');

// Plugin
$Configuration['Plugin']['OASocialLogin']['Enable'] = '1';
$Configuration['Plugin']['OASocialLogin']['Curl'] = '1';
$Configuration['Plugin']['OASocialLogin']['SSL'] = '0';
$Configuration['Plugin']['OASocialLogin']['IndexPageEnable'] = '1';
$Configuration['Plugin']['OASocialLogin']['IndexPageCaption'] = 'Login with:';
$Configuration['Plugin']['OASocialLogin']['LoginPageEnable'] = '1';
$Configuration['Plugin']['OASocialLogin']['LoginPageCaption'] = 'Login with:';
$Configuration['Plugin']['OASocialLogin']['RegistrationPageEnable'] = '1';
$Configuration['Plugin']['OASocialLogin']['RegistrationPageCaption'] = 'Register with:';
$Configuration['Plugin']['OASocialLogin']['AvatarsEnable'] = '1';
$Configuration['Plugin']['OASocialLogin']['Validate'] = '0';
$Configuration['Plugin']['OASocialLogin']['LinkingEnable'] = '1';
$Configuration['Plugin']['OASocialLogin']['Redirect'] = '';
$Configuration['Plugin']['OASocialLogin']['ApiSubdomain'] = 'app-745021-1';
$Configuration['Plugin']['OASocialLogin']['ApiKey'] = 'fe4dc0df-d402-4207-aed0-4a617bcc8608';
$Configuration['Plugin']['OASocialLogin']['ApiSecret'] = 'e80d060d-951b-4bd2-8892-3642ba4c7b31';
$Configuration['Plugin']['OASocialLogin']['Providers'] = array('facebook', 'google', 'instagram', 'linkedin');

// Plugins
$Configuration['Plugins']['GettingStarted']['Dashboard'] = '1';
$Configuration['Plugins']['GettingStarted']['Plugins'] = '1';
$Configuration['Plugins']['GettingStarted']['Discussion'] = '1';
$Configuration['Plugins']['GettingStarted']['Registration'] = '1';
$Configuration['Plugins']['GettingStarted']['Categories'] = '1';
$Configuration['Plugins']['GettingStarted']['Profile'] = '1';
$Configuration['Plugins']['ReplyTo']['Enabled'] = false;
$Configuration['Plugins']['DiscussionPolls']['EnableShowResults'] = '1';
$Configuration['Plugins']['DiscussionPolls']['DisablePollTitle'] = false;
$Configuration['Plugins']['Vanillicon']['Type'] = 'v1';
$Configuration['Plugins']['MyProfile']['Version'] = '0.2.0b';
$Configuration['Plugins']['CountdownTimer']['Tag'] = '[COUNTDOWN]';
$Configuration['Plugins']['CountdownTimer']['Time'] = '12:23:12';
$Configuration['Plugins']['CountdownTimer']['Timezone'] = 'UTC';
$Configuration['Plugins']['Tagging']['DisableInline'] = false;
$Configuration['Plugins']['Tagging']['EnableInMeta'] = true;
$Configuration['Plugins']['Groups']['Enabled'] = true;
$Configuration['Plugins']['Memberships']['Enabled'] = true;
$Configuration['Plugins']['Uploader']['Enabled'] = true;

// ProfileExtender
$Configuration['ProfileExtender']['Fields']['Yearsusingcannabis']['FormType'] = 'TextBox';
$Configuration['ProfileExtender']['Fields']['Yearsusingcannabis']['Label'] = 'Years using cannabis';
$Configuration['ProfileExtender']['Fields']['Yearsusingcannabis']['Options'] = '';
$Configuration['ProfileExtender']['Fields']['Yearsusingcannabis']['OnProfile'] = '1';
$Configuration['ProfileExtender']['Fields']['Yearsusingcannabis']['Required'] = false;
$Configuration['ProfileExtender']['Fields']['Yearsusingcannabis']['OnRegister'] = false;
$Configuration['ProfileExtender']['Fields']['Aboutme']['FormType'] = 'TextBox';
$Configuration['ProfileExtender']['Fields']['Aboutme']['Label'] = 'About me';
$Configuration['ProfileExtender']['Fields']['Aboutme']['Options'] = '';
$Configuration['ProfileExtender']['Fields']['Aboutme']['OnProfile'] = '1';
$Configuration['ProfileExtender']['Fields']['Aboutme']['Required'] = false;
$Configuration['ProfileExtender']['Fields']['Aboutme']['OnRegister'] = false;
$Configuration['ProfileExtender']['Fields']['Condition']['FormType'] = 'TextBox';
$Configuration['ProfileExtender']['Fields']['Condition']['Label'] = 'Condition';
$Configuration['ProfileExtender']['Fields']['Condition']['Options'] = '';
$Configuration['ProfileExtender']['Fields']['Condition']['OnProfile'] = '1';
$Configuration['ProfileExtender']['Fields']['Condition']['Required'] = false;
$Configuration['ProfileExtender']['Fields']['Condition']['OnRegister'] = false;

// RegistrationMessage
$Configuration['RegistrationMessage']['Message'] = 'Hi %%NAME%%, welcome to the community!';

// ReplyTo
$Configuration['ReplyTo']['Mention']['Insert'] = '1';

// Routes
$Configuration['Routes']['DefaultController'] = array('categories', 'Internal');

// ThemeOption
$Configuration['ThemeOption']['CurrentStyle'] = 'material-blue';
$Configuration['ThemeOption']['PreviewStyle'] = true;
$Configuration['ThemeOption']['ExtraStyle'] = 'material-blue';

// TopPosters
$Configuration['TopPosters']['Limit'] = '5';
$Configuration['TopPosters']['Location']['Show'] = 'every';
$Configuration['TopPosters']['Excluded'] = false;

// Vanilla
$Configuration['Vanilla']['Version'] = '2.3.1';
$Configuration['Vanilla']['Discussions']['Layout'] = 'modern';
$Configuration['Vanilla']['Categories']['Layout'] = 'mixed';
$Configuration['Vanilla']['Categories']['MaxDisplayDepth'] = '2';
$Configuration['Vanilla']['Categories']['DoHeadings'] = false;
$Configuration['Vanilla']['Categories']['HideModule'] = false;
$Configuration['Vanilla']['Categories']['Use'] = true;

// Yaga
$Configuration['Yaga']['Version'] = '1.1';
$Configuration['Yaga']['Reactions']['Enabled'] = '1';
$Configuration['Yaga']['Badges']['Enabled'] = '1';
$Configuration['Yaga']['Ranks']['Enabled'] = '1';
$Configuration['Yaga']['MenuLinks']['Show'] = '';
$Configuration['Yaga']['LeaderBoard']['Enabled'] = '';
$Configuration['Yaga']['LeaderBoard']['Limit'] = '';

// Last edited by manvir (127.0.0.1)2018-01-24 21:43:28