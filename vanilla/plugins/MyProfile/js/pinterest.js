(function($) {
  $.pinterest = {
    albums: function(user, callback) {
      var url = "http://pinterestapi.co.uk/:user_id/pins?format=json&jsoncallback=?"
      url = url.replace(/:user_id/, user);
      $.getJSON(url, function(data) {
        var album = null;
        var albums = [];
        $.each(data.body, function(i, element) {
          album = {}
          album.images = element;
          albums.push(album);
        });
        callback(albums);
      });
    }
  };

  $.fn.pinterestAlbums = function(user, callback) {
    $.pinterest.albums(user, function(images) {
      if (callback) {
        callback(images);
      }
    });
  };

  $.fn.pinterestGallery = function(user, album, callback) {
    var scope = $(this);
    $.pinterest.images(user, album, function(images) {
      if (callback) {
        callback.apply(scope, images);
      } else {
        var pinterestAlbum = "<ul class='pinterest-album'>\n";
        $.each(images, function(i, element) {
          pinterestAlbum += "  <li class='pinterest-image'>\n";
          pinterestAlbum += "    <a class='pinterest-image-large' href='" + element.href + "'>\n";
          pinterestAlbum += "      <img class='pinterest-image-thumb' src='" + element.src + "'/>\n";
          pinterestAlbum += "    </a>\n";
          pinterestAlbum += "  </li>\n";
        });
        pinterestAlbum += "</ul>";
        scope.append(pinterestAlbum);
      }
    });
  }
})(jQuery);
jQuery(document).ready(function(){
	// adds a gallery of album covers
	$(".pinterest").each(function(){
		var that = $(this);
		$(this).pinterestAlbums($(this).val(),function(images){
			var pinterestAlbum = '<div>Album Feed:</b>';
			pinterestAlbum += "<ul class='album'>\n";
			$.each(images, function(i, element) {
			  pinterestAlbum += "  <li class='image'>\n";
			  pinterestAlbum += "     <a href='" + element.href + "'>\n";
			  pinterestAlbum += "     <b>" + element.desc + "</b>\n";
			  pinterestAlbum += "     <img src='" + element.src + "'/>\n";
			  pinterestAlbum += "     </a>\n";
			  pinterestAlbum += "  </li>\n";
			});
			pinterestAlbum += "</ul>";
			pinterestAlbum += "</div>";
			$(that).next('a').after(pinterestAlbum);
			
		});
	});
});
